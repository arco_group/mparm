#ifndef _AXI_MASTER_IF_MO_IMPL_H
#define _AXI_MASTER_IF_MO_IMPL_H

#include <systemc.h>

#include "axi_master_if.h"
#include "axi_master_port.h"

class ccss_monitor_msg;

template <int DataBusWidth = 32>
class AXI_MasterIFMoImpl
#ifdef __EDG__
	: virtual public sc_module
	, public axi_namespace::AXI_MasterIF
#else
	: public axi_namespace::AXI_MasterIF
#endif
{
public:

	// Port used for forwarding master method calls.
	sc_port<axi_namespace::AXI_MasterIF,2> MasterP;


	// This single bit signal indicates, when high, that
	// the address and control information is valid and
	// will remain stable until the address acknowledge
	// signal, AREADY, is high.
	// 
	// The default value of this signal is low.
	sc_out<sc_bv<1> >              AVALID;

	// The address bus gives the initial address of a
	// burst transaction. Only the start address of the
	// burst is provided and the control signals that are
	// broadcast alongside the address detail how the
	// address should be modified for the remaining
	// transfers in the burst.
	sc_out<sc_bv<32> >             ADDR;

	// The write signal indicates the direction of data
	// transfer for the burst. When high the burst is a
	// write, when low the burst is a read.
	sc_out<sc_bv<1> >              AWRITE;

	// The burst length gives the exact number of
	// transfers in a burst. This information is used to
	// determine the number of data transfers
	// associated with the address.
	sc_out<sc_bv<4> >              ALEN;

	// Gives the size of each transfer in the burst. For
	// write transfers, byte lane strobes are used to
	// indicate exactly which byte lanes should be
	// updated.
	sc_out<sc_bv<3> >              ASIZE;

	// The burst type, coupled with the size
	// information, details how the address for each
	// transfer within the burst should be calculated.
	sc_out<sc_bv<2> >              ABURST;

	// Gives additional information about the atomic
	// characteristics of the transfer.
	sc_out<sc_bv<2> >              ALOCK;

	// Gives additional information about cacheable
	// characteristics of the transfer.
	sc_out<sc_bv<4> >              ACACHE;

	// Gives protection unit information for the
	// transaction.
	sc_out<sc_bv<3> >              APROT;

	// The identification tag for the address group of
	// signals.
	sc_out<sc_bv<14> >             AID;

	// The read ready signal is used to indicate if the
	// master can accept the read data and response
	// information.
	sc_out<sc_bv<1> >              RREADY;

	// The write valid signal is used to indicate that
	// valid write data and strobes are available.
	// 
	// The default value for this signal is low.
	sc_out<sc_bv<1> >              WVALID;

	// Indicates the last data transfer in a write burst.
	sc_out<sc_bv<1> >              WLAST;

	// Write data bus. The write data bus can be any
	// width that is a power of 2, (i.e. 8, 16, 32, 64, 128,
	// 256, 512 or 1024 bits).
	sc_out<sc_bv<DataBusWidth> >   WDATA;

	// The write strobes are used to indicate which byte
	// lanes should be updated. One write strobe exists
	// for each 8 bits of the write data bus, so WSTRB[n]
	// corresponds to WDATA[(8*n)+7: (8*n)].
	sc_out<sc_bv<DataBusWidth/8> > WSTRB;

	// The identification tag for the write data group of
	// signals. The write ID is generated by the master
	// and should match the corresponding AID.
	sc_out<sc_bv<14> >             WID;

	// The response ready signal is used to indicate if
	// the master can accept the response information.
	sc_out<sc_bv<1> >              BREADY;


	// constructor

	AXI_MasterIFMoImpl(int ByteStrobeMin = 0,
					   int ByteStrobeMax = 127);

	// destructor
	~AXI_MasterIFMoImpl();

	// Master IF methods
	virtual void PutChannel(int Idx, const axi_namespace::Achannel&);
	virtual void PutChannel(int Idx, const axi_namespace::Wchannel&);
	virtual void PutChannel(int Idx, const axi_namespace::RreadySig&);
	virtual void PutChannel(int Idx, const axi_namespace::BreadySig&);

	virtual int GetMasterIdx(int MasterID, int& SlaveID);

	// Direct access method Master to Slave direction.
	virtual bool PutDirect(int ID, bool Write, 
						   axi_namespace::axi_addr_t Addr,
						   axi_namespace::axi_data_t Data,
						   int NumBytes);

	virtual bool PutDirect(int ID, int BeatNumber,
						   axi_namespace::Achannel &A,
						   const axi_namespace::Wchannel& W,
						   axi_namespace::axi_resp_t& Status);

	virtual bool PutDirect(int ID, int BeatNumber,
						   axi_namespace::Achannel &A,
						   axi_namespace::Rchannel &R,
						   axi_namespace::axi_resp_t& Status);

	virtual bool GetSlaveParam(int ID, int& ItrlDepth,
							   axi_namespace::AXI_AddressMap&);

protected :

	virtual void EnableMonitor(bool);

	virtual void SetTraceMonitor(ccss_monitor_msg*);
	virtual void SetTraceFile(ofstream*);

	virtual void register_port(sc_port_base& port, const char* if_typename);

private :

	bool                    m_MonitorOn;

	ccss_monitor_msg*       m_trace_transfers;
	ofstream*               m_trace_file;

	sc_string               m_trace_msg;

	int                     m_ByteStrobeMin;
	int                     m_ByteStrobeMax;

	sc_port_base            *m_mportb;
};
#endif // _AXI_MONITOR_IMPL_H
