///////////////////////////////////////////////////////////////////////////////
// Copyright 2003 DEIS - Universita' di Bologna
// 
// name         amba_occn_signal.h
// author       DEIS - Universita' di Bologna
//              Davide Bertozzi - dbertozzi@deis.unibo.it
//              Mirko Loghi - mloghi@deis.unibo.it
//              Federico Angiolini - fangiolini@deis.unibo.it
//              Francesco Poletti - fpoletti@deis.unibo.it
// portions by  Massimo Scardamaglia - mascard@vizzavi.it
// info         Global signals of AMBA (OCCN) bus
//
///////////////////////////////////////////////////////////////////////////////


#ifndef __AMBA_OCCN_SIGNAL_H__
#define __AMBA_OCCN_SIGNAL_H__

#include <systemc.h>
#include "globals.h"
#include "config.h"
#include "core_signal.h"

#endif // __AMBA_OCCN_SIGNAL_H__
