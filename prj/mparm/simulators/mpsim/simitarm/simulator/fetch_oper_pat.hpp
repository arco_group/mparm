/*************************************************************************
    Copyright (C) 2002,2003,2004 Wei Qin
    See file COPYING for more information.

    This program is free software; you can redistribute it and/or modify    
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*************************************************************************/
/*
Binary decoder synthesized by decgen version 1.0a

Input statistics 
Total entries  : 1289
Unique labels  : 548
Shannon entropy     : 9.09208
Huffman tree height : 9.12751

Decoder characteristics 
Gamma          : 0.25
1 bit only     : 0
Average height : 2.35487
Maximum height : 3
Minimum height : 1
Table entries  : 1216
Total nodes    : 323
*/

_STUB_DEC(stub_0_0)
_STUB_DEC(stub_0_1)
_STUB_DEC(stub_0_2)
_STUB_DEC(stub_0_3)
_STUB_DEC(stub_0_4)
_STUB_DEC(stub_0_5)
_STUB_DEC(stub_0_6)
_STUB_DEC(stub_0_7)
_STUB_DEC(stub_0_8)
_STUB_DEC(stub_0_9)
_STUB_DEC(stub_0_10)
_STUB_DEC(stub_0_11)
_STUB_DEC(stub_0_12)
_STUB_DEC(stub_0_13)
_STUB_DEC(stub_0_14)
_STUB_DEC(stub_0_15)
_STUB_DEC(stub_0_16)
_STUB_DEC(stub_0_17)
_STUB_DEC(stub_0_18)
_STUB_DEC(stub_0_19)
_STUB_DEC(stub_0_20)
_STUB_DEC(stub_0_21)
_STUB_DEC(stub_0_22)
_STUB_DEC(stub_0_23)
_STUB_DEC(stub_0_24)
_STUB_DEC(stub_0_25)
_STUB_DEC(stub_0_26)
_STUB_DEC(stub_0_27)
_STUB_DEC(stub_0_28)
_STUB_DEC(stub_0_29)
_STUB_DEC(stub_0_30)
_STUB_DEC(stub_0_31)
_STUB_DEC(stub_0_96)
_STUB_DEC(stub_0_97)
_STUB_DEC(stub_0_98)
_STUB_DEC(stub_0_99)
_STUB_DEC(stub_0_100)
_STUB_DEC(stub_0_101)
_STUB_DEC(stub_0_102)
_STUB_DEC(stub_0_103)
_STUB_DEC(stub_0_104)
_STUB_DEC(stub_0_105)
_STUB_DEC(stub_0_106)
_STUB_DEC(stub_0_107)
_STUB_DEC(stub_0_108)
_STUB_DEC(stub_0_109)
_STUB_DEC(stub_0_110)
_STUB_DEC(stub_0_111)
_STUB_DEC(stub_0_112)
_STUB_DEC(stub_0_113)
_STUB_DEC(stub_0_114)
_STUB_DEC(stub_0_115)
_STUB_DEC(stub_0_116)
_STUB_DEC(stub_0_117)
_STUB_DEC(stub_0_118)
_STUB_DEC(stub_0_119)
_STUB_DEC(stub_0_120)
_STUB_DEC(stub_0_121)
_STUB_DEC(stub_0_122)
_STUB_DEC(stub_0_123)
_STUB_DEC(stub_0_124)
_STUB_DEC(stub_0_125)
_STUB_DEC(stub_0_126)
_STUB_DEC(stub_0_127)
_STUB_DEC(stub_0_208)
_STUB_DEC(stub_0_209)
_STUB_DEC(stub_0_210)
_STUB_DEC(stub_0_211)
_STUB_DEC(stub_0_212)
_STUB_DEC(stub_0_213)
_STUB_DEC(stub_0_214)
_STUB_DEC(stub_0_215)
_STUB_DEC(stub_0_216)
_STUB_DEC(stub_0_217)
_STUB_DEC(stub_0_218)
_STUB_DEC(stub_0_219)
_STUB_DEC(stub_0_220)
_STUB_DEC(stub_0_221)
_STUB_DEC(stub_0_222)
_STUB_DEC(stub_0_223)
_STUB_DEC(stub_1_0)
_STUB_DEC(stub_1_2)
_STUB_DEC(stub_1_4)
_STUB_DEC(stub_1_6)
_STUB_DEC(stub_2_0)
_STUB_DEC(stub_2_2)
_STUB_DEC(stub_2_4)
_STUB_DEC(stub_2_6)
_STUB_DEC(stub_3_0)
_STUB_DEC(stub_3_2)
_STUB_DEC(stub_3_4)
_STUB_DEC(stub_3_6)
_STUB_DEC(stub_4_0)
_STUB_DEC(stub_4_2)
_STUB_DEC(stub_4_4)
_STUB_DEC(stub_4_6)
_STUB_DEC(stub_5_0)
_STUB_DEC(stub_5_2)
_STUB_DEC(stub_5_4)
_STUB_DEC(stub_5_6)
_STUB_DEC(stub_6_0)
_STUB_DEC(stub_6_2)
_STUB_DEC(stub_6_4)
_STUB_DEC(stub_6_6)
_STUB_DEC(stub_7_0)
_STUB_DEC(stub_7_2)
_STUB_DEC(stub_7_4)
_STUB_DEC(stub_7_6)
_STUB_DEC(stub_8_0)
_STUB_DEC(stub_8_2)
_STUB_DEC(stub_8_4)
_STUB_DEC(stub_8_6)
_STUB_DEC(stub_9_0)
_STUB_DEC(stub_9_2)
_STUB_DEC(stub_9_4)
_STUB_DEC(stub_9_6)
_STUB_DEC(stub_10_0)
_STUB_DEC(stub_10_2)
_STUB_DEC(stub_10_4)
_STUB_DEC(stub_10_6)
_STUB_DEC(stub_11_0)
_STUB_DEC(stub_11_2)
_STUB_DEC(stub_11_4)
_STUB_DEC(stub_11_6)
_STUB_DEC(stub_12_0)
_STUB_DEC(stub_12_2)
_STUB_DEC(stub_12_4)
_STUB_DEC(stub_12_6)
_STUB_DEC(stub_13_0)
_STUB_DEC(stub_13_2)
_STUB_DEC(stub_13_4)
_STUB_DEC(stub_13_6)
_STUB_DEC(stub_14_0)
_STUB_DEC(stub_14_2)
_STUB_DEC(stub_14_4)
_STUB_DEC(stub_14_6)
_STUB_DEC(stub_15_0)
_STUB_DEC(stub_15_2)
_STUB_DEC(stub_15_4)
_STUB_DEC(stub_15_6)
_STUB_DEC(stub_16_0)
_STUB_DEC(stub_16_2)
_STUB_DEC(stub_16_4)
_STUB_DEC(stub_16_6)
_STUB_DEC(stub_17_0)
_STUB_DEC(stub_17_2)
_STUB_DEC(stub_17_4)
_STUB_DEC(stub_17_6)
_STUB_DEC(stub_18_0)
_STUB_DEC(stub_18_2)
_STUB_DEC(stub_18_4)
_STUB_DEC(stub_18_6)
_STUB_DEC(stub_19_0)
_STUB_DEC(stub_19_2)
_STUB_DEC(stub_19_4)
_STUB_DEC(stub_19_6)
_STUB_DEC(stub_20_0)
_STUB_DEC(stub_20_2)
_STUB_DEC(stub_20_4)
_STUB_DEC(stub_20_6)
_STUB_DEC(stub_21_0)
_STUB_DEC(stub_21_2)
_STUB_DEC(stub_21_4)
_STUB_DEC(stub_21_6)
_STUB_DEC(stub_22_0)
_STUB_DEC(stub_22_2)
_STUB_DEC(stub_22_4)
_STUB_DEC(stub_22_6)
_STUB_DEC(stub_23_0)
_STUB_DEC(stub_23_2)
_STUB_DEC(stub_23_4)
_STUB_DEC(stub_23_6)
_STUB_DEC(stub_24_0)
_STUB_DEC(stub_24_2)
_STUB_DEC(stub_24_4)
_STUB_DEC(stub_24_6)
_STUB_DEC(stub_25_0)
_STUB_DEC(stub_25_2)
_STUB_DEC(stub_25_4)
_STUB_DEC(stub_25_6)
_STUB_DEC(stub_26_0)
_STUB_DEC(stub_26_2)
_STUB_DEC(stub_26_4)
_STUB_DEC(stub_26_6)
_STUB_DEC(stub_27_0)
_STUB_DEC(stub_27_2)
_STUB_DEC(stub_27_4)
_STUB_DEC(stub_27_6)
_STUB_DEC(stub_28_0)
_STUB_DEC(stub_28_2)
_STUB_DEC(stub_28_4)
_STUB_DEC(stub_28_6)
_STUB_DEC(stub_29_0)
_STUB_DEC(stub_29_2)
_STUB_DEC(stub_29_4)
_STUB_DEC(stub_29_6)
_STUB_DEC(stub_30_0)
_STUB_DEC(stub_30_2)
_STUB_DEC(stub_30_4)
_STUB_DEC(stub_30_6)
_STUB_DEC(stub_31_0)
_STUB_DEC(stub_31_2)
_STUB_DEC(stub_31_4)
_STUB_DEC(stub_31_6)
_STUB_DEC(stub_32_0)
_STUB_DEC(stub_32_2)
_STUB_DEC(stub_32_4)
_STUB_DEC(stub_32_6)
_STUB_DEC(stub_33_0)
_STUB_DEC(stub_33_2)
_STUB_DEC(stub_33_4)
_STUB_DEC(stub_33_6)
_STUB_DEC(stub_34_0)
_STUB_DEC(stub_34_2)
_STUB_DEC(stub_34_4)
_STUB_DEC(stub_34_6)
_STUB_DEC(stub_35_0)
_STUB_DEC(stub_35_2)
_STUB_DEC(stub_35_4)
_STUB_DEC(stub_35_6)
_STUB_DEC(stub_36_0)
_STUB_DEC(stub_36_2)
_STUB_DEC(stub_36_4)
_STUB_DEC(stub_36_6)
_STUB_DEC(stub_37_0)
_STUB_DEC(stub_37_2)
_STUB_DEC(stub_37_4)
_STUB_DEC(stub_37_6)
_STUB_DEC(stub_38_0)
_STUB_DEC(stub_38_2)
_STUB_DEC(stub_38_4)
_STUB_DEC(stub_38_6)
_STUB_DEC(stub_39_0)
_STUB_DEC(stub_39_2)
_STUB_DEC(stub_39_4)
_STUB_DEC(stub_39_6)
_STUB_DEC(stub_40_0)
_STUB_DEC(stub_40_2)
_STUB_DEC(stub_40_4)
_STUB_DEC(stub_40_6)
_STUB_DEC(stub_41_0)
_STUB_DEC(stub_41_2)
_STUB_DEC(stub_41_4)
_STUB_DEC(stub_41_6)
_STUB_DEC(stub_42_0)
_STUB_DEC(stub_42_2)
_STUB_DEC(stub_42_4)
_STUB_DEC(stub_42_6)
_STUB_DEC(stub_43_0)
_STUB_DEC(stub_43_2)
_STUB_DEC(stub_43_4)
_STUB_DEC(stub_43_6)
_STUB_DEC(stub_44_0)
_STUB_DEC(stub_44_2)
_STUB_DEC(stub_44_4)
_STUB_DEC(stub_44_6)
_STUB_DEC(stub_45_0)
_STUB_DEC(stub_45_2)
_STUB_DEC(stub_45_4)
_STUB_DEC(stub_45_6)
_STUB_DEC(stub_46_0)
_STUB_DEC(stub_46_2)
_STUB_DEC(stub_46_4)
_STUB_DEC(stub_46_6)
_STUB_DEC(stub_47_0)
_STUB_DEC(stub_47_2)
_STUB_DEC(stub_47_4)
_STUB_DEC(stub_47_6)
_STUB_DEC(stub_48_0)
_STUB_DEC(stub_48_2)
_STUB_DEC(stub_48_4)
_STUB_DEC(stub_48_6)
_STUB_DEC(stub_49_0)
_STUB_DEC(stub_49_2)
_STUB_DEC(stub_49_4)
_STUB_DEC(stub_49_6)
_STUB_DEC(stub_50_0)
_STUB_DEC(stub_50_2)
_STUB_DEC(stub_50_4)
_STUB_DEC(stub_50_6)
_STUB_DEC(stub_51_0)
_STUB_DEC(stub_51_2)
_STUB_DEC(stub_51_4)
_STUB_DEC(stub_51_6)
_STUB_DEC(stub_52_0)
_STUB_DEC(stub_52_2)
_STUB_DEC(stub_52_4)
_STUB_DEC(stub_52_6)
_STUB_DEC(stub_53_0)
_STUB_DEC(stub_53_2)
_STUB_DEC(stub_53_4)
_STUB_DEC(stub_53_6)
_STUB_DEC(stub_54_0)
_STUB_DEC(stub_54_2)
_STUB_DEC(stub_54_4)
_STUB_DEC(stub_54_6)
_STUB_DEC(stub_55_0)
_STUB_DEC(stub_55_2)
_STUB_DEC(stub_55_4)
_STUB_DEC(stub_55_6)
_STUB_DEC(stub_56_0)
_STUB_DEC(stub_56_2)
_STUB_DEC(stub_56_4)
_STUB_DEC(stub_56_6)
_STUB_DEC(stub_57_0)
_STUB_DEC(stub_57_2)
_STUB_DEC(stub_57_4)
_STUB_DEC(stub_57_6)
_STUB_DEC(stub_58_0)
_STUB_DEC(stub_58_2)
_STUB_DEC(stub_58_4)
_STUB_DEC(stub_58_6)
_STUB_DEC(stub_59_0)
_STUB_DEC(stub_59_2)
_STUB_DEC(stub_59_4)
_STUB_DEC(stub_59_6)
_STUB_DEC(stub_60_0)
_STUB_DEC(stub_60_2)
_STUB_DEC(stub_60_4)
_STUB_DEC(stub_60_6)

/*
Table begin: index=0 size=1289
Mask=0x0ff00ff0	DMask=0x00000000	CMask=0x08000000	Sig=0x03000000
Prob=549.005	Entropy=9.09208418	HTreeHeight=9.1275125
-------------------------------------------------------------------
mask=0x0f000000	sig=0x0a000000	prob=0.00182147704	name=DEC_fetch_oper_branch_
mask=0x0f000000	sig=0x0b000000	prob=0.00182147704	name=DEC_fetch_oper_branch_link_
mask=0x0f000200	sig=0x0d000000	prob=0.000455369259	name=DEC_fetch_oper_fpe_
mask=0x0f000400	sig=0x0d000200	prob=0.000455369259	name=DEC_fetch_oper_fpe_
mask=0x0f000800	sig=0x0d000600	prob=0.000455369259	name=DEC_fetch_oper_fpe_
mask=0x0f100200	sig=0x0d100c00	prob=0.000151789753	name=DEC_fetch_oper_fpe_
mask=0x0f100400	sig=0x0d100a00	prob=0.000151789753	name=DEC_fetch_oper_fpe_
mask=0x0f100800	sig=0x0d100600	prob=0.000151789753	name=DEC_fetch_oper_fpe_
mask=0x0f100e00	sig=0x0d000e00	prob=0.00182147704	name=DEC_fetch_oper_coproc_st_
mask=0x0ff00000	sig=0x02a00000	prob=0.00182147704	name=DEC_fetch_oper_dpi_imm_binop_adc_
mask=0x0ff00000	sig=0x02b00000	prob=0.00182147704	name=DEC_fetch_oper_dpi_imm_binop_adcs_
mask=0x0ff00000	sig=0x02800000	prob=0.00182147704	name=DEC_fetch_oper_dpi_imm_binop_add_
mask=0x0ff00000	sig=0x02900000	prob=0.00182147704	name=DEC_fetch_oper_dpi_imm_binop_adds_
mask=0x0ff00000	sig=0x02000000	prob=0.00182147704	name=DEC_fetch_oper_dpi_imm_binop_and_
mask=0x0ff00000	sig=0x02100000	prob=0.00182147704	name=DEC_fetch_oper_dpi_imm_binop_ands_
mask=0x0ff00000	sig=0x03c00000	prob=0.00182147704	name=DEC_fetch_oper_dpi_imm_binop_bic_
mask=0x0ff00000	sig=0x03d00000	prob=0.00182147704	name=DEC_fetch_oper_dpi_imm_binop_bics_
mask=0x0ff00000	sig=0x02200000	prob=0.00182147704	name=DEC_fetch_oper_dpi_imm_binop_eor_
mask=0x0ff00000	sig=0x02300000	prob=0.00182147704	name=DEC_fetch_oper_dpi_imm_binop_eors_
mask=0x0ff00000	sig=0x03800000	prob=0.00182147704	name=DEC_fetch_oper_dpi_imm_binop_orr_
mask=0x0ff00000	sig=0x03900000	prob=0.00182147704	name=DEC_fetch_oper_dpi_imm_binop_orrs_
mask=0x0ff00000	sig=0x02600000	prob=0.00182147704	name=DEC_fetch_oper_dpi_imm_binop_rsb_
mask=0x0ff00000	sig=0x02700000	prob=0.00182147704	name=DEC_fetch_oper_dpi_imm_binop_rsbs_
mask=0x0ff00000	sig=0x02e00000	prob=0.00182147704	name=DEC_fetch_oper_dpi_imm_binop_rsc_
mask=0x0ff00000	sig=0x02f00000	prob=0.00182147704	name=DEC_fetch_oper_dpi_imm_binop_rscs_
mask=0x0ff00000	sig=0x02c00000	prob=0.00182147704	name=DEC_fetch_oper_dpi_imm_binop_sbc_
mask=0x0ff00000	sig=0x02d00000	prob=0.00182147704	name=DEC_fetch_oper_dpi_imm_binop_sbcs_
mask=0x0ff00000	sig=0x02400000	prob=0.00182147704	name=DEC_fetch_oper_dpi_imm_binop_sub_
mask=0x0ff00000	sig=0x02500000	prob=0.00182147704	name=DEC_fetch_oper_dpi_imm_binop_subs_
mask=0x0ff00000	sig=0x03700000	prob=0.00182147704	name=DEC_fetch_oper_dpi_imm_tst_cmn_
mask=0x0ff00000	sig=0x03500000	prob=0.00182147704	name=DEC_fetch_oper_dpi_imm_tst_cmp_
mask=0x0ff00000	sig=0x03300000	prob=0.00182147704	name=DEC_fetch_oper_dpi_imm_tst_teq_
mask=0x0ff00000	sig=0x03100000	prob=0.00182147704	name=DEC_fetch_oper_dpi_imm_tst_tst_
mask=0x0ff00000	sig=0x03a00000	prob=0.00182147704	name=DEC_fetch_oper_dpi_imm_unop_mov_
mask=0x0ff00000	sig=0x03b00000	prob=0.00182147704	name=DEC_fetch_oper_dpi_imm_unop_movs_
mask=0x0ff00000	sig=0x03e00000	prob=0.00182147704	name=DEC_fetch_oper_dpi_imm_unop_mvn_
mask=0x0ff00000	sig=0x03f00000	prob=0.00182147704	name=DEC_fetch_oper_dpi_imm_unop_mvns_
mask=0x0ff000f0	sig=0x00a000c0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_adc_
mask=0x0ff00170	sig=0x00a00140	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_adc_
mask=0x0ff00270	sig=0x00a00240	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_adc_
mask=0x0ff00470	sig=0x00a00440	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_adc_
mask=0x0ff00870	sig=0x00a00840	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_adc_
mask=0x0ff000f0	sig=0x00b000c0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_adcs_
mask=0x0ff00170	sig=0x00b00140	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_adcs_
mask=0x0ff00270	sig=0x00b00240	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_adcs_
mask=0x0ff00470	sig=0x00b00440	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_adcs_
mask=0x0ff00870	sig=0x00b00840	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_adcs_
mask=0x0ff000f0	sig=0x008000c0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_add_
mask=0x0ff00170	sig=0x00800140	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_add_
mask=0x0ff00270	sig=0x00800240	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_add_
mask=0x0ff00470	sig=0x00800440	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_add_
mask=0x0ff00870	sig=0x00800840	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_add_
mask=0x0ff000f0	sig=0x009000c0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_adds_
mask=0x0ff00170	sig=0x00900140	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_adds_
mask=0x0ff00270	sig=0x00900240	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_adds_
mask=0x0ff00470	sig=0x00900440	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_adds_
mask=0x0ff00870	sig=0x00900840	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_adds_
mask=0x0ff000f0	sig=0x000000c0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_and_
mask=0x0ff00170	sig=0x00000140	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_and_
mask=0x0ff00270	sig=0x00000240	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_and_
mask=0x0ff00470	sig=0x00000440	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_and_
mask=0x0ff00870	sig=0x00000840	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_and_
mask=0x0ff000f0	sig=0x001000c0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_ands_
mask=0x0ff00170	sig=0x00100140	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_ands_
mask=0x0ff00270	sig=0x00100240	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_ands_
mask=0x0ff00470	sig=0x00100440	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_ands_
mask=0x0ff00870	sig=0x00100840	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_ands_
mask=0x0ff000f0	sig=0x01c000c0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_bic_
mask=0x0ff00170	sig=0x01c00140	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_bic_
mask=0x0ff00270	sig=0x01c00240	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_bic_
mask=0x0ff00470	sig=0x01c00440	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_bic_
mask=0x0ff00870	sig=0x01c00840	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_bic_
mask=0x0ff000f0	sig=0x01d000c0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_bics_
mask=0x0ff00170	sig=0x01d00140	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_bics_
mask=0x0ff00270	sig=0x01d00240	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_bics_
mask=0x0ff00470	sig=0x01d00440	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_bics_
mask=0x0ff00870	sig=0x01d00840	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_bics_
mask=0x0ff000f0	sig=0x002000c0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_eor_
mask=0x0ff00170	sig=0x00200140	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_eor_
mask=0x0ff00270	sig=0x00200240	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_eor_
mask=0x0ff00470	sig=0x00200440	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_eor_
mask=0x0ff00870	sig=0x00200840	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_eor_
mask=0x0ff000f0	sig=0x003000c0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_eors_
mask=0x0ff00170	sig=0x00300140	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_eors_
mask=0x0ff00270	sig=0x00300240	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_eors_
mask=0x0ff00470	sig=0x00300440	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_eors_
mask=0x0ff00870	sig=0x00300840	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_eors_
mask=0x0ff000f0	sig=0x018000c0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_orr_
mask=0x0ff00170	sig=0x01800140	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_orr_
mask=0x0ff00270	sig=0x01800240	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_orr_
mask=0x0ff00470	sig=0x01800440	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_orr_
mask=0x0ff00870	sig=0x01800840	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_orr_
mask=0x0ff000f0	sig=0x019000c0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_orrs_
mask=0x0ff00170	sig=0x01900140	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_orrs_
mask=0x0ff00270	sig=0x01900240	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_orrs_
mask=0x0ff00470	sig=0x01900440	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_orrs_
mask=0x0ff00870	sig=0x01900840	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_orrs_
mask=0x0ff000f0	sig=0x006000c0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_rsb_
mask=0x0ff00170	sig=0x00600140	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_rsb_
mask=0x0ff00270	sig=0x00600240	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_rsb_
mask=0x0ff00470	sig=0x00600440	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_rsb_
mask=0x0ff00870	sig=0x00600840	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_rsb_
mask=0x0ff000f0	sig=0x007000c0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_rsbs_
mask=0x0ff00170	sig=0x00700140	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_rsbs_
mask=0x0ff00270	sig=0x00700240	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_rsbs_
mask=0x0ff00470	sig=0x00700440	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_rsbs_
mask=0x0ff00870	sig=0x00700840	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_rsbs_
mask=0x0ff000f0	sig=0x00e000c0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_rsc_
mask=0x0ff00170	sig=0x00e00140	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_rsc_
mask=0x0ff00270	sig=0x00e00240	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_rsc_
mask=0x0ff00470	sig=0x00e00440	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_rsc_
mask=0x0ff00870	sig=0x00e00840	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_rsc_
mask=0x0ff000f0	sig=0x00f000c0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_rscs_
mask=0x0ff00170	sig=0x00f00140	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_rscs_
mask=0x0ff00270	sig=0x00f00240	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_rscs_
mask=0x0ff00470	sig=0x00f00440	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_rscs_
mask=0x0ff00870	sig=0x00f00840	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_rscs_
mask=0x0ff000f0	sig=0x00c000c0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_sbc_
mask=0x0ff00170	sig=0x00c00140	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_sbc_
mask=0x0ff00270	sig=0x00c00240	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_sbc_
mask=0x0ff00470	sig=0x00c00440	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_sbc_
mask=0x0ff00870	sig=0x00c00840	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_sbc_
mask=0x0ff000f0	sig=0x00d000c0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_sbcs_
mask=0x0ff00170	sig=0x00d00140	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_sbcs_
mask=0x0ff00270	sig=0x00d00240	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_sbcs_
mask=0x0ff00470	sig=0x00d00440	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_sbcs_
mask=0x0ff00870	sig=0x00d00840	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_sbcs_
mask=0x0ff000f0	sig=0x004000c0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_sub_
mask=0x0ff00170	sig=0x00400140	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_sub_
mask=0x0ff00270	sig=0x00400240	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_sub_
mask=0x0ff00470	sig=0x00400440	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_sub_
mask=0x0ff00870	sig=0x00400840	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_sub_
mask=0x0ff000f0	sig=0x005000c0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_subs_
mask=0x0ff00170	sig=0x00500140	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_subs_
mask=0x0ff00270	sig=0x00500240	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_subs_
mask=0x0ff00470	sig=0x00500440	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_subs_
mask=0x0ff00870	sig=0x00500840	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_subs_
mask=0x0ff000f0	sig=0x00a00080	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_adc_
mask=0x0ff00170	sig=0x00a00100	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_adc_
mask=0x0ff00270	sig=0x00a00200	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_adc_
mask=0x0ff00470	sig=0x00a00400	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_adc_
mask=0x0ff00870	sig=0x00a00800	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_adc_
mask=0x0ff000f0	sig=0x00b00080	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_adcs_
mask=0x0ff00170	sig=0x00b00100	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_adcs_
mask=0x0ff00270	sig=0x00b00200	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_adcs_
mask=0x0ff00470	sig=0x00b00400	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_adcs_
mask=0x0ff00870	sig=0x00b00800	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_adcs_
mask=0x0ff000f0	sig=0x00800080	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_add_
mask=0x0ff00170	sig=0x00800100	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_add_
mask=0x0ff00270	sig=0x00800200	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_add_
mask=0x0ff00470	sig=0x00800400	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_add_
mask=0x0ff00870	sig=0x00800800	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_add_
mask=0x0ff000f0	sig=0x00900080	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_adds_
mask=0x0ff00170	sig=0x00900100	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_adds_
mask=0x0ff00270	sig=0x00900200	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_adds_
mask=0x0ff00470	sig=0x00900400	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_adds_
mask=0x0ff00870	sig=0x00900800	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_adds_
mask=0x0ff000f0	sig=0x00000080	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_and_
mask=0x0ff00170	sig=0x00000100	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_and_
mask=0x0ff00270	sig=0x00000200	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_and_
mask=0x0ff00470	sig=0x00000400	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_and_
mask=0x0ff00870	sig=0x00000800	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_and_
mask=0x0ff000f0	sig=0x00100080	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_ands_
mask=0x0ff00170	sig=0x00100100	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_ands_
mask=0x0ff00270	sig=0x00100200	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_ands_
mask=0x0ff00470	sig=0x00100400	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_ands_
mask=0x0ff00870	sig=0x00100800	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_ands_
mask=0x0ff000f0	sig=0x01c00080	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_bic_
mask=0x0ff00170	sig=0x01c00100	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_bic_
mask=0x0ff00270	sig=0x01c00200	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_bic_
mask=0x0ff00470	sig=0x01c00400	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_bic_
mask=0x0ff00870	sig=0x01c00800	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_bic_
mask=0x0ff000f0	sig=0x01d00080	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_bics_
mask=0x0ff00170	sig=0x01d00100	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_bics_
mask=0x0ff00270	sig=0x01d00200	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_bics_
mask=0x0ff00470	sig=0x01d00400	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_bics_
mask=0x0ff00870	sig=0x01d00800	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_bics_
mask=0x0ff000f0	sig=0x00200080	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_eor_
mask=0x0ff00170	sig=0x00200100	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_eor_
mask=0x0ff00270	sig=0x00200200	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_eor_
mask=0x0ff00470	sig=0x00200400	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_eor_
mask=0x0ff00870	sig=0x00200800	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_eor_
mask=0x0ff000f0	sig=0x00300080	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_eors_
mask=0x0ff00170	sig=0x00300100	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_eors_
mask=0x0ff00270	sig=0x00300200	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_eors_
mask=0x0ff00470	sig=0x00300400	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_eors_
mask=0x0ff00870	sig=0x00300800	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_eors_
mask=0x0ff000f0	sig=0x01800080	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_orr_
mask=0x0ff00170	sig=0x01800100	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_orr_
mask=0x0ff00270	sig=0x01800200	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_orr_
mask=0x0ff00470	sig=0x01800400	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_orr_
mask=0x0ff00870	sig=0x01800800	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_orr_
mask=0x0ff000f0	sig=0x01900080	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_orrs_
mask=0x0ff00170	sig=0x01900100	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_orrs_
mask=0x0ff00270	sig=0x01900200	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_orrs_
mask=0x0ff00470	sig=0x01900400	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_orrs_
mask=0x0ff00870	sig=0x01900800	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_orrs_
mask=0x0ff000f0	sig=0x00600080	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_rsb_
mask=0x0ff00170	sig=0x00600100	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_rsb_
mask=0x0ff00270	sig=0x00600200	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_rsb_
mask=0x0ff00470	sig=0x00600400	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_rsb_
mask=0x0ff00870	sig=0x00600800	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_rsb_
mask=0x0ff000f0	sig=0x00700080	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_rsbs_
mask=0x0ff00170	sig=0x00700100	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_rsbs_
mask=0x0ff00270	sig=0x00700200	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_rsbs_
mask=0x0ff00470	sig=0x00700400	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_rsbs_
mask=0x0ff00870	sig=0x00700800	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_rsbs_
mask=0x0ff000f0	sig=0x00e00080	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_rsc_
mask=0x0ff00170	sig=0x00e00100	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_rsc_
mask=0x0ff00270	sig=0x00e00200	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_rsc_
mask=0x0ff00470	sig=0x00e00400	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_rsc_
mask=0x0ff00870	sig=0x00e00800	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_rsc_
mask=0x0ff000f0	sig=0x00f00080	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_rscs_
mask=0x0ff00170	sig=0x00f00100	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_rscs_
mask=0x0ff00270	sig=0x00f00200	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_rscs_
mask=0x0ff00470	sig=0x00f00400	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_rscs_
mask=0x0ff00870	sig=0x00f00800	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_rscs_
mask=0x0ff000f0	sig=0x00c00080	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_sbc_
mask=0x0ff00170	sig=0x00c00100	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_sbc_
mask=0x0ff00270	sig=0x00c00200	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_sbc_
mask=0x0ff00470	sig=0x00c00400	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_sbc_
mask=0x0ff00870	sig=0x00c00800	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_sbc_
mask=0x0ff000f0	sig=0x00d00080	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_sbcs_
mask=0x0ff00170	sig=0x00d00100	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_sbcs_
mask=0x0ff00270	sig=0x00d00200	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_sbcs_
mask=0x0ff00470	sig=0x00d00400	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_sbcs_
mask=0x0ff00870	sig=0x00d00800	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_sbcs_
mask=0x0ff000f0	sig=0x00400080	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_sub_
mask=0x0ff00170	sig=0x00400100	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_sub_
mask=0x0ff00270	sig=0x00400200	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_sub_
mask=0x0ff00470	sig=0x00400400	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_sub_
mask=0x0ff00870	sig=0x00400800	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_sub_
mask=0x0ff000f0	sig=0x00500080	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_subs_
mask=0x0ff00170	sig=0x00500100	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_subs_
mask=0x0ff00270	sig=0x00500200	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_subs_
mask=0x0ff00470	sig=0x00500400	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_subs_
mask=0x0ff00870	sig=0x00500800	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_subs_
mask=0x0ff000f0	sig=0x00a000a0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_adc_
mask=0x0ff00170	sig=0x00a00120	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_adc_
mask=0x0ff00270	sig=0x00a00220	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_adc_
mask=0x0ff00470	sig=0x00a00420	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_adc_
mask=0x0ff00870	sig=0x00a00820	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_adc_
mask=0x0ff000f0	sig=0x00b000a0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_adcs_
mask=0x0ff00170	sig=0x00b00120	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_adcs_
mask=0x0ff00270	sig=0x00b00220	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_adcs_
mask=0x0ff00470	sig=0x00b00420	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_adcs_
mask=0x0ff00870	sig=0x00b00820	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_adcs_
mask=0x0ff000f0	sig=0x008000a0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_add_
mask=0x0ff00170	sig=0x00800120	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_add_
mask=0x0ff00270	sig=0x00800220	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_add_
mask=0x0ff00470	sig=0x00800420	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_add_
mask=0x0ff00870	sig=0x00800820	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_add_
mask=0x0ff000f0	sig=0x009000a0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_adds_
mask=0x0ff00170	sig=0x00900120	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_adds_
mask=0x0ff00270	sig=0x00900220	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_adds_
mask=0x0ff00470	sig=0x00900420	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_adds_
mask=0x0ff00870	sig=0x00900820	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_adds_
mask=0x0ff000f0	sig=0x000000a0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_and_
mask=0x0ff00170	sig=0x00000120	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_and_
mask=0x0ff00270	sig=0x00000220	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_and_
mask=0x0ff00470	sig=0x00000420	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_and_
mask=0x0ff00870	sig=0x00000820	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_and_
mask=0x0ff000f0	sig=0x001000a0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_ands_
mask=0x0ff00170	sig=0x00100120	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_ands_
mask=0x0ff00270	sig=0x00100220	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_ands_
mask=0x0ff00470	sig=0x00100420	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_ands_
mask=0x0ff00870	sig=0x00100820	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_ands_
mask=0x0ff000f0	sig=0x01c000a0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_bic_
mask=0x0ff00170	sig=0x01c00120	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_bic_
mask=0x0ff00270	sig=0x01c00220	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_bic_
mask=0x0ff00470	sig=0x01c00420	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_bic_
mask=0x0ff00870	sig=0x01c00820	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_bic_
mask=0x0ff000f0	sig=0x01d000a0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_bics_
mask=0x0ff00170	sig=0x01d00120	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_bics_
mask=0x0ff00270	sig=0x01d00220	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_bics_
mask=0x0ff00470	sig=0x01d00420	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_bics_
mask=0x0ff00870	sig=0x01d00820	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_bics_
mask=0x0ff000f0	sig=0x002000a0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_eor_
mask=0x0ff00170	sig=0x00200120	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_eor_
mask=0x0ff00270	sig=0x00200220	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_eor_
mask=0x0ff00470	sig=0x00200420	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_eor_
mask=0x0ff00870	sig=0x00200820	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_eor_
mask=0x0ff000f0	sig=0x003000a0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_eors_
mask=0x0ff00170	sig=0x00300120	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_eors_
mask=0x0ff00270	sig=0x00300220	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_eors_
mask=0x0ff00470	sig=0x00300420	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_eors_
mask=0x0ff00870	sig=0x00300820	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_eors_
mask=0x0ff000f0	sig=0x018000a0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_orr_
mask=0x0ff00170	sig=0x01800120	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_orr_
mask=0x0ff00270	sig=0x01800220	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_orr_
mask=0x0ff00470	sig=0x01800420	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_orr_
mask=0x0ff00870	sig=0x01800820	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_orr_
mask=0x0ff000f0	sig=0x019000a0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_orrs_
mask=0x0ff00170	sig=0x01900120	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_orrs_
mask=0x0ff00270	sig=0x01900220	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_orrs_
mask=0x0ff00470	sig=0x01900420	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_orrs_
mask=0x0ff00870	sig=0x01900820	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_orrs_
mask=0x0ff000f0	sig=0x006000a0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_rsb_
mask=0x0ff00170	sig=0x00600120	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_rsb_
mask=0x0ff00270	sig=0x00600220	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_rsb_
mask=0x0ff00470	sig=0x00600420	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_rsb_
mask=0x0ff00870	sig=0x00600820	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_rsb_
mask=0x0ff000f0	sig=0x007000a0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_rsbs_
mask=0x0ff00170	sig=0x00700120	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_rsbs_
mask=0x0ff00270	sig=0x00700220	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_rsbs_
mask=0x0ff00470	sig=0x00700420	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_rsbs_
mask=0x0ff00870	sig=0x00700820	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_rsbs_
mask=0x0ff000f0	sig=0x00e000a0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_rsc_
mask=0x0ff00170	sig=0x00e00120	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_rsc_
mask=0x0ff00270	sig=0x00e00220	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_rsc_
mask=0x0ff00470	sig=0x00e00420	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_rsc_
mask=0x0ff00870	sig=0x00e00820	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_rsc_
mask=0x0ff000f0	sig=0x00f000a0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_rscs_
mask=0x0ff00170	sig=0x00f00120	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_rscs_
mask=0x0ff00270	sig=0x00f00220	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_rscs_
mask=0x0ff00470	sig=0x00f00420	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_rscs_
mask=0x0ff00870	sig=0x00f00820	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_rscs_
mask=0x0ff000f0	sig=0x00c000a0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_sbc_
mask=0x0ff00170	sig=0x00c00120	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_sbc_
mask=0x0ff00270	sig=0x00c00220	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_sbc_
mask=0x0ff00470	sig=0x00c00420	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_sbc_
mask=0x0ff00870	sig=0x00c00820	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_sbc_
mask=0x0ff000f0	sig=0x00d000a0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_sbcs_
mask=0x0ff00170	sig=0x00d00120	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_sbcs_
mask=0x0ff00270	sig=0x00d00220	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_sbcs_
mask=0x0ff00470	sig=0x00d00420	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_sbcs_
mask=0x0ff00870	sig=0x00d00820	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_sbcs_
mask=0x0ff000f0	sig=0x004000a0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_sub_
mask=0x0ff00170	sig=0x00400120	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_sub_
mask=0x0ff00270	sig=0x00400220	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_sub_
mask=0x0ff00470	sig=0x00400420	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_sub_
mask=0x0ff00870	sig=0x00400820	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_sub_
mask=0x0ff000f0	sig=0x005000a0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_subs_
mask=0x0ff00170	sig=0x00500120	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_subs_
mask=0x0ff00270	sig=0x00500220	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_subs_
mask=0x0ff00470	sig=0x00500420	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_subs_
mask=0x0ff00870	sig=0x00500820	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_subs_
mask=0x0ff000f0	sig=0x00a000e0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_adc_
mask=0x0ff00170	sig=0x00a00160	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_adc_
mask=0x0ff00270	sig=0x00a00260	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_adc_
mask=0x0ff00470	sig=0x00a00460	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_adc_
mask=0x0ff00870	sig=0x00a00860	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_adc_
mask=0x0ff000f0	sig=0x00b000e0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_adcs_
mask=0x0ff00170	sig=0x00b00160	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_adcs_
mask=0x0ff00270	sig=0x00b00260	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_adcs_
mask=0x0ff00470	sig=0x00b00460	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_adcs_
mask=0x0ff00870	sig=0x00b00860	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_adcs_
mask=0x0ff000f0	sig=0x008000e0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_add_
mask=0x0ff00170	sig=0x00800160	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_add_
mask=0x0ff00270	sig=0x00800260	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_add_
mask=0x0ff00470	sig=0x00800460	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_add_
mask=0x0ff00870	sig=0x00800860	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_add_
mask=0x0ff000f0	sig=0x009000e0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_adds_
mask=0x0ff00170	sig=0x00900160	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_adds_
mask=0x0ff00270	sig=0x00900260	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_adds_
mask=0x0ff00470	sig=0x00900460	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_adds_
mask=0x0ff00870	sig=0x00900860	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_adds_
mask=0x0ff000f0	sig=0x000000e0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_and_
mask=0x0ff00170	sig=0x00000160	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_and_
mask=0x0ff00270	sig=0x00000260	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_and_
mask=0x0ff00470	sig=0x00000460	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_and_
mask=0x0ff00870	sig=0x00000860	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_and_
mask=0x0ff000f0	sig=0x001000e0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_ands_
mask=0x0ff00170	sig=0x00100160	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_ands_
mask=0x0ff00270	sig=0x00100260	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_ands_
mask=0x0ff00470	sig=0x00100460	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_ands_
mask=0x0ff00870	sig=0x00100860	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_ands_
mask=0x0ff000f0	sig=0x01c000e0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_bic_
mask=0x0ff00170	sig=0x01c00160	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_bic_
mask=0x0ff00270	sig=0x01c00260	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_bic_
mask=0x0ff00470	sig=0x01c00460	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_bic_
mask=0x0ff00870	sig=0x01c00860	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_bic_
mask=0x0ff000f0	sig=0x01d000e0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_bics_
mask=0x0ff00170	sig=0x01d00160	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_bics_
mask=0x0ff00270	sig=0x01d00260	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_bics_
mask=0x0ff00470	sig=0x01d00460	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_bics_
mask=0x0ff00870	sig=0x01d00860	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_bics_
mask=0x0ff000f0	sig=0x002000e0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_eor_
mask=0x0ff00170	sig=0x00200160	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_eor_
mask=0x0ff00270	sig=0x00200260	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_eor_
mask=0x0ff00470	sig=0x00200460	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_eor_
mask=0x0ff00870	sig=0x00200860	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_eor_
mask=0x0ff000f0	sig=0x003000e0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_eors_
mask=0x0ff00170	sig=0x00300160	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_eors_
mask=0x0ff00270	sig=0x00300260	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_eors_
mask=0x0ff00470	sig=0x00300460	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_eors_
mask=0x0ff00870	sig=0x00300860	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_eors_
mask=0x0ff000f0	sig=0x018000e0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_orr_
mask=0x0ff00170	sig=0x01800160	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_orr_
mask=0x0ff00270	sig=0x01800260	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_orr_
mask=0x0ff00470	sig=0x01800460	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_orr_
mask=0x0ff00870	sig=0x01800860	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_orr_
mask=0x0ff000f0	sig=0x019000e0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_orrs_
mask=0x0ff00170	sig=0x01900160	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_orrs_
mask=0x0ff00270	sig=0x01900260	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_orrs_
mask=0x0ff00470	sig=0x01900460	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_orrs_
mask=0x0ff00870	sig=0x01900860	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_orrs_
mask=0x0ff000f0	sig=0x006000e0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_rsb_
mask=0x0ff00170	sig=0x00600160	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_rsb_
mask=0x0ff00270	sig=0x00600260	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_rsb_
mask=0x0ff00470	sig=0x00600460	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_rsb_
mask=0x0ff00870	sig=0x00600860	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_rsb_
mask=0x0ff000f0	sig=0x007000e0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_rsbs_
mask=0x0ff00170	sig=0x00700160	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_rsbs_
mask=0x0ff00270	sig=0x00700260	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_rsbs_
mask=0x0ff00470	sig=0x00700460	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_rsbs_
mask=0x0ff00870	sig=0x00700860	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_rsbs_
mask=0x0ff000f0	sig=0x00e000e0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_rsc_
mask=0x0ff00170	sig=0x00e00160	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_rsc_
mask=0x0ff00270	sig=0x00e00260	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_rsc_
mask=0x0ff00470	sig=0x00e00460	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_rsc_
mask=0x0ff00870	sig=0x00e00860	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_rsc_
mask=0x0ff000f0	sig=0x00f000e0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_rscs_
mask=0x0ff00170	sig=0x00f00160	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_rscs_
mask=0x0ff00270	sig=0x00f00260	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_rscs_
mask=0x0ff00470	sig=0x00f00460	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_rscs_
mask=0x0ff00870	sig=0x00f00860	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_rscs_
mask=0x0ff000f0	sig=0x00c000e0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_sbc_
mask=0x0ff00170	sig=0x00c00160	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_sbc_
mask=0x0ff00270	sig=0x00c00260	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_sbc_
mask=0x0ff00470	sig=0x00c00460	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_sbc_
mask=0x0ff00870	sig=0x00c00860	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_sbc_
mask=0x0ff000f0	sig=0x00d000e0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_sbcs_
mask=0x0ff00170	sig=0x00d00160	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_sbcs_
mask=0x0ff00270	sig=0x00d00260	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_sbcs_
mask=0x0ff00470	sig=0x00d00460	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_sbcs_
mask=0x0ff00870	sig=0x00d00860	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_sbcs_
mask=0x0ff000f0	sig=0x004000e0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_sub_
mask=0x0ff00170	sig=0x00400160	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_sub_
mask=0x0ff00270	sig=0x00400260	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_sub_
mask=0x0ff00470	sig=0x00400460	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_sub_
mask=0x0ff00870	sig=0x00400860	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_sub_
mask=0x0ff000f0	sig=0x005000e0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_subs_
mask=0x0ff00170	sig=0x00500160	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_subs_
mask=0x0ff00270	sig=0x00500260	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_subs_
mask=0x0ff00470	sig=0x00500460	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_subs_
mask=0x0ff00870	sig=0x00500860	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_subs_
mask=0x0ff00ff0	sig=0x00a00040	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_adc_
mask=0x0ff00ff0	sig=0x00b00040	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_adcs_
mask=0x0ff00ff0	sig=0x00800040	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_add_
mask=0x0ff00ff0	sig=0x00900040	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_adds_
mask=0x0ff00ff0	sig=0x00000040	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_and_
mask=0x0ff00ff0	sig=0x00100040	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_ands_
mask=0x0ff00ff0	sig=0x01c00040	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_bic_
mask=0x0ff00ff0	sig=0x01d00040	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_bics_
mask=0x0ff00ff0	sig=0x00200040	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_eor_
mask=0x0ff00ff0	sig=0x00300040	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_eors_
mask=0x0ff00ff0	sig=0x01800040	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_orr_
mask=0x0ff00ff0	sig=0x01900040	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_orrs_
mask=0x0ff00ff0	sig=0x00600040	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_rsb_
mask=0x0ff00ff0	sig=0x00700040	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_rsbs_
mask=0x0ff00ff0	sig=0x00e00040	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_rsc_
mask=0x0ff00ff0	sig=0x00f00040	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_rscs_
mask=0x0ff00ff0	sig=0x00c00040	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_sbc_
mask=0x0ff00ff0	sig=0x00d00040	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_sbcs_
mask=0x0ff00ff0	sig=0x00400040	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_sub_
mask=0x0ff00ff0	sig=0x00500040	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_subs_
mask=0x0ff00ff0	sig=0x00a00000	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_adc_
mask=0x0ff00ff0	sig=0x00b00000	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_adcs_
mask=0x0ff00ff0	sig=0x00800000	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_add_
mask=0x0ff00ff0	sig=0x00900000	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_adds_
mask=0x0ff00ff0	sig=0x00000000	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_and_
mask=0x0ff00ff0	sig=0x00100000	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_ands_
mask=0x0ff00ff0	sig=0x01c00000	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_bic_
mask=0x0ff00ff0	sig=0x01d00000	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_bics_
mask=0x0ff00ff0	sig=0x00200000	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_eor_
mask=0x0ff00ff0	sig=0x00300000	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_eors_
mask=0x0ff00ff0	sig=0x01800000	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_orr_
mask=0x0ff00ff0	sig=0x01900000	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_orrs_
mask=0x0ff00ff0	sig=0x00600000	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_rsb_
mask=0x0ff00ff0	sig=0x00700000	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_rsbs_
mask=0x0ff00ff0	sig=0x00e00000	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_rsc_
mask=0x0ff00ff0	sig=0x00f00000	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_rscs_
mask=0x0ff00ff0	sig=0x00c00000	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_sbc_
mask=0x0ff00ff0	sig=0x00d00000	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_sbcs_
mask=0x0ff00ff0	sig=0x00400000	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_sub_
mask=0x0ff00ff0	sig=0x00500000	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_subs_
mask=0x0ff00ff0	sig=0x00a00020	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_adc_
mask=0x0ff00ff0	sig=0x00b00020	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_adcs_
mask=0x0ff00ff0	sig=0x00800020	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_add_
mask=0x0ff00ff0	sig=0x00900020	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_adds_
mask=0x0ff00ff0	sig=0x00000020	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_and_
mask=0x0ff00ff0	sig=0x00100020	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_ands_
mask=0x0ff00ff0	sig=0x01c00020	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_bic_
mask=0x0ff00ff0	sig=0x01d00020	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_bics_
mask=0x0ff00ff0	sig=0x00200020	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_eor_
mask=0x0ff00ff0	sig=0x00300020	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_eors_
mask=0x0ff00ff0	sig=0x01800020	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_orr_
mask=0x0ff00ff0	sig=0x01900020	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_orrs_
mask=0x0ff00ff0	sig=0x00600020	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_rsb_
mask=0x0ff00ff0	sig=0x00700020	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_rsbs_
mask=0x0ff00ff0	sig=0x00e00020	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_rsc_
mask=0x0ff00ff0	sig=0x00f00020	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_rscs_
mask=0x0ff00ff0	sig=0x00c00020	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_sbc_
mask=0x0ff00ff0	sig=0x00d00020	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_sbcs_
mask=0x0ff00ff0	sig=0x00400020	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_sub_
mask=0x0ff00ff0	sig=0x00500020	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_subs_
mask=0x0ff00ff0	sig=0x00a00060	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_adc_
mask=0x0ff00ff0	sig=0x00b00060	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_adcs_
mask=0x0ff00ff0	sig=0x00800060	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_add_
mask=0x0ff00ff0	sig=0x00900060	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_adds_
mask=0x0ff00ff0	sig=0x00000060	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_and_
mask=0x0ff00ff0	sig=0x00100060	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_ands_
mask=0x0ff00ff0	sig=0x01c00060	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_bic_
mask=0x0ff00ff0	sig=0x01d00060	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_bics_
mask=0x0ff00ff0	sig=0x00200060	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_eor_
mask=0x0ff00ff0	sig=0x00300060	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_eors_
mask=0x0ff00ff0	sig=0x01800060	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_orr_
mask=0x0ff00ff0	sig=0x01900060	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_orrs_
mask=0x0ff00ff0	sig=0x00600060	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_rsb_
mask=0x0ff00ff0	sig=0x00700060	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_rsbs_
mask=0x0ff00ff0	sig=0x00e00060	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_rsc_
mask=0x0ff00ff0	sig=0x00f00060	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_rscs_
mask=0x0ff00ff0	sig=0x00c00060	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_sbc_
mask=0x0ff00ff0	sig=0x00d00060	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_sbcs_
mask=0x0ff00ff0	sig=0x00400060	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_sub_
mask=0x0ff00ff0	sig=0x00500060	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_subs_
mask=0x0ff000f0	sig=0x017000c0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_asr_cmn_
mask=0x0ff00170	sig=0x01700140	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_asr_cmn_
mask=0x0ff00270	sig=0x01700240	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_asr_cmn_
mask=0x0ff00470	sig=0x01700440	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_asr_cmn_
mask=0x0ff00870	sig=0x01700840	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_asr_cmn_
mask=0x0ff000f0	sig=0x015000c0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_asr_cmp_
mask=0x0ff00170	sig=0x01500140	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_asr_cmp_
mask=0x0ff00270	sig=0x01500240	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_asr_cmp_
mask=0x0ff00470	sig=0x01500440	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_asr_cmp_
mask=0x0ff00870	sig=0x01500840	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_asr_cmp_
mask=0x0ff000f0	sig=0x013000c0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_asr_teq_
mask=0x0ff00170	sig=0x01300140	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_asr_teq_
mask=0x0ff00270	sig=0x01300240	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_asr_teq_
mask=0x0ff00470	sig=0x01300440	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_asr_teq_
mask=0x0ff00870	sig=0x01300840	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_asr_teq_
mask=0x0ff000f0	sig=0x011000c0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_asr_tst_
mask=0x0ff00170	sig=0x01100140	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_asr_tst_
mask=0x0ff00270	sig=0x01100240	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_asr_tst_
mask=0x0ff00470	sig=0x01100440	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_asr_tst_
mask=0x0ff00870	sig=0x01100840	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_asr_tst_
mask=0x0ff000f0	sig=0x01700080	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsl_cmn_
mask=0x0ff00170	sig=0x01700100	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsl_cmn_
mask=0x0ff00270	sig=0x01700200	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsl_cmn_
mask=0x0ff00470	sig=0x01700400	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsl_cmn_
mask=0x0ff00870	sig=0x01700800	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsl_cmn_
mask=0x0ff000f0	sig=0x01500080	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsl_cmp_
mask=0x0ff00170	sig=0x01500100	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsl_cmp_
mask=0x0ff00270	sig=0x01500200	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsl_cmp_
mask=0x0ff00470	sig=0x01500400	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsl_cmp_
mask=0x0ff00870	sig=0x01500800	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsl_cmp_
mask=0x0ff000f0	sig=0x01300080	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsl_teq_
mask=0x0ff00170	sig=0x01300100	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsl_teq_
mask=0x0ff00270	sig=0x01300200	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsl_teq_
mask=0x0ff00470	sig=0x01300400	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsl_teq_
mask=0x0ff00870	sig=0x01300800	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsl_teq_
mask=0x0ff000f0	sig=0x01100080	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsl_tst_
mask=0x0ff00170	sig=0x01100100	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsl_tst_
mask=0x0ff00270	sig=0x01100200	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsl_tst_
mask=0x0ff00470	sig=0x01100400	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsl_tst_
mask=0x0ff00870	sig=0x01100800	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsl_tst_
mask=0x0ff000f0	sig=0x017000a0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsr_cmn_
mask=0x0ff00170	sig=0x01700120	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsr_cmn_
mask=0x0ff00270	sig=0x01700220	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsr_cmn_
mask=0x0ff00470	sig=0x01700420	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsr_cmn_
mask=0x0ff00870	sig=0x01700820	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsr_cmn_
mask=0x0ff000f0	sig=0x015000a0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsr_cmp_
mask=0x0ff00170	sig=0x01500120	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsr_cmp_
mask=0x0ff00270	sig=0x01500220	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsr_cmp_
mask=0x0ff00470	sig=0x01500420	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsr_cmp_
mask=0x0ff00870	sig=0x01500820	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsr_cmp_
mask=0x0ff000f0	sig=0x013000a0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsr_teq_
mask=0x0ff00170	sig=0x01300120	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsr_teq_
mask=0x0ff00270	sig=0x01300220	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsr_teq_
mask=0x0ff00470	sig=0x01300420	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsr_teq_
mask=0x0ff00870	sig=0x01300820	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsr_teq_
mask=0x0ff000f0	sig=0x011000a0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsr_tst_
mask=0x0ff00170	sig=0x01100120	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsr_tst_
mask=0x0ff00270	sig=0x01100220	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsr_tst_
mask=0x0ff00470	sig=0x01100420	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsr_tst_
mask=0x0ff00870	sig=0x01100820	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsr_tst_
mask=0x0ff000f0	sig=0x017000e0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_ror_cmn_
mask=0x0ff00170	sig=0x01700160	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_ror_cmn_
mask=0x0ff00270	sig=0x01700260	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_ror_cmn_
mask=0x0ff00470	sig=0x01700460	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_ror_cmn_
mask=0x0ff00870	sig=0x01700860	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_ror_cmn_
mask=0x0ff000f0	sig=0x015000e0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_ror_cmp_
mask=0x0ff00170	sig=0x01500160	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_ror_cmp_
mask=0x0ff00270	sig=0x01500260	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_ror_cmp_
mask=0x0ff00470	sig=0x01500460	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_ror_cmp_
mask=0x0ff00870	sig=0x01500860	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_ror_cmp_
mask=0x0ff000f0	sig=0x013000e0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_ror_teq_
mask=0x0ff00170	sig=0x01300160	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_ror_teq_
mask=0x0ff00270	sig=0x01300260	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_ror_teq_
mask=0x0ff00470	sig=0x01300460	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_ror_teq_
mask=0x0ff00870	sig=0x01300860	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_ror_teq_
mask=0x0ff000f0	sig=0x011000e0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_ror_tst_
mask=0x0ff00170	sig=0x01100160	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_ror_tst_
mask=0x0ff00270	sig=0x01100260	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_ror_tst_
mask=0x0ff00470	sig=0x01100460	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_ror_tst_
mask=0x0ff00870	sig=0x01100860	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_ror_tst_
mask=0x0ff00ff0	sig=0x01700040	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_tst_zero_shift_zero_asr_cmn_
mask=0x0ff00ff0	sig=0x01500040	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_tst_zero_shift_zero_asr_cmp_
mask=0x0ff00ff0	sig=0x01300040	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_tst_zero_shift_zero_asr_teq_
mask=0x0ff00ff0	sig=0x01100040	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_tst_zero_shift_zero_asr_tst_
mask=0x0ff00ff0	sig=0x01700000	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_tst_zero_shift_zero_lsl_cmn_
mask=0x0ff00ff0	sig=0x01500000	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_tst_zero_shift_zero_lsl_cmp_
mask=0x0ff00ff0	sig=0x01300000	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_tst_zero_shift_zero_lsl_teq_
mask=0x0ff00ff0	sig=0x01100000	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_tst_zero_shift_zero_lsl_tst_
mask=0x0ff00ff0	sig=0x01700020	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_tst_zero_shift_zero_lsr_cmn_
mask=0x0ff00ff0	sig=0x01500020	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_tst_zero_shift_zero_lsr_cmp_
mask=0x0ff00ff0	sig=0x01300020	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_tst_zero_shift_zero_lsr_teq_
mask=0x0ff00ff0	sig=0x01100020	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_tst_zero_shift_zero_lsr_tst_
mask=0x0ff00ff0	sig=0x01700060	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_tst_zero_shift_zero_ror_cmn_
mask=0x0ff00ff0	sig=0x01500060	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_tst_zero_shift_zero_ror_cmp_
mask=0x0ff00ff0	sig=0x01300060	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_tst_zero_shift_zero_ror_teq_
mask=0x0ff00ff0	sig=0x01100060	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_tst_zero_shift_zero_ror_tst_
mask=0x0ff000f0	sig=0x01a000c0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_asr_mov_
mask=0x0ff00170	sig=0x01a00140	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_asr_mov_
mask=0x0ff00270	sig=0x01a00240	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_asr_mov_
mask=0x0ff00470	sig=0x01a00440	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_asr_mov_
mask=0x0ff00870	sig=0x01a00840	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_asr_mov_
mask=0x0ff000f0	sig=0x01b000c0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_asr_movs_
mask=0x0ff00170	sig=0x01b00140	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_asr_movs_
mask=0x0ff00270	sig=0x01b00240	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_asr_movs_
mask=0x0ff00470	sig=0x01b00440	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_asr_movs_
mask=0x0ff00870	sig=0x01b00840	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_asr_movs_
mask=0x0ff000f0	sig=0x01e000c0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_asr_mvn_
mask=0x0ff00170	sig=0x01e00140	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_asr_mvn_
mask=0x0ff00270	sig=0x01e00240	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_asr_mvn_
mask=0x0ff00470	sig=0x01e00440	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_asr_mvn_
mask=0x0ff00870	sig=0x01e00840	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_asr_mvn_
mask=0x0ff000f0	sig=0x01f000c0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_asr_mvns_
mask=0x0ff00170	sig=0x01f00140	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_asr_mvns_
mask=0x0ff00270	sig=0x01f00240	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_asr_mvns_
mask=0x0ff00470	sig=0x01f00440	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_asr_mvns_
mask=0x0ff00870	sig=0x01f00840	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_asr_mvns_
mask=0x0ff000f0	sig=0x01a00080	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsl_mov_
mask=0x0ff00170	sig=0x01a00100	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsl_mov_
mask=0x0ff00270	sig=0x01a00200	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsl_mov_
mask=0x0ff00470	sig=0x01a00400	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsl_mov_
mask=0x0ff00870	sig=0x01a00800	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsl_mov_
mask=0x0ff000f0	sig=0x01b00080	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsl_movs_
mask=0x0ff00170	sig=0x01b00100	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsl_movs_
mask=0x0ff00270	sig=0x01b00200	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsl_movs_
mask=0x0ff00470	sig=0x01b00400	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsl_movs_
mask=0x0ff00870	sig=0x01b00800	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsl_movs_
mask=0x0ff000f0	sig=0x01e00080	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsl_mvn_
mask=0x0ff00170	sig=0x01e00100	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsl_mvn_
mask=0x0ff00270	sig=0x01e00200	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsl_mvn_
mask=0x0ff00470	sig=0x01e00400	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsl_mvn_
mask=0x0ff00870	sig=0x01e00800	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsl_mvn_
mask=0x0ff000f0	sig=0x01f00080	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsl_mvns_
mask=0x0ff00170	sig=0x01f00100	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsl_mvns_
mask=0x0ff00270	sig=0x01f00200	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsl_mvns_
mask=0x0ff00470	sig=0x01f00400	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsl_mvns_
mask=0x0ff00870	sig=0x01f00800	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsl_mvns_
mask=0x0ff000f0	sig=0x01a000a0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsr_mov_
mask=0x0ff00170	sig=0x01a00120	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsr_mov_
mask=0x0ff00270	sig=0x01a00220	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsr_mov_
mask=0x0ff00470	sig=0x01a00420	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsr_mov_
mask=0x0ff00870	sig=0x01a00820	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsr_mov_
mask=0x0ff000f0	sig=0x01b000a0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsr_movs_
mask=0x0ff00170	sig=0x01b00120	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsr_movs_
mask=0x0ff00270	sig=0x01b00220	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsr_movs_
mask=0x0ff00470	sig=0x01b00420	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsr_movs_
mask=0x0ff00870	sig=0x01b00820	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsr_movs_
mask=0x0ff000f0	sig=0x01e000a0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsr_mvn_
mask=0x0ff00170	sig=0x01e00120	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsr_mvn_
mask=0x0ff00270	sig=0x01e00220	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsr_mvn_
mask=0x0ff00470	sig=0x01e00420	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsr_mvn_
mask=0x0ff00870	sig=0x01e00820	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsr_mvn_
mask=0x0ff000f0	sig=0x01f000a0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsr_mvns_
mask=0x0ff00170	sig=0x01f00120	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsr_mvns_
mask=0x0ff00270	sig=0x01f00220	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsr_mvns_
mask=0x0ff00470	sig=0x01f00420	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsr_mvns_
mask=0x0ff00870	sig=0x01f00820	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsr_mvns_
mask=0x0ff000f0	sig=0x01a000e0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_ror_mov_
mask=0x0ff00170	sig=0x01a00160	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_ror_mov_
mask=0x0ff00270	sig=0x01a00260	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_ror_mov_
mask=0x0ff00470	sig=0x01a00460	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_ror_mov_
mask=0x0ff00870	sig=0x01a00860	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_ror_mov_
mask=0x0ff000f0	sig=0x01b000e0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_ror_movs_
mask=0x0ff00170	sig=0x01b00160	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_ror_movs_
mask=0x0ff00270	sig=0x01b00260	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_ror_movs_
mask=0x0ff00470	sig=0x01b00460	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_ror_movs_
mask=0x0ff00870	sig=0x01b00860	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_ror_movs_
mask=0x0ff000f0	sig=0x01e000e0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_ror_mvn_
mask=0x0ff00170	sig=0x01e00160	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_ror_mvn_
mask=0x0ff00270	sig=0x01e00260	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_ror_mvn_
mask=0x0ff00470	sig=0x01e00460	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_ror_mvn_
mask=0x0ff00870	sig=0x01e00860	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_ror_mvn_
mask=0x0ff000f0	sig=0x01f000e0	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_ror_mvns_
mask=0x0ff00170	sig=0x01f00160	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_ror_mvns_
mask=0x0ff00270	sig=0x01f00260	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_ror_mvns_
mask=0x0ff00470	sig=0x01f00460	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_ror_mvns_
mask=0x0ff00870	sig=0x01f00860	prob=0.000364295407	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_ror_mvns_
mask=0x0ff00ff0	sig=0x01a00040	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_unop_zero_shift_zero_asr_mov_
mask=0x0ff00ff0	sig=0x01b00040	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_unop_zero_shift_zero_asr_movs_
mask=0x0ff00ff0	sig=0x01e00040	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_unop_zero_shift_zero_asr_mvn_
mask=0x0ff00ff0	sig=0x01f00040	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_unop_zero_shift_zero_asr_mvns_
mask=0x0ff00ff0	sig=0x01a00000	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_unop_zero_shift_zero_lsl_mov_
mask=0x0ff00ff0	sig=0x01b00000	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_unop_zero_shift_zero_lsl_movs_
mask=0x0ff00ff0	sig=0x01e00000	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_unop_zero_shift_zero_lsl_mvn_
mask=0x0ff00ff0	sig=0x01f00000	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_unop_zero_shift_zero_lsl_mvns_
mask=0x0ff00ff0	sig=0x01a00020	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_unop_zero_shift_zero_lsr_mov_
mask=0x0ff00ff0	sig=0x01b00020	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_unop_zero_shift_zero_lsr_movs_
mask=0x0ff00ff0	sig=0x01e00020	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_unop_zero_shift_zero_lsr_mvn_
mask=0x0ff00ff0	sig=0x01f00020	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_unop_zero_shift_zero_lsr_mvns_
mask=0x0ff00ff0	sig=0x01a00060	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_unop_zero_shift_zero_ror_mov_
mask=0x0ff00ff0	sig=0x01b00060	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_unop_zero_shift_zero_ror_movs_
mask=0x0ff00ff0	sig=0x01e00060	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_unop_zero_shift_zero_ror_mvn_
mask=0x0ff00ff0	sig=0x01f00060	prob=0.00182147704	name=DEC_fetch_oper_dpi_ishift_unop_zero_shift_zero_ror_mvns_
mask=0x0ff000f0	sig=0x00a00050	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_asr_adc_
mask=0x0ff000f0	sig=0x00b00050	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_asr_adcs_
mask=0x0ff000f0	sig=0x00800050	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_asr_add_
mask=0x0ff000f0	sig=0x00900050	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_asr_adds_
mask=0x0ff000f0	sig=0x00000050	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_asr_and_
mask=0x0ff000f0	sig=0x00100050	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_asr_ands_
mask=0x0ff000f0	sig=0x01c00050	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_asr_bic_
mask=0x0ff000f0	sig=0x01d00050	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_asr_bics_
mask=0x0ff000f0	sig=0x00200050	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_asr_eor_
mask=0x0ff000f0	sig=0x00300050	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_asr_eors_
mask=0x0ff000f0	sig=0x01800050	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_asr_orr_
mask=0x0ff000f0	sig=0x01900050	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_asr_orrs_
mask=0x0ff000f0	sig=0x00600050	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_asr_rsb_
mask=0x0ff000f0	sig=0x00700050	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_asr_rsbs_
mask=0x0ff000f0	sig=0x00e00050	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_asr_rsc_
mask=0x0ff000f0	sig=0x00f00050	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_asr_rscs_
mask=0x0ff000f0	sig=0x00c00050	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_asr_sbc_
mask=0x0ff000f0	sig=0x00d00050	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_asr_sbcs_
mask=0x0ff000f0	sig=0x00400050	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_asr_sub_
mask=0x0ff000f0	sig=0x00500050	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_asr_subs_
mask=0x0ff000f0	sig=0x00a00010	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_lsl_adc_
mask=0x0ff000f0	sig=0x00b00010	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_lsl_adcs_
mask=0x0ff000f0	sig=0x00800010	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_lsl_add_
mask=0x0ff000f0	sig=0x00900010	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_lsl_adds_
mask=0x0ff000f0	sig=0x00000010	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_lsl_and_
mask=0x0ff000f0	sig=0x00100010	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_lsl_ands_
mask=0x0ff000f0	sig=0x01c00010	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_lsl_bic_
mask=0x0ff000f0	sig=0x01d00010	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_lsl_bics_
mask=0x0ff000f0	sig=0x00200010	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_lsl_eor_
mask=0x0ff000f0	sig=0x00300010	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_lsl_eors_
mask=0x0ff000f0	sig=0x01800010	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_lsl_orr_
mask=0x0ff000f0	sig=0x01900010	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_lsl_orrs_
mask=0x0ff000f0	sig=0x00600010	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_lsl_rsb_
mask=0x0ff000f0	sig=0x00700010	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_lsl_rsbs_
mask=0x0ff000f0	sig=0x00e00010	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_lsl_rsc_
mask=0x0ff000f0	sig=0x00f00010	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_lsl_rscs_
mask=0x0ff000f0	sig=0x00c00010	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_lsl_sbc_
mask=0x0ff000f0	sig=0x00d00010	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_lsl_sbcs_
mask=0x0ff000f0	sig=0x00400010	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_lsl_sub_
mask=0x0ff000f0	sig=0x00500010	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_lsl_subs_
mask=0x0ff000f0	sig=0x00a00030	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_lsr_adc_
mask=0x0ff000f0	sig=0x00b00030	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_lsr_adcs_
mask=0x0ff000f0	sig=0x00800030	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_lsr_add_
mask=0x0ff000f0	sig=0x00900030	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_lsr_adds_
mask=0x0ff000f0	sig=0x00000030	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_lsr_and_
mask=0x0ff000f0	sig=0x00100030	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_lsr_ands_
mask=0x0ff000f0	sig=0x01c00030	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_lsr_bic_
mask=0x0ff000f0	sig=0x01d00030	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_lsr_bics_
mask=0x0ff000f0	sig=0x00200030	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_lsr_eor_
mask=0x0ff000f0	sig=0x00300030	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_lsr_eors_
mask=0x0ff000f0	sig=0x01800030	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_lsr_orr_
mask=0x0ff000f0	sig=0x01900030	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_lsr_orrs_
mask=0x0ff000f0	sig=0x00600030	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_lsr_rsb_
mask=0x0ff000f0	sig=0x00700030	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_lsr_rsbs_
mask=0x0ff000f0	sig=0x00e00030	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_lsr_rsc_
mask=0x0ff000f0	sig=0x00f00030	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_lsr_rscs_
mask=0x0ff000f0	sig=0x00c00030	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_lsr_sbc_
mask=0x0ff000f0	sig=0x00d00030	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_lsr_sbcs_
mask=0x0ff000f0	sig=0x00400030	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_lsr_sub_
mask=0x0ff000f0	sig=0x00500030	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_lsr_subs_
mask=0x0ff000f0	sig=0x00a00070	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_ror_adc_
mask=0x0ff000f0	sig=0x00b00070	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_ror_adcs_
mask=0x0ff000f0	sig=0x00800070	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_ror_add_
mask=0x0ff000f0	sig=0x00900070	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_ror_adds_
mask=0x0ff000f0	sig=0x00000070	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_ror_and_
mask=0x0ff000f0	sig=0x00100070	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_ror_ands_
mask=0x0ff000f0	sig=0x01c00070	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_ror_bic_
mask=0x0ff000f0	sig=0x01d00070	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_ror_bics_
mask=0x0ff000f0	sig=0x00200070	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_ror_eor_
mask=0x0ff000f0	sig=0x00300070	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_ror_eors_
mask=0x0ff000f0	sig=0x01800070	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_ror_orr_
mask=0x0ff000f0	sig=0x01900070	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_ror_orrs_
mask=0x0ff000f0	sig=0x00600070	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_ror_rsb_
mask=0x0ff000f0	sig=0x00700070	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_ror_rsbs_
mask=0x0ff000f0	sig=0x00e00070	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_ror_rsc_
mask=0x0ff000f0	sig=0x00f00070	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_ror_rscs_
mask=0x0ff000f0	sig=0x00c00070	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_ror_sbc_
mask=0x0ff000f0	sig=0x00d00070	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_ror_sbcs_
mask=0x0ff000f0	sig=0x00400070	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_ror_sub_
mask=0x0ff000f0	sig=0x00500070	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_binop_r_ror_subs_
mask=0x0ff000f0	sig=0x01700050	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_tst_r_asr_cmn_
mask=0x0ff000f0	sig=0x01500050	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_tst_r_asr_cmp_
mask=0x0ff000f0	sig=0x01300050	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_tst_r_asr_teq_
mask=0x0ff000f0	sig=0x01100050	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_tst_r_asr_tst_
mask=0x0ff000f0	sig=0x01700010	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_tst_r_lsl_cmn_
mask=0x0ff000f0	sig=0x01500010	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_tst_r_lsl_cmp_
mask=0x0ff000f0	sig=0x01300010	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_tst_r_lsl_teq_
mask=0x0ff000f0	sig=0x01100010	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_tst_r_lsl_tst_
mask=0x0ff000f0	sig=0x01700030	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_tst_r_lsr_cmn_
mask=0x0ff000f0	sig=0x01500030	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_tst_r_lsr_cmp_
mask=0x0ff000f0	sig=0x01300030	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_tst_r_lsr_teq_
mask=0x0ff000f0	sig=0x01100030	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_tst_r_lsr_tst_
mask=0x0ff000f0	sig=0x01700070	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_tst_r_ror_cmn_
mask=0x0ff000f0	sig=0x01500070	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_tst_r_ror_cmp_
mask=0x0ff000f0	sig=0x01300070	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_tst_r_ror_teq_
mask=0x0ff000f0	sig=0x01100070	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_tst_r_ror_tst_
mask=0x0ff000f0	sig=0x01a00050	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_unop_r_asr_mov_
mask=0x0ff000f0	sig=0x01b00050	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_unop_r_asr_movs_
mask=0x0ff000f0	sig=0x01e00050	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_unop_r_asr_mvn_
mask=0x0ff000f0	sig=0x01f00050	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_unop_r_asr_mvns_
mask=0x0ff000f0	sig=0x01a00010	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_unop_r_lsl_mov_
mask=0x0ff000f0	sig=0x01b00010	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_unop_r_lsl_movs_
mask=0x0ff000f0	sig=0x01e00010	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_unop_r_lsl_mvn_
mask=0x0ff000f0	sig=0x01f00010	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_unop_r_lsl_mvns_
mask=0x0ff000f0	sig=0x01a00030	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_unop_r_lsr_mov_
mask=0x0ff000f0	sig=0x01b00030	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_unop_r_lsr_movs_
mask=0x0ff000f0	sig=0x01e00030	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_unop_r_lsr_mvn_
mask=0x0ff000f0	sig=0x01f00030	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_unop_r_lsr_mvns_
mask=0x0ff000f0	sig=0x01a00070	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_unop_r_ror_mov_
mask=0x0ff000f0	sig=0x01b00070	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_unop_r_ror_movs_
mask=0x0ff000f0	sig=0x01e00070	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_unop_r_ror_mvn_
mask=0x0ff000f0	sig=0x01f00070	prob=0.00182147704	name=DEC_fetch_oper_dpi_rshift_unop_r_ror_mvns_
mask=0x0f000000	sig=0x0c000000	prob=0.00182147704	name=DEC_fetch_oper_fpe_
mask=0x0f000000	sig=0x0e000000	prob=0.00182147704	name=DEC_fetch_oper_fpe_
mask=0x0f100e00	sig=0x0d100e00	prob=0.00182147704	name=DEC_fetch_oper_coproc_ld_
mask=0x0e300000	sig=0x08100000	prob=0.00182147704	name=DEC_fetch_oper_ldm_noupdate_rn_
mask=0x0e300000	sig=0x08300000	prob=0.00182147704	name=DEC_fetch_oper_ldm_update_rn_
mask=0x0f7000b0	sig=0x005000b0	prob=0.000607159012	name=DEC_fetch_oper_load_ext_imm_mem_mode_2_
mask=0x0f7000d0	sig=0x005000d0	prob=0.000607159012	name=DEC_fetch_oper_load_ext_imm_mem_mode_2_
mask=0x0ff00090	sig=0x00500090	prob=0.000607159012	name=DEC_fetch_oper_load_ext_imm_mem_mode_2_
mask=0x0f7000b0	sig=0x007000b0	prob=0.000607159012	name=DEC_fetch_oper_load_ext_imm_mem_mode_3_
mask=0x0f7000d0	sig=0x007000d0	prob=0.000607159012	name=DEC_fetch_oper_load_ext_imm_mem_mode_3_
mask=0x0ff00090	sig=0x00700090	prob=0.000607159012	name=DEC_fetch_oper_load_ext_imm_mem_mode_3_
mask=0x0f700090	sig=0x01500090	prob=0.00182147704	name=DEC_fetch_oper_load_ext_imm_mem_mode_6_
mask=0x0f700090	sig=0x01700090	prob=0.00182147704	name=DEC_fetch_oper_load_ext_imm_mem_mode_7_
mask=0x0f7000b0	sig=0x001000b0	prob=0.000607159012	name=DEC_fetch_oper_load_ext_reg_mem_mode_0_
mask=0x0f7000d0	sig=0x001000d0	prob=0.000607159012	name=DEC_fetch_oper_load_ext_reg_mem_mode_0_
mask=0x0ff000b0	sig=0x001000b0	prob=0.000303579506	name=DEC_fetch_oper_load_ext_reg_mem_mode_0_
mask=0x0ff000d0	sig=0x001000d0	prob=0.000303579506	name=DEC_fetch_oper_load_ext_reg_mem_mode_0_
mask=0x0f7000b0	sig=0x003000b0	prob=0.000607159012	name=DEC_fetch_oper_load_ext_reg_mem_mode_1_
mask=0x0f7000d0	sig=0x003000d0	prob=0.000607159012	name=DEC_fetch_oper_load_ext_reg_mem_mode_1_
mask=0x0ff000b0	sig=0x003000b0	prob=0.000303579506	name=DEC_fetch_oper_load_ext_reg_mem_mode_1_
mask=0x0ff000d0	sig=0x003000d0	prob=0.000303579506	name=DEC_fetch_oper_load_ext_reg_mem_mode_1_
mask=0x0f700090	sig=0x01100090	prob=0.00182147704	name=DEC_fetch_oper_load_ext_reg_mem_mode_4_
mask=0x0f700090	sig=0x01300090	prob=0.00182147704	name=DEC_fetch_oper_load_ext_reg_mem_mode_5_
mask=0x0f700000	sig=0x04100000	prob=0.00182147704	name=DEC_fetch_oper_load_imm_mem_mode_0_
mask=0x0f700000	sig=0x04300000	prob=0.00182147704	name=DEC_fetch_oper_load_imm_mem_mode_1_
mask=0x0f700000	sig=0x04500000	prob=0.00182147704	name=DEC_fetch_oper_load_imm_mem_mode_2_
mask=0x0f700000	sig=0x04700000	prob=0.00182147704	name=DEC_fetch_oper_load_imm_mem_mode_3_
mask=0x0f700000	sig=0x05100000	prob=0.00182147704	name=DEC_fetch_oper_load_imm_mem_mode_4_
mask=0x0f700000	sig=0x05300000	prob=0.00182147704	name=DEC_fetch_oper_load_imm_mem_mode_5_
mask=0x0f700000	sig=0x05500000	prob=0.00182147704	name=DEC_fetch_oper_load_imm_mem_mode_6_
mask=0x0f700000	sig=0x05700000	prob=0.00182147704	name=DEC_fetch_oper_load_imm_mem_mode_7_
mask=0x0f7000f0	sig=0x061000c0	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_0_
mask=0x0f700170	sig=0x06100140	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_0_
mask=0x0f700270	sig=0x06100240	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_0_
mask=0x0f700470	sig=0x06100440	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_0_
mask=0x0f700870	sig=0x06100840	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_0_
mask=0x0f7000f0	sig=0x063000c0	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_1_
mask=0x0f700170	sig=0x06300140	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_1_
mask=0x0f700270	sig=0x06300240	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_1_
mask=0x0f700470	sig=0x06300440	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_1_
mask=0x0f700870	sig=0x06300840	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_1_
mask=0x0f7000f0	sig=0x065000c0	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_2_
mask=0x0f700170	sig=0x06500140	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_2_
mask=0x0f700270	sig=0x06500240	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_2_
mask=0x0f700470	sig=0x06500440	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_2_
mask=0x0f700870	sig=0x06500840	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_2_
mask=0x0f7000f0	sig=0x067000c0	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_3_
mask=0x0f700170	sig=0x06700140	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_3_
mask=0x0f700270	sig=0x06700240	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_3_
mask=0x0f700470	sig=0x06700440	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_3_
mask=0x0f700870	sig=0x06700840	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_3_
mask=0x0f7000f0	sig=0x071000c0	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_4_
mask=0x0f700170	sig=0x07100140	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_4_
mask=0x0f700270	sig=0x07100240	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_4_
mask=0x0f700470	sig=0x07100440	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_4_
mask=0x0f700870	sig=0x07100840	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_4_
mask=0x0f7000f0	sig=0x073000c0	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_5_
mask=0x0f700170	sig=0x07300140	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_5_
mask=0x0f700270	sig=0x07300240	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_5_
mask=0x0f700470	sig=0x07300440	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_5_
mask=0x0f700870	sig=0x07300840	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_5_
mask=0x0f7000f0	sig=0x075000c0	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_6_
mask=0x0f700170	sig=0x07500140	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_6_
mask=0x0f700270	sig=0x07500240	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_6_
mask=0x0f700470	sig=0x07500440	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_6_
mask=0x0f700870	sig=0x07500840	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_6_
mask=0x0f7000f0	sig=0x077000c0	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_7_
mask=0x0f700170	sig=0x07700140	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_7_
mask=0x0f700270	sig=0x07700240	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_7_
mask=0x0f700470	sig=0x07700440	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_7_
mask=0x0f700870	sig=0x07700840	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_7_
mask=0x0f7000f0	sig=0x06100080	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_0_
mask=0x0f700170	sig=0x06100100	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_0_
mask=0x0f700270	sig=0x06100200	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_0_
mask=0x0f700470	sig=0x06100400	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_0_
mask=0x0f700870	sig=0x06100800	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_0_
mask=0x0f7000f0	sig=0x06300080	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_1_
mask=0x0f700170	sig=0x06300100	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_1_
mask=0x0f700270	sig=0x06300200	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_1_
mask=0x0f700470	sig=0x06300400	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_1_
mask=0x0f700870	sig=0x06300800	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_1_
mask=0x0f7000f0	sig=0x06500080	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_2_
mask=0x0f700170	sig=0x06500100	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_2_
mask=0x0f700270	sig=0x06500200	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_2_
mask=0x0f700470	sig=0x06500400	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_2_
mask=0x0f700870	sig=0x06500800	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_2_
mask=0x0f7000f0	sig=0x06700080	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_3_
mask=0x0f700170	sig=0x06700100	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_3_
mask=0x0f700270	sig=0x06700200	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_3_
mask=0x0f700470	sig=0x06700400	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_3_
mask=0x0f700870	sig=0x06700800	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_3_
mask=0x0f7000f0	sig=0x07100080	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_4_
mask=0x0f700170	sig=0x07100100	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_4_
mask=0x0f700270	sig=0x07100200	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_4_
mask=0x0f700470	sig=0x07100400	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_4_
mask=0x0f700870	sig=0x07100800	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_4_
mask=0x0f7000f0	sig=0x07300080	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_5_
mask=0x0f700170	sig=0x07300100	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_5_
mask=0x0f700270	sig=0x07300200	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_5_
mask=0x0f700470	sig=0x07300400	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_5_
mask=0x0f700870	sig=0x07300800	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_5_
mask=0x0f7000f0	sig=0x07500080	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_6_
mask=0x0f700170	sig=0x07500100	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_6_
mask=0x0f700270	sig=0x07500200	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_6_
mask=0x0f700470	sig=0x07500400	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_6_
mask=0x0f700870	sig=0x07500800	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_6_
mask=0x0f7000f0	sig=0x07700080	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_7_
mask=0x0f700170	sig=0x07700100	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_7_
mask=0x0f700270	sig=0x07700200	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_7_
mask=0x0f700470	sig=0x07700400	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_7_
mask=0x0f700870	sig=0x07700800	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_7_
mask=0x0f7000f0	sig=0x061000a0	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_0_
mask=0x0f700170	sig=0x06100120	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_0_
mask=0x0f700270	sig=0x06100220	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_0_
mask=0x0f700470	sig=0x06100420	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_0_
mask=0x0f700870	sig=0x06100820	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_0_
mask=0x0f7000f0	sig=0x063000a0	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_1_
mask=0x0f700170	sig=0x06300120	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_1_
mask=0x0f700270	sig=0x06300220	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_1_
mask=0x0f700470	sig=0x06300420	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_1_
mask=0x0f700870	sig=0x06300820	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_1_
mask=0x0f7000f0	sig=0x065000a0	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_2_
mask=0x0f700170	sig=0x06500120	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_2_
mask=0x0f700270	sig=0x06500220	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_2_
mask=0x0f700470	sig=0x06500420	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_2_
mask=0x0f700870	sig=0x06500820	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_2_
mask=0x0f7000f0	sig=0x067000a0	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_3_
mask=0x0f700170	sig=0x06700120	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_3_
mask=0x0f700270	sig=0x06700220	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_3_
mask=0x0f700470	sig=0x06700420	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_3_
mask=0x0f700870	sig=0x06700820	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_3_
mask=0x0f7000f0	sig=0x071000a0	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_4_
mask=0x0f700170	sig=0x07100120	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_4_
mask=0x0f700270	sig=0x07100220	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_4_
mask=0x0f700470	sig=0x07100420	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_4_
mask=0x0f700870	sig=0x07100820	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_4_
mask=0x0f7000f0	sig=0x073000a0	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_5_
mask=0x0f700170	sig=0x07300120	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_5_
mask=0x0f700270	sig=0x07300220	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_5_
mask=0x0f700470	sig=0x07300420	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_5_
mask=0x0f700870	sig=0x07300820	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_5_
mask=0x0f7000f0	sig=0x075000a0	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_6_
mask=0x0f700170	sig=0x07500120	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_6_
mask=0x0f700270	sig=0x07500220	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_6_
mask=0x0f700470	sig=0x07500420	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_6_
mask=0x0f700870	sig=0x07500820	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_6_
mask=0x0f7000f0	sig=0x077000a0	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_7_
mask=0x0f700170	sig=0x07700120	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_7_
mask=0x0f700270	sig=0x07700220	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_7_
mask=0x0f700470	sig=0x07700420	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_7_
mask=0x0f700870	sig=0x07700820	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_7_
mask=0x0f7000f0	sig=0x061000e0	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_0_
mask=0x0f700170	sig=0x06100160	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_0_
mask=0x0f700270	sig=0x06100260	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_0_
mask=0x0f700470	sig=0x06100460	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_0_
mask=0x0f700870	sig=0x06100860	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_0_
mask=0x0f7000f0	sig=0x063000e0	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_1_
mask=0x0f700170	sig=0x06300160	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_1_
mask=0x0f700270	sig=0x06300260	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_1_
mask=0x0f700470	sig=0x06300460	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_1_
mask=0x0f700870	sig=0x06300860	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_1_
mask=0x0f7000f0	sig=0x065000e0	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_2_
mask=0x0f700170	sig=0x06500160	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_2_
mask=0x0f700270	sig=0x06500260	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_2_
mask=0x0f700470	sig=0x06500460	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_2_
mask=0x0f700870	sig=0x06500860	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_2_
mask=0x0f7000f0	sig=0x067000e0	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_3_
mask=0x0f700170	sig=0x06700160	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_3_
mask=0x0f700270	sig=0x06700260	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_3_
mask=0x0f700470	sig=0x06700460	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_3_
mask=0x0f700870	sig=0x06700860	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_3_
mask=0x0f7000f0	sig=0x071000e0	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_4_
mask=0x0f700170	sig=0x07100160	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_4_
mask=0x0f700270	sig=0x07100260	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_4_
mask=0x0f700470	sig=0x07100460	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_4_
mask=0x0f700870	sig=0x07100860	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_4_
mask=0x0f7000f0	sig=0x073000e0	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_5_
mask=0x0f700170	sig=0x07300160	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_5_
mask=0x0f700270	sig=0x07300260	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_5_
mask=0x0f700470	sig=0x07300460	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_5_
mask=0x0f700870	sig=0x07300860	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_5_
mask=0x0f7000f0	sig=0x075000e0	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_6_
mask=0x0f700170	sig=0x07500160	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_6_
mask=0x0f700270	sig=0x07500260	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_6_
mask=0x0f700470	sig=0x07500460	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_6_
mask=0x0f700870	sig=0x07500860	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_6_
mask=0x0f7000f0	sig=0x077000e0	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_7_
mask=0x0f700170	sig=0x07700160	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_7_
mask=0x0f700270	sig=0x07700260	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_7_
mask=0x0f700470	sig=0x07700460	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_7_
mask=0x0f700870	sig=0x07700860	prob=0.000364295407	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_7_
mask=0x0f700ff0	sig=0x06100040	prob=0.00182147704	name=DEC_fetch_oper_load_reg_zero_shift_zero_asr_mem_mode_0_
mask=0x0f700ff0	sig=0x06300040	prob=0.00182147704	name=DEC_fetch_oper_load_reg_zero_shift_zero_asr_mem_mode_1_
mask=0x0f700ff0	sig=0x06500040	prob=0.00182147704	name=DEC_fetch_oper_load_reg_zero_shift_zero_asr_mem_mode_2_
mask=0x0f700ff0	sig=0x06700040	prob=0.00182147704	name=DEC_fetch_oper_load_reg_zero_shift_zero_asr_mem_mode_3_
mask=0x0f700ff0	sig=0x07100040	prob=0.00182147704	name=DEC_fetch_oper_load_reg_zero_shift_zero_asr_mem_mode_4_
mask=0x0f700ff0	sig=0x07300040	prob=0.00182147704	name=DEC_fetch_oper_load_reg_zero_shift_zero_asr_mem_mode_5_
mask=0x0f700ff0	sig=0x07500040	prob=0.00182147704	name=DEC_fetch_oper_load_reg_zero_shift_zero_asr_mem_mode_6_
mask=0x0f700ff0	sig=0x07700040	prob=0.00182147704	name=DEC_fetch_oper_load_reg_zero_shift_zero_asr_mem_mode_7_
mask=0x0f700ff0	sig=0x06100000	prob=0.00182147704	name=DEC_fetch_oper_load_reg_zero_shift_zero_lsl_mem_mode_0_
mask=0x0f700ff0	sig=0x06300000	prob=0.00182147704	name=DEC_fetch_oper_load_reg_zero_shift_zero_lsl_mem_mode_1_
mask=0x0f700ff0	sig=0x06500000	prob=0.00182147704	name=DEC_fetch_oper_load_reg_zero_shift_zero_lsl_mem_mode_2_
mask=0x0f700ff0	sig=0x06700000	prob=0.00182147704	name=DEC_fetch_oper_load_reg_zero_shift_zero_lsl_mem_mode_3_
mask=0x0f700ff0	sig=0x07100000	prob=0.00182147704	name=DEC_fetch_oper_load_reg_zero_shift_zero_lsl_mem_mode_4_
mask=0x0f700ff0	sig=0x07300000	prob=0.00182147704	name=DEC_fetch_oper_load_reg_zero_shift_zero_lsl_mem_mode_5_
mask=0x0f700ff0	sig=0x07500000	prob=0.00182147704	name=DEC_fetch_oper_load_reg_zero_shift_zero_lsl_mem_mode_6_
mask=0x0f700ff0	sig=0x07700000	prob=0.00182147704	name=DEC_fetch_oper_load_reg_zero_shift_zero_lsl_mem_mode_7_
mask=0x0f700ff0	sig=0x06100020	prob=0.00182147704	name=DEC_fetch_oper_load_reg_zero_shift_zero_lsr_mem_mode_0_
mask=0x0f700ff0	sig=0x06300020	prob=0.00182147704	name=DEC_fetch_oper_load_reg_zero_shift_zero_lsr_mem_mode_1_
mask=0x0f700ff0	sig=0x06500020	prob=0.00182147704	name=DEC_fetch_oper_load_reg_zero_shift_zero_lsr_mem_mode_2_
mask=0x0f700ff0	sig=0x06700020	prob=0.00182147704	name=DEC_fetch_oper_load_reg_zero_shift_zero_lsr_mem_mode_3_
mask=0x0f700ff0	sig=0x07100020	prob=0.00182147704	name=DEC_fetch_oper_load_reg_zero_shift_zero_lsr_mem_mode_4_
mask=0x0f700ff0	sig=0x07300020	prob=0.00182147704	name=DEC_fetch_oper_load_reg_zero_shift_zero_lsr_mem_mode_5_
mask=0x0f700ff0	sig=0x07500020	prob=0.00182147704	name=DEC_fetch_oper_load_reg_zero_shift_zero_lsr_mem_mode_6_
mask=0x0f700ff0	sig=0x07700020	prob=0.00182147704	name=DEC_fetch_oper_load_reg_zero_shift_zero_lsr_mem_mode_7_
mask=0x0f700ff0	sig=0x06100060	prob=0.00182147704	name=DEC_fetch_oper_load_reg_zero_shift_zero_ror_mem_mode_0_
mask=0x0f700ff0	sig=0x06300060	prob=0.00182147704	name=DEC_fetch_oper_load_reg_zero_shift_zero_ror_mem_mode_1_
mask=0x0f700ff0	sig=0x06500060	prob=0.00182147704	name=DEC_fetch_oper_load_reg_zero_shift_zero_ror_mem_mode_2_
mask=0x0f700ff0	sig=0x06700060	prob=0.00182147704	name=DEC_fetch_oper_load_reg_zero_shift_zero_ror_mem_mode_3_
mask=0x0f700ff0	sig=0x07100060	prob=0.00182147704	name=DEC_fetch_oper_load_reg_zero_shift_zero_ror_mem_mode_4_
mask=0x0f700ff0	sig=0x07300060	prob=0.00182147704	name=DEC_fetch_oper_load_reg_zero_shift_zero_ror_mem_mode_5_
mask=0x0f700ff0	sig=0x07500060	prob=0.00182147704	name=DEC_fetch_oper_load_reg_zero_shift_zero_ror_mem_mode_6_
mask=0x0f700ff0	sig=0x07700060	prob=0.00182147704	name=DEC_fetch_oper_load_reg_zero_shift_zero_ror_mem_mode_7_
mask=0x0f7000b0	sig=0x002000b0	prob=0.000607159012	name=DEC_fetch_oper_store_ext_reg_mem_mode_1_
mask=0x0f7000d0	sig=0x002000d0	prob=0.000607159012	name=DEC_fetch_oper_store_ext_reg_mem_mode_1_
mask=0x0ff000b0	sig=0x00a000b0	prob=0.000303579506	name=DEC_fetch_oper_store_ext_reg_mem_mode_1_
mask=0x0ff000d0	sig=0x00a000d0	prob=0.000303579506	name=DEC_fetch_oper_store_ext_reg_mem_mode_1_
mask=0x0ff000f0	sig=0x00b00090	prob=0.00182147704	name=DEC_fetch_oper_mult_long_smlals_
mask=0x0f7000b0	sig=0x000000b0	prob=0.000607159012	name=DEC_fetch_oper_store_ext_reg_mem_mode_0_
mask=0x0f7000d0	sig=0x000000d0	prob=0.000607159012	name=DEC_fetch_oper_store_ext_reg_mem_mode_0_
mask=0x0ff000b0	sig=0x008000b0	prob=0.000303579506	name=DEC_fetch_oper_store_ext_reg_mem_mode_0_
mask=0x0ff000d0	sig=0x008000d0	prob=0.000303579506	name=DEC_fetch_oper_store_ext_reg_mem_mode_0_
mask=0x0ff000f0	sig=0x00900090	prob=0.00182147704	name=DEC_fetch_oper_mult_long_smulls_
mask=0x0f7000b0	sig=0x006000b0	prob=0.000607159012	name=DEC_fetch_oper_store_ext_imm_mem_mode_3_
mask=0x0f7000d0	sig=0x006000d0	prob=0.000607159012	name=DEC_fetch_oper_store_ext_imm_mem_mode_3_
mask=0x0ff00090	sig=0x00600090	prob=0.000607159012	name=DEC_fetch_oper_store_ext_imm_mem_mode_3_
mask=0x0ff000f0	sig=0x00f00090	prob=0.00182147704	name=DEC_fetch_oper_mult_long_umlals_
mask=0x0f7000b0	sig=0x004000b0	prob=0.000607159012	name=DEC_fetch_oper_store_ext_imm_mem_mode_2_
mask=0x0f7000d0	sig=0x004000d0	prob=0.000607159012	name=DEC_fetch_oper_store_ext_imm_mem_mode_2_
mask=0x0ff00090	sig=0x00400090	prob=0.000607159012	name=DEC_fetch_oper_store_ext_imm_mem_mode_2_
mask=0x0ff000f0	sig=0x00d00090	prob=0.00182147704	name=DEC_fetch_oper_mult_long_umulls_
mask=0x0ff000f0	sig=0x00200090	prob=0.00182147704	name=DEC_fetch_oper_mult_mla_
mask=0x0ff000f0	sig=0x00300090	prob=0.00182147704	name=DEC_fetch_oper_mult_mlas_
mask=0x0ff000f0	sig=0x00000090	prob=0.00182147704	name=DEC_fetch_oper_mult_mul_
mask=0x0ff000f0	sig=0x00100090	prob=0.00182147704	name=DEC_fetch_oper_mult_muls_
mask=0x0e300000	sig=0x08000000	prob=0.00182147704	name=DEC_fetch_oper_stm_noupdate_rn_
mask=0x0e300000	sig=0x08200000	prob=0.00182147704	name=DEC_fetch_oper_stm_update_rn_
mask=0x0ff000f0	sig=0x00c00090	prob=0.00182147704	name=DEC_fetch_oper_mult_long_umull_
mask=0x0ff000f0	sig=0x00e00090	prob=0.00182147704	name=DEC_fetch_oper_mult_long_umlal_
mask=0x0f700090	sig=0x01400090	prob=0.00182147704	name=DEC_fetch_oper_store_ext_imm_mem_mode_6_
mask=0x0f700090	sig=0x01600090	prob=0.00182147704	name=DEC_fetch_oper_store_ext_imm_mem_mode_7_
mask=0x0ff000f0	sig=0x00800090	prob=0.00182147704	name=DEC_fetch_oper_mult_long_smull_
mask=0x0ff000f0	sig=0x00a00090	prob=0.00182147704	name=DEC_fetch_oper_mult_long_smlal_
mask=0x0f7000b0	sig=0x010000b0	prob=0.000260211005	name=DEC_fetch_oper_store_ext_reg_mem_mode_4_
mask=0x0f7000d0	sig=0x010000d0	prob=0.000260211005	name=DEC_fetch_oper_store_ext_reg_mem_mode_4_
mask=0x0f700190	sig=0x01000190	prob=0.000260211005	name=DEC_fetch_oper_store_ext_reg_mem_mode_4_
mask=0x0f700290	sig=0x01000290	prob=0.000260211005	name=DEC_fetch_oper_store_ext_reg_mem_mode_4_
mask=0x0f700490	sig=0x01000490	prob=0.000260211005	name=DEC_fetch_oper_store_ext_reg_mem_mode_4_
mask=0x0f700890	sig=0x01000890	prob=0.000260211005	name=DEC_fetch_oper_store_ext_reg_mem_mode_4_
mask=0x0ff00090	sig=0x01800090	prob=0.000260211005	name=DEC_fetch_oper_store_ext_reg_mem_mode_4_
mask=0x0f700090	sig=0x01200090	prob=0.00182147704	name=DEC_fetch_oper_store_ext_reg_mem_mode_5_
mask=0x0f700000	sig=0x04000000	prob=0.00182147704	name=DEC_fetch_oper_store_imm_mem_mode_0_
mask=0x0f700000	sig=0x04200000	prob=0.00182147704	name=DEC_fetch_oper_store_imm_mem_mode_1_
mask=0x0f700000	sig=0x04400000	prob=0.00182147704	name=DEC_fetch_oper_store_imm_mem_mode_2_
mask=0x0f700000	sig=0x04600000	prob=0.00182147704	name=DEC_fetch_oper_store_imm_mem_mode_3_
mask=0x0f700000	sig=0x05000000	prob=0.00182147704	name=DEC_fetch_oper_store_imm_mem_mode_4_
mask=0x0f700000	sig=0x05200000	prob=0.00182147704	name=DEC_fetch_oper_store_imm_mem_mode_5_
mask=0x0f700000	sig=0x05400000	prob=0.00182147704	name=DEC_fetch_oper_store_imm_mem_mode_6_
mask=0x0f700000	sig=0x05600000	prob=0.00182147704	name=DEC_fetch_oper_store_imm_mem_mode_7_
mask=0x0f7000f0	sig=0x060000c0	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_0_
mask=0x0f700170	sig=0x06000140	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_0_
mask=0x0f700270	sig=0x06000240	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_0_
mask=0x0f700470	sig=0x06000440	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_0_
mask=0x0f700870	sig=0x06000840	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_0_
mask=0x0f7000f0	sig=0x062000c0	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_1_
mask=0x0f700170	sig=0x06200140	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_1_
mask=0x0f700270	sig=0x06200240	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_1_
mask=0x0f700470	sig=0x06200440	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_1_
mask=0x0f700870	sig=0x06200840	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_1_
mask=0x0f7000f0	sig=0x064000c0	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_2_
mask=0x0f700170	sig=0x06400140	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_2_
mask=0x0f700270	sig=0x06400240	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_2_
mask=0x0f700470	sig=0x06400440	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_2_
mask=0x0f700870	sig=0x06400840	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_2_
mask=0x0f7000f0	sig=0x066000c0	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_3_
mask=0x0f700170	sig=0x06600140	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_3_
mask=0x0f700270	sig=0x06600240	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_3_
mask=0x0f700470	sig=0x06600440	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_3_
mask=0x0f700870	sig=0x06600840	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_3_
mask=0x0f7000f0	sig=0x070000c0	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_4_
mask=0x0f700170	sig=0x07000140	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_4_
mask=0x0f700270	sig=0x07000240	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_4_
mask=0x0f700470	sig=0x07000440	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_4_
mask=0x0f700870	sig=0x07000840	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_4_
mask=0x0f7000f0	sig=0x072000c0	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_5_
mask=0x0f700170	sig=0x07200140	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_5_
mask=0x0f700270	sig=0x07200240	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_5_
mask=0x0f700470	sig=0x07200440	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_5_
mask=0x0f700870	sig=0x07200840	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_5_
mask=0x0f7000f0	sig=0x074000c0	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_6_
mask=0x0f700170	sig=0x07400140	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_6_
mask=0x0f700270	sig=0x07400240	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_6_
mask=0x0f700470	sig=0x07400440	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_6_
mask=0x0f700870	sig=0x07400840	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_6_
mask=0x0f7000f0	sig=0x076000c0	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_7_
mask=0x0f700170	sig=0x07600140	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_7_
mask=0x0f700270	sig=0x07600240	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_7_
mask=0x0f700470	sig=0x07600440	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_7_
mask=0x0f700870	sig=0x07600840	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_7_
mask=0x0f7000f0	sig=0x06000080	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_0_
mask=0x0f700170	sig=0x06000100	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_0_
mask=0x0f700270	sig=0x06000200	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_0_
mask=0x0f700470	sig=0x06000400	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_0_
mask=0x0f700870	sig=0x06000800	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_0_
mask=0x0f7000f0	sig=0x06200080	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_1_
mask=0x0f700170	sig=0x06200100	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_1_
mask=0x0f700270	sig=0x06200200	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_1_
mask=0x0f700470	sig=0x06200400	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_1_
mask=0x0f700870	sig=0x06200800	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_1_
mask=0x0f7000f0	sig=0x06400080	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_2_
mask=0x0f700170	sig=0x06400100	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_2_
mask=0x0f700270	sig=0x06400200	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_2_
mask=0x0f700470	sig=0x06400400	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_2_
mask=0x0f700870	sig=0x06400800	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_2_
mask=0x0f7000f0	sig=0x06600080	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_3_
mask=0x0f700170	sig=0x06600100	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_3_
mask=0x0f700270	sig=0x06600200	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_3_
mask=0x0f700470	sig=0x06600400	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_3_
mask=0x0f700870	sig=0x06600800	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_3_
mask=0x0f7000f0	sig=0x07000080	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_4_
mask=0x0f700170	sig=0x07000100	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_4_
mask=0x0f700270	sig=0x07000200	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_4_
mask=0x0f700470	sig=0x07000400	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_4_
mask=0x0f700870	sig=0x07000800	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_4_
mask=0x0f7000f0	sig=0x07200080	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_5_
mask=0x0f700170	sig=0x07200100	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_5_
mask=0x0f700270	sig=0x07200200	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_5_
mask=0x0f700470	sig=0x07200400	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_5_
mask=0x0f700870	sig=0x07200800	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_5_
mask=0x0f7000f0	sig=0x07400080	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_6_
mask=0x0f700170	sig=0x07400100	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_6_
mask=0x0f700270	sig=0x07400200	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_6_
mask=0x0f700470	sig=0x07400400	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_6_
mask=0x0f700870	sig=0x07400800	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_6_
mask=0x0f7000f0	sig=0x07600080	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_7_
mask=0x0f700170	sig=0x07600100	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_7_
mask=0x0f700270	sig=0x07600200	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_7_
mask=0x0f700470	sig=0x07600400	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_7_
mask=0x0f700870	sig=0x07600800	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_7_
mask=0x0f7000f0	sig=0x060000a0	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_0_
mask=0x0f700170	sig=0x06000120	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_0_
mask=0x0f700270	sig=0x06000220	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_0_
mask=0x0f700470	sig=0x06000420	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_0_
mask=0x0f700870	sig=0x06000820	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_0_
mask=0x0f7000f0	sig=0x062000a0	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_1_
mask=0x0f700170	sig=0x06200120	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_1_
mask=0x0f700270	sig=0x06200220	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_1_
mask=0x0f700470	sig=0x06200420	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_1_
mask=0x0f700870	sig=0x06200820	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_1_
mask=0x0f7000f0	sig=0x064000a0	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_2_
mask=0x0f700170	sig=0x06400120	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_2_
mask=0x0f700270	sig=0x06400220	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_2_
mask=0x0f700470	sig=0x06400420	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_2_
mask=0x0f700870	sig=0x06400820	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_2_
mask=0x0f7000f0	sig=0x066000a0	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_3_
mask=0x0f700170	sig=0x06600120	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_3_
mask=0x0f700270	sig=0x06600220	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_3_
mask=0x0f700470	sig=0x06600420	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_3_
mask=0x0f700870	sig=0x06600820	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_3_
mask=0x0f7000f0	sig=0x070000a0	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_4_
mask=0x0f700170	sig=0x07000120	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_4_
mask=0x0f700270	sig=0x07000220	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_4_
mask=0x0f700470	sig=0x07000420	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_4_
mask=0x0f700870	sig=0x07000820	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_4_
mask=0x0f7000f0	sig=0x072000a0	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_5_
mask=0x0f700170	sig=0x07200120	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_5_
mask=0x0f700270	sig=0x07200220	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_5_
mask=0x0f700470	sig=0x07200420	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_5_
mask=0x0f700870	sig=0x07200820	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_5_
mask=0x0f7000f0	sig=0x074000a0	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_6_
mask=0x0f700170	sig=0x07400120	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_6_
mask=0x0f700270	sig=0x07400220	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_6_
mask=0x0f700470	sig=0x07400420	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_6_
mask=0x0f700870	sig=0x07400820	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_6_
mask=0x0f7000f0	sig=0x076000a0	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_7_
mask=0x0f700170	sig=0x07600120	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_7_
mask=0x0f700270	sig=0x07600220	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_7_
mask=0x0f700470	sig=0x07600420	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_7_
mask=0x0f700870	sig=0x07600820	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_7_
mask=0x0f7000f0	sig=0x060000e0	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_0_
mask=0x0f700170	sig=0x06000160	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_0_
mask=0x0f700270	sig=0x06000260	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_0_
mask=0x0f700470	sig=0x06000460	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_0_
mask=0x0f700870	sig=0x06000860	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_0_
mask=0x0f7000f0	sig=0x062000e0	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_1_
mask=0x0f700170	sig=0x06200160	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_1_
mask=0x0f700270	sig=0x06200260	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_1_
mask=0x0f700470	sig=0x06200460	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_1_
mask=0x0f700870	sig=0x06200860	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_1_
mask=0x0f7000f0	sig=0x064000e0	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_2_
mask=0x0f700170	sig=0x06400160	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_2_
mask=0x0f700270	sig=0x06400260	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_2_
mask=0x0f700470	sig=0x06400460	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_2_
mask=0x0f700870	sig=0x06400860	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_2_
mask=0x0f7000f0	sig=0x066000e0	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_3_
mask=0x0f700170	sig=0x06600160	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_3_
mask=0x0f700270	sig=0x06600260	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_3_
mask=0x0f700470	sig=0x06600460	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_3_
mask=0x0f700870	sig=0x06600860	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_3_
mask=0x0f7000f0	sig=0x070000e0	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_4_
mask=0x0f700170	sig=0x07000160	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_4_
mask=0x0f700270	sig=0x07000260	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_4_
mask=0x0f700470	sig=0x07000460	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_4_
mask=0x0f700870	sig=0x07000860	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_4_
mask=0x0f7000f0	sig=0x072000e0	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_5_
mask=0x0f700170	sig=0x07200160	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_5_
mask=0x0f700270	sig=0x07200260	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_5_
mask=0x0f700470	sig=0x07200460	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_5_
mask=0x0f700870	sig=0x07200860	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_5_
mask=0x0f7000f0	sig=0x074000e0	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_6_
mask=0x0f700170	sig=0x07400160	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_6_
mask=0x0f700270	sig=0x07400260	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_6_
mask=0x0f700470	sig=0x07400460	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_6_
mask=0x0f700870	sig=0x07400860	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_6_
mask=0x0f7000f0	sig=0x076000e0	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_7_
mask=0x0f700170	sig=0x07600160	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_7_
mask=0x0f700270	sig=0x07600260	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_7_
mask=0x0f700470	sig=0x07600460	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_7_
mask=0x0f700870	sig=0x07600860	prob=0.000364295407	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_7_
mask=0x0f700ff0	sig=0x06000040	prob=0.00182147704	name=DEC_fetch_oper_store_reg_zero_shift_zero_asr_mem_mode_0_
mask=0x0f700ff0	sig=0x06200040	prob=0.00182147704	name=DEC_fetch_oper_store_reg_zero_shift_zero_asr_mem_mode_1_
mask=0x0f700ff0	sig=0x06400040	prob=0.00182147704	name=DEC_fetch_oper_store_reg_zero_shift_zero_asr_mem_mode_2_
mask=0x0f700ff0	sig=0x06600040	prob=0.00182147704	name=DEC_fetch_oper_store_reg_zero_shift_zero_asr_mem_mode_3_
mask=0x0f700ff0	sig=0x07000040	prob=0.00182147704	name=DEC_fetch_oper_store_reg_zero_shift_zero_asr_mem_mode_4_
mask=0x0f700ff0	sig=0x07200040	prob=0.00182147704	name=DEC_fetch_oper_store_reg_zero_shift_zero_asr_mem_mode_5_
mask=0x0f700ff0	sig=0x07400040	prob=0.00182147704	name=DEC_fetch_oper_store_reg_zero_shift_zero_asr_mem_mode_6_
mask=0x0f700ff0	sig=0x07600040	prob=0.00182147704	name=DEC_fetch_oper_store_reg_zero_shift_zero_asr_mem_mode_7_
mask=0x0f700ff0	sig=0x06000000	prob=0.00182147704	name=DEC_fetch_oper_store_reg_zero_shift_zero_lsl_mem_mode_0_
mask=0x0f700ff0	sig=0x06200000	prob=0.00182147704	name=DEC_fetch_oper_store_reg_zero_shift_zero_lsl_mem_mode_1_
mask=0x0f700ff0	sig=0x06400000	prob=0.00182147704	name=DEC_fetch_oper_store_reg_zero_shift_zero_lsl_mem_mode_2_
mask=0x0f700ff0	sig=0x06600000	prob=0.00182147704	name=DEC_fetch_oper_store_reg_zero_shift_zero_lsl_mem_mode_3_
mask=0x0f700ff0	sig=0x07000000	prob=0.00182147704	name=DEC_fetch_oper_store_reg_zero_shift_zero_lsl_mem_mode_4_
mask=0x0f700ff0	sig=0x07200000	prob=0.00182147704	name=DEC_fetch_oper_store_reg_zero_shift_zero_lsl_mem_mode_5_
mask=0x0f700ff0	sig=0x07400000	prob=0.00182147704	name=DEC_fetch_oper_store_reg_zero_shift_zero_lsl_mem_mode_6_
mask=0x0f700ff0	sig=0x07600000	prob=0.00182147704	name=DEC_fetch_oper_store_reg_zero_shift_zero_lsl_mem_mode_7_
mask=0x0f700ff0	sig=0x06000020	prob=0.00182147704	name=DEC_fetch_oper_store_reg_zero_shift_zero_lsr_mem_mode_0_
mask=0x0f700ff0	sig=0x06200020	prob=0.00182147704	name=DEC_fetch_oper_store_reg_zero_shift_zero_lsr_mem_mode_1_
mask=0x0f700ff0	sig=0x06400020	prob=0.00182147704	name=DEC_fetch_oper_store_reg_zero_shift_zero_lsr_mem_mode_2_
mask=0x0f700ff0	sig=0x06600020	prob=0.00182147704	name=DEC_fetch_oper_store_reg_zero_shift_zero_lsr_mem_mode_3_
mask=0x0f700ff0	sig=0x07000020	prob=0.00182147704	name=DEC_fetch_oper_store_reg_zero_shift_zero_lsr_mem_mode_4_
mask=0x0f700ff0	sig=0x07200020	prob=0.00182147704	name=DEC_fetch_oper_store_reg_zero_shift_zero_lsr_mem_mode_5_
mask=0x0f700ff0	sig=0x07400020	prob=0.00182147704	name=DEC_fetch_oper_store_reg_zero_shift_zero_lsr_mem_mode_6_
mask=0x0f700ff0	sig=0x07600020	prob=0.00182147704	name=DEC_fetch_oper_store_reg_zero_shift_zero_lsr_mem_mode_7_
mask=0x0f700ff0	sig=0x06000060	prob=0.00182147704	name=DEC_fetch_oper_store_reg_zero_shift_zero_ror_mem_mode_0_
mask=0x0f700ff0	sig=0x06200060	prob=0.00182147704	name=DEC_fetch_oper_store_reg_zero_shift_zero_ror_mem_mode_1_
mask=0x0f700ff0	sig=0x06400060	prob=0.00182147704	name=DEC_fetch_oper_store_reg_zero_shift_zero_ror_mem_mode_2_
mask=0x0f700ff0	sig=0x06600060	prob=0.00182147704	name=DEC_fetch_oper_store_reg_zero_shift_zero_ror_mem_mode_3_
mask=0x0f700ff0	sig=0x07000060	prob=0.00182147704	name=DEC_fetch_oper_store_reg_zero_shift_zero_ror_mem_mode_4_
mask=0x0f700ff0	sig=0x07200060	prob=0.00182147704	name=DEC_fetch_oper_store_reg_zero_shift_zero_ror_mem_mode_5_
mask=0x0f700ff0	sig=0x07400060	prob=0.00182147704	name=DEC_fetch_oper_store_reg_zero_shift_zero_ror_mem_mode_6_
mask=0x0f700ff0	sig=0x07600060	prob=0.00182147704	name=DEC_fetch_oper_store_reg_zero_shift_zero_ror_mem_mode_7_
mask=0x0ff00ff0	sig=0x01000090	prob=0.00182147704	name=DEC_fetch_oper_swap_
mask=0x0f000000	sig=0x0f000000	prob=0.00182147704	name=DEC_fetch_oper_syscall_
mask=0x0e000010	sig=0x06000010	prob=1.82147704e-06	name=DEC_fetch_oper_unknown
mask=0x0d900010	sig=0x01000000	prob=1.82147704e-06	name=DEC_fetch_oper_unknown
mask=0x0d900080	sig=0x01000000	prob=1.82147704e-06	name=DEC_fetch_oper_unknown
mask=0x0b900010	sig=0x03000010	prob=1.82147704e-06	name=DEC_fetch_oper_unknown
mask=0x0f900000	sig=0x03000000	prob=1.82147704e-06	name=DEC_fetch_oper_unknown
-------------------------------------------------------------------
Table Mask=0x0ff00000*/

_TABLE_DEF_BEGIN(table_0, 256)
  _STUB_NAME(stub_0_0),
  _STUB_NAME(stub_0_1),
  _STUB_NAME(stub_0_2),
  _STUB_NAME(stub_0_3),
  _STUB_NAME(stub_0_4),
  _STUB_NAME(stub_0_5),
  _STUB_NAME(stub_0_6),
  _STUB_NAME(stub_0_7),
  _STUB_NAME(stub_0_8),
  _STUB_NAME(stub_0_9),
  _STUB_NAME(stub_0_10),
  _STUB_NAME(stub_0_11),
  _STUB_NAME(stub_0_12),
  _STUB_NAME(stub_0_13),
  _STUB_NAME(stub_0_14),
  _STUB_NAME(stub_0_15),
  _STUB_NAME(stub_0_16),
  _STUB_NAME(stub_0_17),
  _STUB_NAME(stub_0_18),
  _STUB_NAME(stub_0_19),
  _STUB_NAME(stub_0_20),
  _STUB_NAME(stub_0_21),
  _STUB_NAME(stub_0_22),
  _STUB_NAME(stub_0_23),
  _STUB_NAME(stub_0_24),
  _STUB_NAME(stub_0_25),
  _STUB_NAME(stub_0_26),
  _STUB_NAME(stub_0_27),
  _STUB_NAME(stub_0_28),
  _STUB_NAME(stub_0_29),
  _STUB_NAME(stub_0_30),
  _STUB_NAME(stub_0_31),
  _FUNC_NAME(DEC_fetch_oper_dpi_imm_binop_and_),
  _FUNC_NAME(DEC_fetch_oper_dpi_imm_binop_ands_),
  _FUNC_NAME(DEC_fetch_oper_dpi_imm_binop_eor_),
  _FUNC_NAME(DEC_fetch_oper_dpi_imm_binop_eors_),
  _FUNC_NAME(DEC_fetch_oper_dpi_imm_binop_sub_),
  _FUNC_NAME(DEC_fetch_oper_dpi_imm_binop_subs_),
  _FUNC_NAME(DEC_fetch_oper_dpi_imm_binop_rsb_),
  _FUNC_NAME(DEC_fetch_oper_dpi_imm_binop_rsbs_),
  _FUNC_NAME(DEC_fetch_oper_dpi_imm_binop_add_),
  _FUNC_NAME(DEC_fetch_oper_dpi_imm_binop_adds_),
  _FUNC_NAME(DEC_fetch_oper_dpi_imm_binop_adc_),
  _FUNC_NAME(DEC_fetch_oper_dpi_imm_binop_adcs_),
  _FUNC_NAME(DEC_fetch_oper_dpi_imm_binop_sbc_),
  _FUNC_NAME(DEC_fetch_oper_dpi_imm_binop_sbcs_),
  _FUNC_NAME(DEC_fetch_oper_dpi_imm_binop_rsc_),
  _FUNC_NAME(DEC_fetch_oper_dpi_imm_binop_rscs_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_dpi_imm_tst_tst_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_dpi_imm_tst_teq_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_dpi_imm_tst_cmp_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_dpi_imm_tst_cmn_),
  _FUNC_NAME(DEC_fetch_oper_dpi_imm_binop_orr_),
  _FUNC_NAME(DEC_fetch_oper_dpi_imm_binop_orrs_),
  _FUNC_NAME(DEC_fetch_oper_dpi_imm_unop_mov_),
  _FUNC_NAME(DEC_fetch_oper_dpi_imm_unop_movs_),
  _FUNC_NAME(DEC_fetch_oper_dpi_imm_binop_bic_),
  _FUNC_NAME(DEC_fetch_oper_dpi_imm_binop_bics_),
  _FUNC_NAME(DEC_fetch_oper_dpi_imm_unop_mvn_),
  _FUNC_NAME(DEC_fetch_oper_dpi_imm_unop_mvns_),
  _FUNC_NAME(DEC_fetch_oper_store_imm_mem_mode_0_),
  _FUNC_NAME(DEC_fetch_oper_load_imm_mem_mode_0_),
  _FUNC_NAME(DEC_fetch_oper_store_imm_mem_mode_1_),
  _FUNC_NAME(DEC_fetch_oper_load_imm_mem_mode_1_),
  _FUNC_NAME(DEC_fetch_oper_store_imm_mem_mode_2_),
  _FUNC_NAME(DEC_fetch_oper_load_imm_mem_mode_2_),
  _FUNC_NAME(DEC_fetch_oper_store_imm_mem_mode_3_),
  _FUNC_NAME(DEC_fetch_oper_load_imm_mem_mode_3_),
  _FUNC_NAME(DEC_fetch_oper_store_imm_mem_mode_0_),
  _FUNC_NAME(DEC_fetch_oper_load_imm_mem_mode_0_),
  _FUNC_NAME(DEC_fetch_oper_store_imm_mem_mode_1_),
  _FUNC_NAME(DEC_fetch_oper_load_imm_mem_mode_1_),
  _FUNC_NAME(DEC_fetch_oper_store_imm_mem_mode_2_),
  _FUNC_NAME(DEC_fetch_oper_load_imm_mem_mode_2_),
  _FUNC_NAME(DEC_fetch_oper_store_imm_mem_mode_3_),
  _FUNC_NAME(DEC_fetch_oper_load_imm_mem_mode_3_),
  _FUNC_NAME(DEC_fetch_oper_store_imm_mem_mode_4_),
  _FUNC_NAME(DEC_fetch_oper_load_imm_mem_mode_4_),
  _FUNC_NAME(DEC_fetch_oper_store_imm_mem_mode_5_),
  _FUNC_NAME(DEC_fetch_oper_load_imm_mem_mode_5_),
  _FUNC_NAME(DEC_fetch_oper_store_imm_mem_mode_6_),
  _FUNC_NAME(DEC_fetch_oper_load_imm_mem_mode_6_),
  _FUNC_NAME(DEC_fetch_oper_store_imm_mem_mode_7_),
  _FUNC_NAME(DEC_fetch_oper_load_imm_mem_mode_7_),
  _FUNC_NAME(DEC_fetch_oper_store_imm_mem_mode_4_),
  _FUNC_NAME(DEC_fetch_oper_load_imm_mem_mode_4_),
  _FUNC_NAME(DEC_fetch_oper_store_imm_mem_mode_5_),
  _FUNC_NAME(DEC_fetch_oper_load_imm_mem_mode_5_),
  _FUNC_NAME(DEC_fetch_oper_store_imm_mem_mode_6_),
  _FUNC_NAME(DEC_fetch_oper_load_imm_mem_mode_6_),
  _FUNC_NAME(DEC_fetch_oper_store_imm_mem_mode_7_),
  _FUNC_NAME(DEC_fetch_oper_load_imm_mem_mode_7_),
  _STUB_NAME(stub_0_96),
  _STUB_NAME(stub_0_97),
  _STUB_NAME(stub_0_98),
  _STUB_NAME(stub_0_99),
  _STUB_NAME(stub_0_100),
  _STUB_NAME(stub_0_101),
  _STUB_NAME(stub_0_102),
  _STUB_NAME(stub_0_103),
  _STUB_NAME(stub_0_104),
  _STUB_NAME(stub_0_105),
  _STUB_NAME(stub_0_106),
  _STUB_NAME(stub_0_107),
  _STUB_NAME(stub_0_108),
  _STUB_NAME(stub_0_109),
  _STUB_NAME(stub_0_110),
  _STUB_NAME(stub_0_111),
  _STUB_NAME(stub_0_112),
  _STUB_NAME(stub_0_113),
  _STUB_NAME(stub_0_114),
  _STUB_NAME(stub_0_115),
  _STUB_NAME(stub_0_116),
  _STUB_NAME(stub_0_117),
  _STUB_NAME(stub_0_118),
  _STUB_NAME(stub_0_119),
  _STUB_NAME(stub_0_120),
  _STUB_NAME(stub_0_121),
  _STUB_NAME(stub_0_122),
  _STUB_NAME(stub_0_123),
  _STUB_NAME(stub_0_124),
  _STUB_NAME(stub_0_125),
  _STUB_NAME(stub_0_126),
  _STUB_NAME(stub_0_127),
  _FUNC_NAME(DEC_fetch_oper_stm_noupdate_rn_),
  _FUNC_NAME(DEC_fetch_oper_ldm_noupdate_rn_),
  _FUNC_NAME(DEC_fetch_oper_stm_update_rn_),
  _FUNC_NAME(DEC_fetch_oper_ldm_update_rn_),
  _FUNC_NAME(DEC_fetch_oper_stm_noupdate_rn_),
  _FUNC_NAME(DEC_fetch_oper_ldm_noupdate_rn_),
  _FUNC_NAME(DEC_fetch_oper_stm_update_rn_),
  _FUNC_NAME(DEC_fetch_oper_ldm_update_rn_),
  _FUNC_NAME(DEC_fetch_oper_stm_noupdate_rn_),
  _FUNC_NAME(DEC_fetch_oper_ldm_noupdate_rn_),
  _FUNC_NAME(DEC_fetch_oper_stm_update_rn_),
  _FUNC_NAME(DEC_fetch_oper_ldm_update_rn_),
  _FUNC_NAME(DEC_fetch_oper_stm_noupdate_rn_),
  _FUNC_NAME(DEC_fetch_oper_ldm_noupdate_rn_),
  _FUNC_NAME(DEC_fetch_oper_stm_update_rn_),
  _FUNC_NAME(DEC_fetch_oper_ldm_update_rn_),
  _FUNC_NAME(DEC_fetch_oper_stm_noupdate_rn_),
  _FUNC_NAME(DEC_fetch_oper_ldm_noupdate_rn_),
  _FUNC_NAME(DEC_fetch_oper_stm_update_rn_),
  _FUNC_NAME(DEC_fetch_oper_ldm_update_rn_),
  _FUNC_NAME(DEC_fetch_oper_stm_noupdate_rn_),
  _FUNC_NAME(DEC_fetch_oper_ldm_noupdate_rn_),
  _FUNC_NAME(DEC_fetch_oper_stm_update_rn_),
  _FUNC_NAME(DEC_fetch_oper_ldm_update_rn_),
  _FUNC_NAME(DEC_fetch_oper_stm_noupdate_rn_),
  _FUNC_NAME(DEC_fetch_oper_ldm_noupdate_rn_),
  _FUNC_NAME(DEC_fetch_oper_stm_update_rn_),
  _FUNC_NAME(DEC_fetch_oper_ldm_update_rn_),
  _FUNC_NAME(DEC_fetch_oper_stm_noupdate_rn_),
  _FUNC_NAME(DEC_fetch_oper_ldm_noupdate_rn_),
  _FUNC_NAME(DEC_fetch_oper_stm_update_rn_),
  _FUNC_NAME(DEC_fetch_oper_ldm_update_rn_),
  _FUNC_NAME(DEC_fetch_oper_branch_),
  _FUNC_NAME(DEC_fetch_oper_branch_),
  _FUNC_NAME(DEC_fetch_oper_branch_),
  _FUNC_NAME(DEC_fetch_oper_branch_),
  _FUNC_NAME(DEC_fetch_oper_branch_),
  _FUNC_NAME(DEC_fetch_oper_branch_),
  _FUNC_NAME(DEC_fetch_oper_branch_),
  _FUNC_NAME(DEC_fetch_oper_branch_),
  _FUNC_NAME(DEC_fetch_oper_branch_),
  _FUNC_NAME(DEC_fetch_oper_branch_),
  _FUNC_NAME(DEC_fetch_oper_branch_),
  _FUNC_NAME(DEC_fetch_oper_branch_),
  _FUNC_NAME(DEC_fetch_oper_branch_),
  _FUNC_NAME(DEC_fetch_oper_branch_),
  _FUNC_NAME(DEC_fetch_oper_branch_),
  _FUNC_NAME(DEC_fetch_oper_branch_),
  _FUNC_NAME(DEC_fetch_oper_branch_link_),
  _FUNC_NAME(DEC_fetch_oper_branch_link_),
  _FUNC_NAME(DEC_fetch_oper_branch_link_),
  _FUNC_NAME(DEC_fetch_oper_branch_link_),
  _FUNC_NAME(DEC_fetch_oper_branch_link_),
  _FUNC_NAME(DEC_fetch_oper_branch_link_),
  _FUNC_NAME(DEC_fetch_oper_branch_link_),
  _FUNC_NAME(DEC_fetch_oper_branch_link_),
  _FUNC_NAME(DEC_fetch_oper_branch_link_),
  _FUNC_NAME(DEC_fetch_oper_branch_link_),
  _FUNC_NAME(DEC_fetch_oper_branch_link_),
  _FUNC_NAME(DEC_fetch_oper_branch_link_),
  _FUNC_NAME(DEC_fetch_oper_branch_link_),
  _FUNC_NAME(DEC_fetch_oper_branch_link_),
  _FUNC_NAME(DEC_fetch_oper_branch_link_),
  _FUNC_NAME(DEC_fetch_oper_branch_link_),
  _FUNC_NAME(DEC_fetch_oper_fpe_),
  _FUNC_NAME(DEC_fetch_oper_fpe_),
  _FUNC_NAME(DEC_fetch_oper_fpe_),
  _FUNC_NAME(DEC_fetch_oper_fpe_),
  _FUNC_NAME(DEC_fetch_oper_fpe_),
  _FUNC_NAME(DEC_fetch_oper_fpe_),
  _FUNC_NAME(DEC_fetch_oper_fpe_),
  _FUNC_NAME(DEC_fetch_oper_fpe_),
  _FUNC_NAME(DEC_fetch_oper_fpe_),
  _FUNC_NAME(DEC_fetch_oper_fpe_),
  _FUNC_NAME(DEC_fetch_oper_fpe_),
  _FUNC_NAME(DEC_fetch_oper_fpe_),
  _FUNC_NAME(DEC_fetch_oper_fpe_),
  _FUNC_NAME(DEC_fetch_oper_fpe_),
  _FUNC_NAME(DEC_fetch_oper_fpe_),
  _FUNC_NAME(DEC_fetch_oper_fpe_),
  _STUB_NAME(stub_0_208),
  _STUB_NAME(stub_0_209),
  _STUB_NAME(stub_0_210),
  _STUB_NAME(stub_0_211),
  _STUB_NAME(stub_0_212),
  _STUB_NAME(stub_0_213),
  _STUB_NAME(stub_0_214),
  _STUB_NAME(stub_0_215),
  _STUB_NAME(stub_0_216),
  _STUB_NAME(stub_0_217),
  _STUB_NAME(stub_0_218),
  _STUB_NAME(stub_0_219),
  _STUB_NAME(stub_0_220),
  _STUB_NAME(stub_0_221),
  _STUB_NAME(stub_0_222),
  _STUB_NAME(stub_0_223),
  _FUNC_NAME(DEC_fetch_oper_fpe_),
  _FUNC_NAME(DEC_fetch_oper_fpe_),
  _FUNC_NAME(DEC_fetch_oper_fpe_),
  _FUNC_NAME(DEC_fetch_oper_fpe_),
  _FUNC_NAME(DEC_fetch_oper_fpe_),
  _FUNC_NAME(DEC_fetch_oper_fpe_),
  _FUNC_NAME(DEC_fetch_oper_fpe_),
  _FUNC_NAME(DEC_fetch_oper_fpe_),
  _FUNC_NAME(DEC_fetch_oper_fpe_),
  _FUNC_NAME(DEC_fetch_oper_fpe_),
  _FUNC_NAME(DEC_fetch_oper_fpe_),
  _FUNC_NAME(DEC_fetch_oper_fpe_),
  _FUNC_NAME(DEC_fetch_oper_fpe_),
  _FUNC_NAME(DEC_fetch_oper_fpe_),
  _FUNC_NAME(DEC_fetch_oper_fpe_),
  _FUNC_NAME(DEC_fetch_oper_fpe_),
  _FUNC_NAME(DEC_fetch_oper_syscall_),
  _FUNC_NAME(DEC_fetch_oper_syscall_),
  _FUNC_NAME(DEC_fetch_oper_syscall_),
  _FUNC_NAME(DEC_fetch_oper_syscall_),
  _FUNC_NAME(DEC_fetch_oper_syscall_),
  _FUNC_NAME(DEC_fetch_oper_syscall_),
  _FUNC_NAME(DEC_fetch_oper_syscall_),
  _FUNC_NAME(DEC_fetch_oper_syscall_),
  _FUNC_NAME(DEC_fetch_oper_syscall_),
  _FUNC_NAME(DEC_fetch_oper_syscall_),
  _FUNC_NAME(DEC_fetch_oper_syscall_),
  _FUNC_NAME(DEC_fetch_oper_syscall_),
  _FUNC_NAME(DEC_fetch_oper_syscall_),
  _FUNC_NAME(DEC_fetch_oper_syscall_),
  _FUNC_NAME(DEC_fetch_oper_syscall_),
  _FUNC_NAME(DEC_fetch_oper_syscall_),
_TABLE_DEF_END

/*
Table begin: index=0 size=31
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x000000d0
Prob=0.0242863605	Entropy=3.77658966	HTreeHeight=3.85
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x000000c0	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_and_
mask=0x0ff00170	sig=0x00000140	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_and_
mask=0x0ff00270	sig=0x00000240	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_and_
mask=0x0ff00470	sig=0x00000440	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_and_
mask=0x0ff00870	sig=0x00000840	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_and_
mask=0x0ff00870	sig=0x00000800	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_and_
mask=0x0ff00470	sig=0x00000400	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_and_
mask=0x0ff00270	sig=0x00000200	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_and_
mask=0x0ff00170	sig=0x00000100	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_and_
mask=0x0ff000f0	sig=0x00000080	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_and_
mask=0x0ff000f0	sig=0x000000a0	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_and_
mask=0x0ff00170	sig=0x00000120	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_and_
mask=0x0ff00270	sig=0x00000220	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_and_
mask=0x0ff00470	sig=0x00000420	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_and_
mask=0x0ff00870	sig=0x00000820	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_and_
mask=0x0ff00870	sig=0x00000860	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_and_
mask=0x0ff00470	sig=0x00000460	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_and_
mask=0x0ff00270	sig=0x00000260	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_and_
mask=0x0ff00170	sig=0x00000160	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_and_
mask=0x0ff000f0	sig=0x000000e0	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_and_
mask=0x0ff00ff0	sig=0x00000040	prob=0.075	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_and_
mask=0x0ff00ff0	sig=0x00000000	prob=0.075	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_and_
mask=0x0ff00ff0	sig=0x00000020	prob=0.075	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_and_
mask=0x0ff00ff0	sig=0x00000060	prob=0.075	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_and_
mask=0x0ff000f0	sig=0x00000050	prob=0.075	name=DEC_fetch_oper_dpi_rshift_binop_r_asr_and_
mask=0x0ff000f0	sig=0x00000010	prob=0.075	name=DEC_fetch_oper_dpi_rshift_binop_r_lsl_and_
mask=0x0ff000f0	sig=0x00000030	prob=0.075	name=DEC_fetch_oper_dpi_rshift_binop_r_lsr_and_
mask=0x0ff000f0	sig=0x00000070	prob=0.075	name=DEC_fetch_oper_dpi_rshift_binop_r_ror_and_
mask=0x0ff000f0	sig=0x00000090	prob=0.075	name=DEC_fetch_oper_mult_mul_
mask=0x0ff000b0	sig=0x000000b0	prob=0.0125	name=DEC_fetch_oper_store_ext_reg_mem_mode_0_
mask=0x0ff000d0	sig=0x000000d0	prob=0.0125	name=DEC_fetch_oper_store_ext_reg_mem_mode_0_
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_1, 16)
  _STUB_NAME(stub_1_0),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_lsl_and_),
  _STUB_NAME(stub_1_2),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_lsr_and_),
  _STUB_NAME(stub_1_4),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_asr_and_),
  _STUB_NAME(stub_1_6),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_ror_and_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_and_),
  _FUNC_NAME(DEC_fetch_oper_mult_mul_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_and_),
  _FUNC_NAME(DEC_fetch_oper_store_ext_reg_mem_mode_0_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_and_),
  _FUNC_NAME(DEC_fetch_oper_store_ext_reg_mem_mode_0_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_and_),
  _FUNC_NAME(DEC_fetch_oper_store_ext_reg_mem_mode_0_),
_TABLE_DEF_END

/*
Table begin: index=1 size=33
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x00100090
Prob=0.0248935195	Entropy=3.80112426	HTreeHeight=3.85365854
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x001000c0	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_ands_
mask=0x0ff00170	sig=0x00100140	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_ands_
mask=0x0ff00270	sig=0x00100240	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_ands_
mask=0x0ff00470	sig=0x00100440	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_ands_
mask=0x0ff00870	sig=0x00100840	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_ands_
mask=0x0ff00870	sig=0x00100800	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_ands_
mask=0x0ff00470	sig=0x00100400	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_ands_
mask=0x0ff00270	sig=0x00100200	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_ands_
mask=0x0ff00170	sig=0x00100100	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_ands_
mask=0x0ff000f0	sig=0x00100080	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_ands_
mask=0x0ff000f0	sig=0x001000a0	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_ands_
mask=0x0ff00170	sig=0x00100120	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_ands_
mask=0x0ff00270	sig=0x00100220	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_ands_
mask=0x0ff00470	sig=0x00100420	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_ands_
mask=0x0ff00870	sig=0x00100820	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_ands_
mask=0x0ff00870	sig=0x00100860	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_ands_
mask=0x0ff00470	sig=0x00100460	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_ands_
mask=0x0ff00270	sig=0x00100260	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_ands_
mask=0x0ff00170	sig=0x00100160	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_ands_
mask=0x0ff000f0	sig=0x001000e0	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_ands_
mask=0x0ff00ff0	sig=0x00100040	prob=0.0731707317	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_ands_
mask=0x0ff00ff0	sig=0x00100000	prob=0.0731707317	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_ands_
mask=0x0ff00ff0	sig=0x00100020	prob=0.0731707317	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_ands_
mask=0x0ff00ff0	sig=0x00100060	prob=0.0731707317	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_ands_
mask=0x0ff000f0	sig=0x00100050	prob=0.0731707317	name=DEC_fetch_oper_dpi_rshift_binop_r_asr_ands_
mask=0x0ff000f0	sig=0x00100010	prob=0.0731707317	name=DEC_fetch_oper_dpi_rshift_binop_r_lsl_ands_
mask=0x0ff000f0	sig=0x00100030	prob=0.0731707317	name=DEC_fetch_oper_dpi_rshift_binop_r_lsr_ands_
mask=0x0ff000f0	sig=0x00100070	prob=0.0731707317	name=DEC_fetch_oper_dpi_rshift_binop_r_ror_ands_
mask=0x0ff000b0	sig=0x001000b0	prob=0.012195122	name=DEC_fetch_oper_load_ext_reg_mem_mode_0_
mask=0x0ff000d0	sig=0x001000d0	prob=0.012195122	name=DEC_fetch_oper_load_ext_reg_mem_mode_0_
mask=0x0ff000b0	sig=0x001000b0	prob=0.012195122	name=DEC_fetch_oper_load_ext_reg_mem_mode_0_
mask=0x0ff000d0	sig=0x001000d0	prob=0.012195122	name=DEC_fetch_oper_load_ext_reg_mem_mode_0_
mask=0x0ff000f0	sig=0x00100090	prob=0.0731707317	name=DEC_fetch_oper_mult_muls_
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_2, 16)
  _STUB_NAME(stub_2_0),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_lsl_ands_),
  _STUB_NAME(stub_2_2),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_lsr_ands_),
  _STUB_NAME(stub_2_4),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_asr_ands_),
  _STUB_NAME(stub_2_6),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_ror_ands_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_ands_),
  _FUNC_NAME(DEC_fetch_oper_mult_muls_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_ands_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_reg_mem_mode_0_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_ands_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_reg_mem_mode_0_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_ands_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_reg_mem_mode_0_),
_TABLE_DEF_END

/*
Table begin: index=2 size=31
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x002000d0
Prob=0.0242863605	Entropy=3.77658966	HTreeHeight=3.85
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x002000c0	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_eor_
mask=0x0ff00170	sig=0x00200140	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_eor_
mask=0x0ff00270	sig=0x00200240	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_eor_
mask=0x0ff00470	sig=0x00200440	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_eor_
mask=0x0ff00870	sig=0x00200840	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_eor_
mask=0x0ff00870	sig=0x00200800	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_eor_
mask=0x0ff00470	sig=0x00200400	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_eor_
mask=0x0ff00270	sig=0x00200200	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_eor_
mask=0x0ff00170	sig=0x00200100	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_eor_
mask=0x0ff000f0	sig=0x00200080	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_eor_
mask=0x0ff000f0	sig=0x002000a0	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_eor_
mask=0x0ff00170	sig=0x00200120	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_eor_
mask=0x0ff00270	sig=0x00200220	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_eor_
mask=0x0ff00470	sig=0x00200420	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_eor_
mask=0x0ff00870	sig=0x00200820	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_eor_
mask=0x0ff00870	sig=0x00200860	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_eor_
mask=0x0ff00470	sig=0x00200460	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_eor_
mask=0x0ff00270	sig=0x00200260	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_eor_
mask=0x0ff00170	sig=0x00200160	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_eor_
mask=0x0ff000f0	sig=0x002000e0	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_eor_
mask=0x0ff00ff0	sig=0x00200040	prob=0.075	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_eor_
mask=0x0ff00ff0	sig=0x00200000	prob=0.075	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_eor_
mask=0x0ff00ff0	sig=0x00200020	prob=0.075	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_eor_
mask=0x0ff00ff0	sig=0x00200060	prob=0.075	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_eor_
mask=0x0ff000f0	sig=0x00200050	prob=0.075	name=DEC_fetch_oper_dpi_rshift_binop_r_asr_eor_
mask=0x0ff000f0	sig=0x00200010	prob=0.075	name=DEC_fetch_oper_dpi_rshift_binop_r_lsl_eor_
mask=0x0ff000f0	sig=0x00200030	prob=0.075	name=DEC_fetch_oper_dpi_rshift_binop_r_lsr_eor_
mask=0x0ff000f0	sig=0x00200070	prob=0.075	name=DEC_fetch_oper_dpi_rshift_binop_r_ror_eor_
mask=0x0ff000f0	sig=0x00200090	prob=0.075	name=DEC_fetch_oper_mult_mla_
mask=0x0ff000b0	sig=0x002000b0	prob=0.0125	name=DEC_fetch_oper_store_ext_reg_mem_mode_1_
mask=0x0ff000d0	sig=0x002000d0	prob=0.0125	name=DEC_fetch_oper_store_ext_reg_mem_mode_1_
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_3, 16)
  _STUB_NAME(stub_3_0),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_lsl_eor_),
  _STUB_NAME(stub_3_2),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_lsr_eor_),
  _STUB_NAME(stub_3_4),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_asr_eor_),
  _STUB_NAME(stub_3_6),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_ror_eor_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_eor_),
  _FUNC_NAME(DEC_fetch_oper_mult_mla_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_eor_),
  _FUNC_NAME(DEC_fetch_oper_store_ext_reg_mem_mode_1_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_eor_),
  _FUNC_NAME(DEC_fetch_oper_store_ext_reg_mem_mode_1_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_eor_),
  _FUNC_NAME(DEC_fetch_oper_store_ext_reg_mem_mode_1_),
_TABLE_DEF_END

/*
Table begin: index=3 size=33
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x00300090
Prob=0.0248935195	Entropy=3.80112426	HTreeHeight=3.85365854
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x003000c0	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_eors_
mask=0x0ff00170	sig=0x00300140	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_eors_
mask=0x0ff00270	sig=0x00300240	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_eors_
mask=0x0ff00470	sig=0x00300440	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_eors_
mask=0x0ff00870	sig=0x00300840	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_eors_
mask=0x0ff00870	sig=0x00300800	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_eors_
mask=0x0ff00470	sig=0x00300400	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_eors_
mask=0x0ff00270	sig=0x00300200	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_eors_
mask=0x0ff00170	sig=0x00300100	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_eors_
mask=0x0ff000f0	sig=0x00300080	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_eors_
mask=0x0ff000f0	sig=0x003000a0	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_eors_
mask=0x0ff00170	sig=0x00300120	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_eors_
mask=0x0ff00270	sig=0x00300220	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_eors_
mask=0x0ff00470	sig=0x00300420	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_eors_
mask=0x0ff00870	sig=0x00300820	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_eors_
mask=0x0ff00870	sig=0x00300860	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_eors_
mask=0x0ff00470	sig=0x00300460	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_eors_
mask=0x0ff00270	sig=0x00300260	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_eors_
mask=0x0ff00170	sig=0x00300160	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_eors_
mask=0x0ff000f0	sig=0x003000e0	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_eors_
mask=0x0ff00ff0	sig=0x00300040	prob=0.0731707317	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_eors_
mask=0x0ff00ff0	sig=0x00300000	prob=0.0731707317	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_eors_
mask=0x0ff00ff0	sig=0x00300020	prob=0.0731707317	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_eors_
mask=0x0ff00ff0	sig=0x00300060	prob=0.0731707317	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_eors_
mask=0x0ff000f0	sig=0x00300050	prob=0.0731707317	name=DEC_fetch_oper_dpi_rshift_binop_r_asr_eors_
mask=0x0ff000f0	sig=0x00300010	prob=0.0731707317	name=DEC_fetch_oper_dpi_rshift_binop_r_lsl_eors_
mask=0x0ff000f0	sig=0x00300030	prob=0.0731707317	name=DEC_fetch_oper_dpi_rshift_binop_r_lsr_eors_
mask=0x0ff000f0	sig=0x00300070	prob=0.0731707317	name=DEC_fetch_oper_dpi_rshift_binop_r_ror_eors_
mask=0x0ff000b0	sig=0x003000b0	prob=0.012195122	name=DEC_fetch_oper_load_ext_reg_mem_mode_1_
mask=0x0ff000d0	sig=0x003000d0	prob=0.012195122	name=DEC_fetch_oper_load_ext_reg_mem_mode_1_
mask=0x0ff000b0	sig=0x003000b0	prob=0.012195122	name=DEC_fetch_oper_load_ext_reg_mem_mode_1_
mask=0x0ff000d0	sig=0x003000d0	prob=0.012195122	name=DEC_fetch_oper_load_ext_reg_mem_mode_1_
mask=0x0ff000f0	sig=0x00300090	prob=0.0731707317	name=DEC_fetch_oper_mult_mlas_
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_4, 16)
  _STUB_NAME(stub_4_0),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_lsl_eors_),
  _STUB_NAME(stub_4_2),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_lsr_eors_),
  _STUB_NAME(stub_4_4),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_asr_eors_),
  _STUB_NAME(stub_4_6),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_ror_eors_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_eors_),
  _FUNC_NAME(DEC_fetch_oper_mult_mlas_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_eors_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_reg_mem_mode_1_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_eors_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_reg_mem_mode_1_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_eors_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_reg_mem_mode_1_),
_TABLE_DEF_END

/*
Table begin: index=4 size=31
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x00400090
Prob=0.0230720425	Entropy=3.69375251	HTreeHeight=3.76315789
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x004000c0	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_sub_
mask=0x0ff00170	sig=0x00400140	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_sub_
mask=0x0ff00270	sig=0x00400240	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_sub_
mask=0x0ff00470	sig=0x00400440	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_sub_
mask=0x0ff00870	sig=0x00400840	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_sub_
mask=0x0ff00870	sig=0x00400800	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_sub_
mask=0x0ff00470	sig=0x00400400	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_sub_
mask=0x0ff00270	sig=0x00400200	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_sub_
mask=0x0ff00170	sig=0x00400100	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_sub_
mask=0x0ff000f0	sig=0x00400080	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_sub_
mask=0x0ff000f0	sig=0x004000a0	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_sub_
mask=0x0ff00170	sig=0x00400120	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_sub_
mask=0x0ff00270	sig=0x00400220	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_sub_
mask=0x0ff00470	sig=0x00400420	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_sub_
mask=0x0ff00870	sig=0x00400820	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_sub_
mask=0x0ff00870	sig=0x00400860	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_sub_
mask=0x0ff00470	sig=0x00400460	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_sub_
mask=0x0ff00270	sig=0x00400260	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_sub_
mask=0x0ff00170	sig=0x00400160	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_sub_
mask=0x0ff000f0	sig=0x004000e0	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_sub_
mask=0x0ff00ff0	sig=0x00400040	prob=0.0789473684	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_sub_
mask=0x0ff00ff0	sig=0x00400000	prob=0.0789473684	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_sub_
mask=0x0ff00ff0	sig=0x00400020	prob=0.0789473684	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_sub_
mask=0x0ff00ff0	sig=0x00400060	prob=0.0789473684	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_sub_
mask=0x0ff000f0	sig=0x00400050	prob=0.0789473684	name=DEC_fetch_oper_dpi_rshift_binop_r_asr_sub_
mask=0x0ff000f0	sig=0x00400010	prob=0.0789473684	name=DEC_fetch_oper_dpi_rshift_binop_r_lsl_sub_
mask=0x0ff000f0	sig=0x00400030	prob=0.0789473684	name=DEC_fetch_oper_dpi_rshift_binop_r_lsr_sub_
mask=0x0ff000f0	sig=0x00400070	prob=0.0789473684	name=DEC_fetch_oper_dpi_rshift_binop_r_ror_sub_
mask=0x0ff000b0	sig=0x004000b0	prob=0.0131578947	name=DEC_fetch_oper_store_ext_imm_mem_mode_2_
mask=0x0ff000d0	sig=0x004000d0	prob=0.0131578947	name=DEC_fetch_oper_store_ext_imm_mem_mode_2_
mask=0x0ff00090	sig=0x00400090	prob=0.0263157895	name=DEC_fetch_oper_store_ext_imm_mem_mode_2_
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_5, 16)
  _STUB_NAME(stub_5_0),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_lsl_sub_),
  _STUB_NAME(stub_5_2),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_lsr_sub_),
  _STUB_NAME(stub_5_4),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_asr_sub_),
  _STUB_NAME(stub_5_6),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_ror_sub_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_sub_),
  _FUNC_NAME(DEC_fetch_oper_store_ext_imm_mem_mode_2_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_sub_),
  _FUNC_NAME(DEC_fetch_oper_store_ext_imm_mem_mode_2_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_sub_),
  _FUNC_NAME(DEC_fetch_oper_store_ext_imm_mem_mode_2_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_sub_),
  _FUNC_NAME(DEC_fetch_oper_store_ext_imm_mem_mode_2_),
_TABLE_DEF_END

/*
Table begin: index=5 size=31
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x00500090
Prob=0.0230720425	Entropy=3.69375251	HTreeHeight=3.76315789
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x005000c0	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_subs_
mask=0x0ff00170	sig=0x00500140	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_subs_
mask=0x0ff00270	sig=0x00500240	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_subs_
mask=0x0ff00470	sig=0x00500440	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_subs_
mask=0x0ff00870	sig=0x00500840	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_subs_
mask=0x0ff00870	sig=0x00500800	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_subs_
mask=0x0ff00470	sig=0x00500400	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_subs_
mask=0x0ff00270	sig=0x00500200	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_subs_
mask=0x0ff00170	sig=0x00500100	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_subs_
mask=0x0ff000f0	sig=0x00500080	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_subs_
mask=0x0ff000f0	sig=0x005000a0	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_subs_
mask=0x0ff00170	sig=0x00500120	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_subs_
mask=0x0ff00270	sig=0x00500220	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_subs_
mask=0x0ff00470	sig=0x00500420	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_subs_
mask=0x0ff00870	sig=0x00500820	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_subs_
mask=0x0ff00870	sig=0x00500860	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_subs_
mask=0x0ff00470	sig=0x00500460	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_subs_
mask=0x0ff00270	sig=0x00500260	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_subs_
mask=0x0ff00170	sig=0x00500160	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_subs_
mask=0x0ff000f0	sig=0x005000e0	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_subs_
mask=0x0ff00ff0	sig=0x00500040	prob=0.0789473684	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_subs_
mask=0x0ff00ff0	sig=0x00500000	prob=0.0789473684	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_subs_
mask=0x0ff00ff0	sig=0x00500020	prob=0.0789473684	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_subs_
mask=0x0ff00ff0	sig=0x00500060	prob=0.0789473684	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_subs_
mask=0x0ff000f0	sig=0x00500050	prob=0.0789473684	name=DEC_fetch_oper_dpi_rshift_binop_r_asr_subs_
mask=0x0ff000f0	sig=0x00500010	prob=0.0789473684	name=DEC_fetch_oper_dpi_rshift_binop_r_lsl_subs_
mask=0x0ff000f0	sig=0x00500030	prob=0.0789473684	name=DEC_fetch_oper_dpi_rshift_binop_r_lsr_subs_
mask=0x0ff000f0	sig=0x00500070	prob=0.0789473684	name=DEC_fetch_oper_dpi_rshift_binop_r_ror_subs_
mask=0x0ff000b0	sig=0x005000b0	prob=0.0131578947	name=DEC_fetch_oper_load_ext_imm_mem_mode_2_
mask=0x0ff000d0	sig=0x005000d0	prob=0.0131578947	name=DEC_fetch_oper_load_ext_imm_mem_mode_2_
mask=0x0ff00090	sig=0x00500090	prob=0.0263157895	name=DEC_fetch_oper_load_ext_imm_mem_mode_2_
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_6, 16)
  _STUB_NAME(stub_6_0),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_lsl_subs_),
  _STUB_NAME(stub_6_2),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_lsr_subs_),
  _STUB_NAME(stub_6_4),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_asr_subs_),
  _STUB_NAME(stub_6_6),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_ror_subs_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_subs_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_imm_mem_mode_2_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_subs_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_imm_mem_mode_2_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_subs_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_imm_mem_mode_2_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_subs_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_imm_mem_mode_2_),
_TABLE_DEF_END

/*
Table begin: index=6 size=31
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x00600090
Prob=0.0230720425	Entropy=3.69375251	HTreeHeight=3.76315789
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x006000c0	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_rsb_
mask=0x0ff00170	sig=0x00600140	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_rsb_
mask=0x0ff00270	sig=0x00600240	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_rsb_
mask=0x0ff00470	sig=0x00600440	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_rsb_
mask=0x0ff00870	sig=0x00600840	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_rsb_
mask=0x0ff00870	sig=0x00600800	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_rsb_
mask=0x0ff00470	sig=0x00600400	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_rsb_
mask=0x0ff00270	sig=0x00600200	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_rsb_
mask=0x0ff00170	sig=0x00600100	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_rsb_
mask=0x0ff000f0	sig=0x00600080	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_rsb_
mask=0x0ff000f0	sig=0x006000a0	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_rsb_
mask=0x0ff00170	sig=0x00600120	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_rsb_
mask=0x0ff00270	sig=0x00600220	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_rsb_
mask=0x0ff00470	sig=0x00600420	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_rsb_
mask=0x0ff00870	sig=0x00600820	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_rsb_
mask=0x0ff00870	sig=0x00600860	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_rsb_
mask=0x0ff00470	sig=0x00600460	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_rsb_
mask=0x0ff00270	sig=0x00600260	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_rsb_
mask=0x0ff00170	sig=0x00600160	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_rsb_
mask=0x0ff000f0	sig=0x006000e0	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_rsb_
mask=0x0ff00ff0	sig=0x00600040	prob=0.0789473684	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_rsb_
mask=0x0ff00ff0	sig=0x00600000	prob=0.0789473684	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_rsb_
mask=0x0ff00ff0	sig=0x00600020	prob=0.0789473684	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_rsb_
mask=0x0ff00ff0	sig=0x00600060	prob=0.0789473684	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_rsb_
mask=0x0ff000f0	sig=0x00600050	prob=0.0789473684	name=DEC_fetch_oper_dpi_rshift_binop_r_asr_rsb_
mask=0x0ff000f0	sig=0x00600010	prob=0.0789473684	name=DEC_fetch_oper_dpi_rshift_binop_r_lsl_rsb_
mask=0x0ff000f0	sig=0x00600030	prob=0.0789473684	name=DEC_fetch_oper_dpi_rshift_binop_r_lsr_rsb_
mask=0x0ff000f0	sig=0x00600070	prob=0.0789473684	name=DEC_fetch_oper_dpi_rshift_binop_r_ror_rsb_
mask=0x0ff000b0	sig=0x006000b0	prob=0.0131578947	name=DEC_fetch_oper_store_ext_imm_mem_mode_3_
mask=0x0ff000d0	sig=0x006000d0	prob=0.0131578947	name=DEC_fetch_oper_store_ext_imm_mem_mode_3_
mask=0x0ff00090	sig=0x00600090	prob=0.0263157895	name=DEC_fetch_oper_store_ext_imm_mem_mode_3_
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_7, 16)
  _STUB_NAME(stub_7_0),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_lsl_rsb_),
  _STUB_NAME(stub_7_2),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_lsr_rsb_),
  _STUB_NAME(stub_7_4),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_asr_rsb_),
  _STUB_NAME(stub_7_6),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_ror_rsb_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_rsb_),
  _FUNC_NAME(DEC_fetch_oper_store_ext_imm_mem_mode_3_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_rsb_),
  _FUNC_NAME(DEC_fetch_oper_store_ext_imm_mem_mode_3_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_rsb_),
  _FUNC_NAME(DEC_fetch_oper_store_ext_imm_mem_mode_3_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_rsb_),
  _FUNC_NAME(DEC_fetch_oper_store_ext_imm_mem_mode_3_),
_TABLE_DEF_END

/*
Table begin: index=7 size=31
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x00700090
Prob=0.0230720425	Entropy=3.69375251	HTreeHeight=3.76315789
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x007000c0	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_rsbs_
mask=0x0ff00170	sig=0x00700140	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_rsbs_
mask=0x0ff00270	sig=0x00700240	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_rsbs_
mask=0x0ff00470	sig=0x00700440	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_rsbs_
mask=0x0ff00870	sig=0x00700840	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_rsbs_
mask=0x0ff00870	sig=0x00700800	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_rsbs_
mask=0x0ff00470	sig=0x00700400	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_rsbs_
mask=0x0ff00270	sig=0x00700200	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_rsbs_
mask=0x0ff00170	sig=0x00700100	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_rsbs_
mask=0x0ff000f0	sig=0x00700080	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_rsbs_
mask=0x0ff000f0	sig=0x007000a0	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_rsbs_
mask=0x0ff00170	sig=0x00700120	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_rsbs_
mask=0x0ff00270	sig=0x00700220	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_rsbs_
mask=0x0ff00470	sig=0x00700420	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_rsbs_
mask=0x0ff00870	sig=0x00700820	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_rsbs_
mask=0x0ff00870	sig=0x00700860	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_rsbs_
mask=0x0ff00470	sig=0x00700460	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_rsbs_
mask=0x0ff00270	sig=0x00700260	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_rsbs_
mask=0x0ff00170	sig=0x00700160	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_rsbs_
mask=0x0ff000f0	sig=0x007000e0	prob=0.0157894737	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_rsbs_
mask=0x0ff00ff0	sig=0x00700040	prob=0.0789473684	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_rsbs_
mask=0x0ff00ff0	sig=0x00700000	prob=0.0789473684	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_rsbs_
mask=0x0ff00ff0	sig=0x00700020	prob=0.0789473684	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_rsbs_
mask=0x0ff00ff0	sig=0x00700060	prob=0.0789473684	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_rsbs_
mask=0x0ff000f0	sig=0x00700050	prob=0.0789473684	name=DEC_fetch_oper_dpi_rshift_binop_r_asr_rsbs_
mask=0x0ff000f0	sig=0x00700010	prob=0.0789473684	name=DEC_fetch_oper_dpi_rshift_binop_r_lsl_rsbs_
mask=0x0ff000f0	sig=0x00700030	prob=0.0789473684	name=DEC_fetch_oper_dpi_rshift_binop_r_lsr_rsbs_
mask=0x0ff000f0	sig=0x00700070	prob=0.0789473684	name=DEC_fetch_oper_dpi_rshift_binop_r_ror_rsbs_
mask=0x0ff000b0	sig=0x007000b0	prob=0.0131578947	name=DEC_fetch_oper_load_ext_imm_mem_mode_3_
mask=0x0ff000d0	sig=0x007000d0	prob=0.0131578947	name=DEC_fetch_oper_load_ext_imm_mem_mode_3_
mask=0x0ff00090	sig=0x00700090	prob=0.0263157895	name=DEC_fetch_oper_load_ext_imm_mem_mode_3_
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_8, 16)
  _STUB_NAME(stub_8_0),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_lsl_rsbs_),
  _STUB_NAME(stub_8_2),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_lsr_rsbs_),
  _STUB_NAME(stub_8_4),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_asr_rsbs_),
  _STUB_NAME(stub_8_6),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_ror_rsbs_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_rsbs_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_imm_mem_mode_3_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_rsbs_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_imm_mem_mode_3_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_rsbs_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_imm_mem_mode_3_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_rsbs_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_imm_mem_mode_3_),
_TABLE_DEF_END

/*
Table begin: index=8 size=33
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x008000d0
Prob=0.0248935195	Entropy=3.80112426	HTreeHeight=3.85365854
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x008000c0	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_add_
mask=0x0ff00170	sig=0x00800140	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_add_
mask=0x0ff00270	sig=0x00800240	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_add_
mask=0x0ff00470	sig=0x00800440	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_add_
mask=0x0ff00870	sig=0x00800840	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_add_
mask=0x0ff00870	sig=0x00800800	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_add_
mask=0x0ff00470	sig=0x00800400	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_add_
mask=0x0ff00270	sig=0x00800200	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_add_
mask=0x0ff00170	sig=0x00800100	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_add_
mask=0x0ff000f0	sig=0x00800080	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_add_
mask=0x0ff000f0	sig=0x008000a0	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_add_
mask=0x0ff00170	sig=0x00800120	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_add_
mask=0x0ff00270	sig=0x00800220	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_add_
mask=0x0ff00470	sig=0x00800420	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_add_
mask=0x0ff00870	sig=0x00800820	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_add_
mask=0x0ff00870	sig=0x00800860	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_add_
mask=0x0ff00470	sig=0x00800460	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_add_
mask=0x0ff00270	sig=0x00800260	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_add_
mask=0x0ff00170	sig=0x00800160	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_add_
mask=0x0ff000f0	sig=0x008000e0	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_add_
mask=0x0ff00ff0	sig=0x00800040	prob=0.0731707317	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_add_
mask=0x0ff00ff0	sig=0x00800000	prob=0.0731707317	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_add_
mask=0x0ff00ff0	sig=0x00800020	prob=0.0731707317	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_add_
mask=0x0ff00ff0	sig=0x00800060	prob=0.0731707317	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_add_
mask=0x0ff000f0	sig=0x00800050	prob=0.0731707317	name=DEC_fetch_oper_dpi_rshift_binop_r_asr_add_
mask=0x0ff000f0	sig=0x00800010	prob=0.0731707317	name=DEC_fetch_oper_dpi_rshift_binop_r_lsl_add_
mask=0x0ff000f0	sig=0x00800030	prob=0.0731707317	name=DEC_fetch_oper_dpi_rshift_binop_r_lsr_add_
mask=0x0ff000f0	sig=0x00800070	prob=0.0731707317	name=DEC_fetch_oper_dpi_rshift_binop_r_ror_add_
mask=0x0ff000f0	sig=0x00800090	prob=0.0731707317	name=DEC_fetch_oper_mult_long_smull_
mask=0x0ff000b0	sig=0x008000b0	prob=0.012195122	name=DEC_fetch_oper_store_ext_reg_mem_mode_0_
mask=0x0ff000d0	sig=0x008000d0	prob=0.012195122	name=DEC_fetch_oper_store_ext_reg_mem_mode_0_
mask=0x0ff000b0	sig=0x008000b0	prob=0.012195122	name=DEC_fetch_oper_store_ext_reg_mem_mode_0_
mask=0x0ff000d0	sig=0x008000d0	prob=0.012195122	name=DEC_fetch_oper_store_ext_reg_mem_mode_0_
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_9, 16)
  _STUB_NAME(stub_9_0),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_lsl_add_),
  _STUB_NAME(stub_9_2),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_lsr_add_),
  _STUB_NAME(stub_9_4),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_asr_add_),
  _STUB_NAME(stub_9_6),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_ror_add_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_add_),
  _FUNC_NAME(DEC_fetch_oper_mult_long_smull_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_add_),
  _FUNC_NAME(DEC_fetch_oper_store_ext_reg_mem_mode_0_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_add_),
  _FUNC_NAME(DEC_fetch_oper_store_ext_reg_mem_mode_0_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_add_),
  _FUNC_NAME(DEC_fetch_oper_store_ext_reg_mem_mode_0_),
_TABLE_DEF_END

/*
Table begin: index=9 size=31
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x00900090
Prob=0.0242863605	Entropy=3.77658966	HTreeHeight=3.85
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x009000c0	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_adds_
mask=0x0ff00170	sig=0x00900140	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_adds_
mask=0x0ff00270	sig=0x00900240	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_adds_
mask=0x0ff00470	sig=0x00900440	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_adds_
mask=0x0ff00870	sig=0x00900840	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_adds_
mask=0x0ff00870	sig=0x00900800	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_adds_
mask=0x0ff00470	sig=0x00900400	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_adds_
mask=0x0ff00270	sig=0x00900200	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_adds_
mask=0x0ff00170	sig=0x00900100	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_adds_
mask=0x0ff000f0	sig=0x00900080	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_adds_
mask=0x0ff000f0	sig=0x009000a0	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_adds_
mask=0x0ff00170	sig=0x00900120	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_adds_
mask=0x0ff00270	sig=0x00900220	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_adds_
mask=0x0ff00470	sig=0x00900420	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_adds_
mask=0x0ff00870	sig=0x00900820	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_adds_
mask=0x0ff00870	sig=0x00900860	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_adds_
mask=0x0ff00470	sig=0x00900460	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_adds_
mask=0x0ff00270	sig=0x00900260	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_adds_
mask=0x0ff00170	sig=0x00900160	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_adds_
mask=0x0ff000f0	sig=0x009000e0	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_adds_
mask=0x0ff00ff0	sig=0x00900040	prob=0.075	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_adds_
mask=0x0ff00ff0	sig=0x00900000	prob=0.075	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_adds_
mask=0x0ff00ff0	sig=0x00900020	prob=0.075	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_adds_
mask=0x0ff00ff0	sig=0x00900060	prob=0.075	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_adds_
mask=0x0ff000f0	sig=0x00900050	prob=0.075	name=DEC_fetch_oper_dpi_rshift_binop_r_asr_adds_
mask=0x0ff000f0	sig=0x00900010	prob=0.075	name=DEC_fetch_oper_dpi_rshift_binop_r_lsl_adds_
mask=0x0ff000f0	sig=0x00900030	prob=0.075	name=DEC_fetch_oper_dpi_rshift_binop_r_lsr_adds_
mask=0x0ff000f0	sig=0x00900070	prob=0.075	name=DEC_fetch_oper_dpi_rshift_binop_r_ror_adds_
mask=0x0ff000b0	sig=0x009000b0	prob=0.0125	name=DEC_fetch_oper_load_ext_reg_mem_mode_0_
mask=0x0ff000d0	sig=0x009000d0	prob=0.0125	name=DEC_fetch_oper_load_ext_reg_mem_mode_0_
mask=0x0ff000f0	sig=0x00900090	prob=0.075	name=DEC_fetch_oper_mult_long_smulls_
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_10, 16)
  _STUB_NAME(stub_10_0),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_lsl_adds_),
  _STUB_NAME(stub_10_2),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_lsr_adds_),
  _STUB_NAME(stub_10_4),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_asr_adds_),
  _STUB_NAME(stub_10_6),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_ror_adds_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_adds_),
  _FUNC_NAME(DEC_fetch_oper_mult_long_smulls_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_adds_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_reg_mem_mode_0_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_adds_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_reg_mem_mode_0_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_adds_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_reg_mem_mode_0_),
_TABLE_DEF_END

/*
Table begin: index=10 size=33
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x00a000d0
Prob=0.0248935195	Entropy=3.80112426	HTreeHeight=3.85365854
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x00a000c0	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_adc_
mask=0x0ff00170	sig=0x00a00140	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_adc_
mask=0x0ff00270	sig=0x00a00240	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_adc_
mask=0x0ff00470	sig=0x00a00440	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_adc_
mask=0x0ff00870	sig=0x00a00840	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_adc_
mask=0x0ff00870	sig=0x00a00800	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_adc_
mask=0x0ff00470	sig=0x00a00400	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_adc_
mask=0x0ff00270	sig=0x00a00200	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_adc_
mask=0x0ff00170	sig=0x00a00100	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_adc_
mask=0x0ff000f0	sig=0x00a00080	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_adc_
mask=0x0ff000f0	sig=0x00a000a0	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_adc_
mask=0x0ff00170	sig=0x00a00120	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_adc_
mask=0x0ff00270	sig=0x00a00220	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_adc_
mask=0x0ff00470	sig=0x00a00420	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_adc_
mask=0x0ff00870	sig=0x00a00820	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_adc_
mask=0x0ff00870	sig=0x00a00860	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_adc_
mask=0x0ff00470	sig=0x00a00460	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_adc_
mask=0x0ff00270	sig=0x00a00260	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_adc_
mask=0x0ff00170	sig=0x00a00160	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_adc_
mask=0x0ff000f0	sig=0x00a000e0	prob=0.0146341463	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_adc_
mask=0x0ff00ff0	sig=0x00a00040	prob=0.0731707317	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_adc_
mask=0x0ff00ff0	sig=0x00a00000	prob=0.0731707317	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_adc_
mask=0x0ff00ff0	sig=0x00a00020	prob=0.0731707317	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_adc_
mask=0x0ff00ff0	sig=0x00a00060	prob=0.0731707317	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_adc_
mask=0x0ff000f0	sig=0x00a00050	prob=0.0731707317	name=DEC_fetch_oper_dpi_rshift_binop_r_asr_adc_
mask=0x0ff000f0	sig=0x00a00010	prob=0.0731707317	name=DEC_fetch_oper_dpi_rshift_binop_r_lsl_adc_
mask=0x0ff000f0	sig=0x00a00030	prob=0.0731707317	name=DEC_fetch_oper_dpi_rshift_binop_r_lsr_adc_
mask=0x0ff000f0	sig=0x00a00070	prob=0.0731707317	name=DEC_fetch_oper_dpi_rshift_binop_r_ror_adc_
mask=0x0ff000f0	sig=0x00a00090	prob=0.0731707317	name=DEC_fetch_oper_mult_long_smlal_
mask=0x0ff000b0	sig=0x00a000b0	prob=0.012195122	name=DEC_fetch_oper_store_ext_reg_mem_mode_1_
mask=0x0ff000d0	sig=0x00a000d0	prob=0.012195122	name=DEC_fetch_oper_store_ext_reg_mem_mode_1_
mask=0x0ff000b0	sig=0x00a000b0	prob=0.012195122	name=DEC_fetch_oper_store_ext_reg_mem_mode_1_
mask=0x0ff000d0	sig=0x00a000d0	prob=0.012195122	name=DEC_fetch_oper_store_ext_reg_mem_mode_1_
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_11, 16)
  _STUB_NAME(stub_11_0),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_lsl_adc_),
  _STUB_NAME(stub_11_2),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_lsr_adc_),
  _STUB_NAME(stub_11_4),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_asr_adc_),
  _STUB_NAME(stub_11_6),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_ror_adc_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_adc_),
  _FUNC_NAME(DEC_fetch_oper_mult_long_smlal_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_adc_),
  _FUNC_NAME(DEC_fetch_oper_store_ext_reg_mem_mode_1_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_adc_),
  _FUNC_NAME(DEC_fetch_oper_store_ext_reg_mem_mode_1_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_adc_),
  _FUNC_NAME(DEC_fetch_oper_store_ext_reg_mem_mode_1_),
_TABLE_DEF_END

/*
Table begin: index=11 size=31
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x00b00090
Prob=0.0242863605	Entropy=3.77658966	HTreeHeight=3.85
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x00b000c0	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_adcs_
mask=0x0ff00170	sig=0x00b00140	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_adcs_
mask=0x0ff00270	sig=0x00b00240	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_adcs_
mask=0x0ff00470	sig=0x00b00440	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_adcs_
mask=0x0ff00870	sig=0x00b00840	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_adcs_
mask=0x0ff00870	sig=0x00b00800	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_adcs_
mask=0x0ff00470	sig=0x00b00400	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_adcs_
mask=0x0ff00270	sig=0x00b00200	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_adcs_
mask=0x0ff00170	sig=0x00b00100	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_adcs_
mask=0x0ff000f0	sig=0x00b00080	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_adcs_
mask=0x0ff000f0	sig=0x00b000a0	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_adcs_
mask=0x0ff00170	sig=0x00b00120	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_adcs_
mask=0x0ff00270	sig=0x00b00220	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_adcs_
mask=0x0ff00470	sig=0x00b00420	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_adcs_
mask=0x0ff00870	sig=0x00b00820	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_adcs_
mask=0x0ff00870	sig=0x00b00860	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_adcs_
mask=0x0ff00470	sig=0x00b00460	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_adcs_
mask=0x0ff00270	sig=0x00b00260	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_adcs_
mask=0x0ff00170	sig=0x00b00160	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_adcs_
mask=0x0ff000f0	sig=0x00b000e0	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_adcs_
mask=0x0ff00ff0	sig=0x00b00040	prob=0.075	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_adcs_
mask=0x0ff00ff0	sig=0x00b00000	prob=0.075	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_adcs_
mask=0x0ff00ff0	sig=0x00b00020	prob=0.075	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_adcs_
mask=0x0ff00ff0	sig=0x00b00060	prob=0.075	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_adcs_
mask=0x0ff000f0	sig=0x00b00050	prob=0.075	name=DEC_fetch_oper_dpi_rshift_binop_r_asr_adcs_
mask=0x0ff000f0	sig=0x00b00010	prob=0.075	name=DEC_fetch_oper_dpi_rshift_binop_r_lsl_adcs_
mask=0x0ff000f0	sig=0x00b00030	prob=0.075	name=DEC_fetch_oper_dpi_rshift_binop_r_lsr_adcs_
mask=0x0ff000f0	sig=0x00b00070	prob=0.075	name=DEC_fetch_oper_dpi_rshift_binop_r_ror_adcs_
mask=0x0ff000b0	sig=0x00b000b0	prob=0.0125	name=DEC_fetch_oper_load_ext_reg_mem_mode_1_
mask=0x0ff000d0	sig=0x00b000d0	prob=0.0125	name=DEC_fetch_oper_load_ext_reg_mem_mode_1_
mask=0x0ff000f0	sig=0x00b00090	prob=0.075	name=DEC_fetch_oper_mult_long_smlals_
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_12, 16)
  _STUB_NAME(stub_12_0),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_lsl_adcs_),
  _STUB_NAME(stub_12_2),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_lsr_adcs_),
  _STUB_NAME(stub_12_4),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_asr_adcs_),
  _STUB_NAME(stub_12_6),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_ror_adcs_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_adcs_),
  _FUNC_NAME(DEC_fetch_oper_mult_long_smlals_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_adcs_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_reg_mem_mode_1_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_adcs_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_reg_mem_mode_1_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_adcs_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_reg_mem_mode_1_),
_TABLE_DEF_END

/*
Table begin: index=12 size=31
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x00c000d0
Prob=0.0242863605	Entropy=3.77658966	HTreeHeight=3.85
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x00c000c0	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_sbc_
mask=0x0ff00170	sig=0x00c00140	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_sbc_
mask=0x0ff00270	sig=0x00c00240	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_sbc_
mask=0x0ff00470	sig=0x00c00440	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_sbc_
mask=0x0ff00870	sig=0x00c00840	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_sbc_
mask=0x0ff00870	sig=0x00c00800	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_sbc_
mask=0x0ff00470	sig=0x00c00400	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_sbc_
mask=0x0ff00270	sig=0x00c00200	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_sbc_
mask=0x0ff00170	sig=0x00c00100	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_sbc_
mask=0x0ff000f0	sig=0x00c00080	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_sbc_
mask=0x0ff000f0	sig=0x00c000a0	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_sbc_
mask=0x0ff00170	sig=0x00c00120	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_sbc_
mask=0x0ff00270	sig=0x00c00220	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_sbc_
mask=0x0ff00470	sig=0x00c00420	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_sbc_
mask=0x0ff00870	sig=0x00c00820	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_sbc_
mask=0x0ff00870	sig=0x00c00860	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_sbc_
mask=0x0ff00470	sig=0x00c00460	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_sbc_
mask=0x0ff00270	sig=0x00c00260	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_sbc_
mask=0x0ff00170	sig=0x00c00160	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_sbc_
mask=0x0ff000f0	sig=0x00c000e0	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_sbc_
mask=0x0ff00ff0	sig=0x00c00040	prob=0.075	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_sbc_
mask=0x0ff00ff0	sig=0x00c00000	prob=0.075	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_sbc_
mask=0x0ff00ff0	sig=0x00c00020	prob=0.075	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_sbc_
mask=0x0ff00ff0	sig=0x00c00060	prob=0.075	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_sbc_
mask=0x0ff000f0	sig=0x00c00050	prob=0.075	name=DEC_fetch_oper_dpi_rshift_binop_r_asr_sbc_
mask=0x0ff000f0	sig=0x00c00010	prob=0.075	name=DEC_fetch_oper_dpi_rshift_binop_r_lsl_sbc_
mask=0x0ff000f0	sig=0x00c00030	prob=0.075	name=DEC_fetch_oper_dpi_rshift_binop_r_lsr_sbc_
mask=0x0ff000f0	sig=0x00c00070	prob=0.075	name=DEC_fetch_oper_dpi_rshift_binop_r_ror_sbc_
mask=0x0ff000f0	sig=0x00c00090	prob=0.075	name=DEC_fetch_oper_mult_long_umull_
mask=0x0ff000b0	sig=0x00c000b0	prob=0.0125	name=DEC_fetch_oper_store_ext_imm_mem_mode_2_
mask=0x0ff000d0	sig=0x00c000d0	prob=0.0125	name=DEC_fetch_oper_store_ext_imm_mem_mode_2_
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_13, 16)
  _STUB_NAME(stub_13_0),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_lsl_sbc_),
  _STUB_NAME(stub_13_2),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_lsr_sbc_),
  _STUB_NAME(stub_13_4),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_asr_sbc_),
  _STUB_NAME(stub_13_6),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_ror_sbc_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_sbc_),
  _FUNC_NAME(DEC_fetch_oper_mult_long_umull_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_sbc_),
  _FUNC_NAME(DEC_fetch_oper_store_ext_imm_mem_mode_2_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_sbc_),
  _FUNC_NAME(DEC_fetch_oper_store_ext_imm_mem_mode_2_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_sbc_),
  _FUNC_NAME(DEC_fetch_oper_store_ext_imm_mem_mode_2_),
_TABLE_DEF_END

/*
Table begin: index=13 size=31
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x00d00090
Prob=0.0242863605	Entropy=3.77658966	HTreeHeight=3.85
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x00d000c0	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_sbcs_
mask=0x0ff00170	sig=0x00d00140	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_sbcs_
mask=0x0ff00270	sig=0x00d00240	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_sbcs_
mask=0x0ff00470	sig=0x00d00440	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_sbcs_
mask=0x0ff00870	sig=0x00d00840	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_sbcs_
mask=0x0ff00870	sig=0x00d00800	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_sbcs_
mask=0x0ff00470	sig=0x00d00400	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_sbcs_
mask=0x0ff00270	sig=0x00d00200	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_sbcs_
mask=0x0ff00170	sig=0x00d00100	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_sbcs_
mask=0x0ff000f0	sig=0x00d00080	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_sbcs_
mask=0x0ff000f0	sig=0x00d000a0	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_sbcs_
mask=0x0ff00170	sig=0x00d00120	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_sbcs_
mask=0x0ff00270	sig=0x00d00220	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_sbcs_
mask=0x0ff00470	sig=0x00d00420	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_sbcs_
mask=0x0ff00870	sig=0x00d00820	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_sbcs_
mask=0x0ff00870	sig=0x00d00860	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_sbcs_
mask=0x0ff00470	sig=0x00d00460	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_sbcs_
mask=0x0ff00270	sig=0x00d00260	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_sbcs_
mask=0x0ff00170	sig=0x00d00160	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_sbcs_
mask=0x0ff000f0	sig=0x00d000e0	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_sbcs_
mask=0x0ff00ff0	sig=0x00d00040	prob=0.075	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_sbcs_
mask=0x0ff00ff0	sig=0x00d00000	prob=0.075	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_sbcs_
mask=0x0ff00ff0	sig=0x00d00020	prob=0.075	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_sbcs_
mask=0x0ff00ff0	sig=0x00d00060	prob=0.075	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_sbcs_
mask=0x0ff000f0	sig=0x00d00050	prob=0.075	name=DEC_fetch_oper_dpi_rshift_binop_r_asr_sbcs_
mask=0x0ff000f0	sig=0x00d00010	prob=0.075	name=DEC_fetch_oper_dpi_rshift_binop_r_lsl_sbcs_
mask=0x0ff000f0	sig=0x00d00030	prob=0.075	name=DEC_fetch_oper_dpi_rshift_binop_r_lsr_sbcs_
mask=0x0ff000f0	sig=0x00d00070	prob=0.075	name=DEC_fetch_oper_dpi_rshift_binop_r_ror_sbcs_
mask=0x0ff000b0	sig=0x00d000b0	prob=0.0125	name=DEC_fetch_oper_load_ext_imm_mem_mode_2_
mask=0x0ff000d0	sig=0x00d000d0	prob=0.0125	name=DEC_fetch_oper_load_ext_imm_mem_mode_2_
mask=0x0ff000f0	sig=0x00d00090	prob=0.075	name=DEC_fetch_oper_mult_long_umulls_
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_14, 16)
  _STUB_NAME(stub_14_0),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_lsl_sbcs_),
  _STUB_NAME(stub_14_2),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_lsr_sbcs_),
  _STUB_NAME(stub_14_4),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_asr_sbcs_),
  _STUB_NAME(stub_14_6),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_ror_sbcs_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_sbcs_),
  _FUNC_NAME(DEC_fetch_oper_mult_long_umulls_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_sbcs_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_imm_mem_mode_2_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_sbcs_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_imm_mem_mode_2_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_sbcs_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_imm_mem_mode_2_),
_TABLE_DEF_END

/*
Table begin: index=14 size=31
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x00e000d0
Prob=0.0242863605	Entropy=3.77658966	HTreeHeight=3.85
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x00e000c0	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_rsc_
mask=0x0ff00170	sig=0x00e00140	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_rsc_
mask=0x0ff00270	sig=0x00e00240	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_rsc_
mask=0x0ff00470	sig=0x00e00440	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_rsc_
mask=0x0ff00870	sig=0x00e00840	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_rsc_
mask=0x0ff00870	sig=0x00e00800	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_rsc_
mask=0x0ff00470	sig=0x00e00400	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_rsc_
mask=0x0ff00270	sig=0x00e00200	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_rsc_
mask=0x0ff00170	sig=0x00e00100	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_rsc_
mask=0x0ff000f0	sig=0x00e00080	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_rsc_
mask=0x0ff000f0	sig=0x00e000a0	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_rsc_
mask=0x0ff00170	sig=0x00e00120	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_rsc_
mask=0x0ff00270	sig=0x00e00220	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_rsc_
mask=0x0ff00470	sig=0x00e00420	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_rsc_
mask=0x0ff00870	sig=0x00e00820	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_rsc_
mask=0x0ff00870	sig=0x00e00860	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_rsc_
mask=0x0ff00470	sig=0x00e00460	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_rsc_
mask=0x0ff00270	sig=0x00e00260	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_rsc_
mask=0x0ff00170	sig=0x00e00160	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_rsc_
mask=0x0ff000f0	sig=0x00e000e0	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_rsc_
mask=0x0ff00ff0	sig=0x00e00040	prob=0.075	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_rsc_
mask=0x0ff00ff0	sig=0x00e00000	prob=0.075	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_rsc_
mask=0x0ff00ff0	sig=0x00e00020	prob=0.075	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_rsc_
mask=0x0ff00ff0	sig=0x00e00060	prob=0.075	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_rsc_
mask=0x0ff000f0	sig=0x00e00050	prob=0.075	name=DEC_fetch_oper_dpi_rshift_binop_r_asr_rsc_
mask=0x0ff000f0	sig=0x00e00010	prob=0.075	name=DEC_fetch_oper_dpi_rshift_binop_r_lsl_rsc_
mask=0x0ff000f0	sig=0x00e00030	prob=0.075	name=DEC_fetch_oper_dpi_rshift_binop_r_lsr_rsc_
mask=0x0ff000f0	sig=0x00e00070	prob=0.075	name=DEC_fetch_oper_dpi_rshift_binop_r_ror_rsc_
mask=0x0ff000f0	sig=0x00e00090	prob=0.075	name=DEC_fetch_oper_mult_long_umlal_
mask=0x0ff000b0	sig=0x00e000b0	prob=0.0125	name=DEC_fetch_oper_store_ext_imm_mem_mode_3_
mask=0x0ff000d0	sig=0x00e000d0	prob=0.0125	name=DEC_fetch_oper_store_ext_imm_mem_mode_3_
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_15, 16)
  _STUB_NAME(stub_15_0),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_lsl_rsc_),
  _STUB_NAME(stub_15_2),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_lsr_rsc_),
  _STUB_NAME(stub_15_4),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_asr_rsc_),
  _STUB_NAME(stub_15_6),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_ror_rsc_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_rsc_),
  _FUNC_NAME(DEC_fetch_oper_mult_long_umlal_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_rsc_),
  _FUNC_NAME(DEC_fetch_oper_store_ext_imm_mem_mode_3_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_rsc_),
  _FUNC_NAME(DEC_fetch_oper_store_ext_imm_mem_mode_3_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_rsc_),
  _FUNC_NAME(DEC_fetch_oper_store_ext_imm_mem_mode_3_),
_TABLE_DEF_END

/*
Table begin: index=15 size=31
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x00f00090
Prob=0.0242863605	Entropy=3.77658966	HTreeHeight=3.85
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x00f000c0	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_rscs_
mask=0x0ff00170	sig=0x00f00140	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_rscs_
mask=0x0ff00270	sig=0x00f00240	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_rscs_
mask=0x0ff00470	sig=0x00f00440	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_rscs_
mask=0x0ff00870	sig=0x00f00840	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_rscs_
mask=0x0ff00870	sig=0x00f00800	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_rscs_
mask=0x0ff00470	sig=0x00f00400	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_rscs_
mask=0x0ff00270	sig=0x00f00200	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_rscs_
mask=0x0ff00170	sig=0x00f00100	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_rscs_
mask=0x0ff000f0	sig=0x00f00080	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_rscs_
mask=0x0ff000f0	sig=0x00f000a0	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_rscs_
mask=0x0ff00170	sig=0x00f00120	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_rscs_
mask=0x0ff00270	sig=0x00f00220	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_rscs_
mask=0x0ff00470	sig=0x00f00420	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_rscs_
mask=0x0ff00870	sig=0x00f00820	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_rscs_
mask=0x0ff00870	sig=0x00f00860	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_rscs_
mask=0x0ff00470	sig=0x00f00460	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_rscs_
mask=0x0ff00270	sig=0x00f00260	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_rscs_
mask=0x0ff00170	sig=0x00f00160	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_rscs_
mask=0x0ff000f0	sig=0x00f000e0	prob=0.015	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_rscs_
mask=0x0ff00ff0	sig=0x00f00040	prob=0.075	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_rscs_
mask=0x0ff00ff0	sig=0x00f00000	prob=0.075	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_rscs_
mask=0x0ff00ff0	sig=0x00f00020	prob=0.075	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_rscs_
mask=0x0ff00ff0	sig=0x00f00060	prob=0.075	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_rscs_
mask=0x0ff000f0	sig=0x00f00050	prob=0.075	name=DEC_fetch_oper_dpi_rshift_binop_r_asr_rscs_
mask=0x0ff000f0	sig=0x00f00010	prob=0.075	name=DEC_fetch_oper_dpi_rshift_binop_r_lsl_rscs_
mask=0x0ff000f0	sig=0x00f00030	prob=0.075	name=DEC_fetch_oper_dpi_rshift_binop_r_lsr_rscs_
mask=0x0ff000f0	sig=0x00f00070	prob=0.075	name=DEC_fetch_oper_dpi_rshift_binop_r_ror_rscs_
mask=0x0ff000b0	sig=0x00f000b0	prob=0.0125	name=DEC_fetch_oper_load_ext_imm_mem_mode_3_
mask=0x0ff000d0	sig=0x00f000d0	prob=0.0125	name=DEC_fetch_oper_load_ext_imm_mem_mode_3_
mask=0x0ff000f0	sig=0x00f00090	prob=0.075	name=DEC_fetch_oper_mult_long_umlals_
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_16, 16)
  _STUB_NAME(stub_16_0),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_lsl_rscs_),
  _STUB_NAME(stub_16_2),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_lsr_rscs_),
  _STUB_NAME(stub_16_4),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_asr_rscs_),
  _STUB_NAME(stub_16_6),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_ror_rscs_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_rscs_),
  _FUNC_NAME(DEC_fetch_oper_mult_long_umlals_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_rscs_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_imm_mem_mode_3_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_rscs_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_imm_mem_mode_3_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_rscs_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_imm_mem_mode_3_),
_TABLE_DEF_END

/*
Table begin: index=17 size=29
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x01100090
Prob=0.0227684629	Entropy=3.68385619	HTreeHeight=3.76
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x011000c0	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_asr_tst_
mask=0x0ff00170	sig=0x01100140	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_asr_tst_
mask=0x0ff00270	sig=0x01100240	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_asr_tst_
mask=0x0ff00470	sig=0x01100440	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_asr_tst_
mask=0x0ff00870	sig=0x01100840	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_asr_tst_
mask=0x0ff000f0	sig=0x01100080	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsl_tst_
mask=0x0ff00170	sig=0x01100100	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsl_tst_
mask=0x0ff00270	sig=0x01100200	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsl_tst_
mask=0x0ff00470	sig=0x01100400	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsl_tst_
mask=0x0ff00870	sig=0x01100800	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsl_tst_
mask=0x0ff00870	sig=0x01100820	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsr_tst_
mask=0x0ff00470	sig=0x01100420	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsr_tst_
mask=0x0ff00270	sig=0x01100220	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsr_tst_
mask=0x0ff00170	sig=0x01100120	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsr_tst_
mask=0x0ff000f0	sig=0x011000a0	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsr_tst_
mask=0x0ff000f0	sig=0x011000e0	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_ror_tst_
mask=0x0ff00170	sig=0x01100160	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_ror_tst_
mask=0x0ff00270	sig=0x01100260	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_ror_tst_
mask=0x0ff00470	sig=0x01100460	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_ror_tst_
mask=0x0ff00870	sig=0x01100860	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_ror_tst_
mask=0x0ff00ff0	sig=0x01100040	prob=0.08	name=DEC_fetch_oper_dpi_ishift_tst_zero_shift_zero_asr_tst_
mask=0x0ff00ff0	sig=0x01100000	prob=0.08	name=DEC_fetch_oper_dpi_ishift_tst_zero_shift_zero_lsl_tst_
mask=0x0ff00ff0	sig=0x01100020	prob=0.08	name=DEC_fetch_oper_dpi_ishift_tst_zero_shift_zero_lsr_tst_
mask=0x0ff00ff0	sig=0x01100060	prob=0.08	name=DEC_fetch_oper_dpi_ishift_tst_zero_shift_zero_ror_tst_
mask=0x0ff000f0	sig=0x01100050	prob=0.08	name=DEC_fetch_oper_dpi_rshift_tst_r_asr_tst_
mask=0x0ff000f0	sig=0x01100010	prob=0.08	name=DEC_fetch_oper_dpi_rshift_tst_r_lsl_tst_
mask=0x0ff000f0	sig=0x01100030	prob=0.08	name=DEC_fetch_oper_dpi_rshift_tst_r_lsr_tst_
mask=0x0ff000f0	sig=0x01100070	prob=0.08	name=DEC_fetch_oper_dpi_rshift_tst_r_ror_tst_
mask=0x0ff00090	sig=0x01100090	prob=0.04	name=DEC_fetch_oper_load_ext_reg_mem_mode_4_
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_17, 16)
  _STUB_NAME(stub_17_0),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_tst_r_lsl_tst_),
  _STUB_NAME(stub_17_2),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_tst_r_lsr_tst_),
  _STUB_NAME(stub_17_4),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_tst_r_asr_tst_),
  _STUB_NAME(stub_17_6),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_tst_r_ror_tst_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsl_tst_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_reg_mem_mode_4_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsr_tst_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_reg_mem_mode_4_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_asr_tst_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_reg_mem_mode_4_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_ror_tst_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_reg_mem_mode_4_),
_TABLE_DEF_END

/*
Table begin: index=19 size=29
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x01300090
Prob=0.0227684629	Entropy=3.68385619	HTreeHeight=3.76
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x013000c0	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_asr_teq_
mask=0x0ff00170	sig=0x01300140	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_asr_teq_
mask=0x0ff00270	sig=0x01300240	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_asr_teq_
mask=0x0ff00470	sig=0x01300440	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_asr_teq_
mask=0x0ff00870	sig=0x01300840	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_asr_teq_
mask=0x0ff000f0	sig=0x01300080	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsl_teq_
mask=0x0ff00170	sig=0x01300100	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsl_teq_
mask=0x0ff00270	sig=0x01300200	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsl_teq_
mask=0x0ff00470	sig=0x01300400	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsl_teq_
mask=0x0ff00870	sig=0x01300800	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsl_teq_
mask=0x0ff00870	sig=0x01300820	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsr_teq_
mask=0x0ff00470	sig=0x01300420	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsr_teq_
mask=0x0ff00270	sig=0x01300220	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsr_teq_
mask=0x0ff00170	sig=0x01300120	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsr_teq_
mask=0x0ff000f0	sig=0x013000a0	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsr_teq_
mask=0x0ff000f0	sig=0x013000e0	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_ror_teq_
mask=0x0ff00170	sig=0x01300160	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_ror_teq_
mask=0x0ff00270	sig=0x01300260	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_ror_teq_
mask=0x0ff00470	sig=0x01300460	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_ror_teq_
mask=0x0ff00870	sig=0x01300860	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_ror_teq_
mask=0x0ff00ff0	sig=0x01300040	prob=0.08	name=DEC_fetch_oper_dpi_ishift_tst_zero_shift_zero_asr_teq_
mask=0x0ff00ff0	sig=0x01300000	prob=0.08	name=DEC_fetch_oper_dpi_ishift_tst_zero_shift_zero_lsl_teq_
mask=0x0ff00ff0	sig=0x01300020	prob=0.08	name=DEC_fetch_oper_dpi_ishift_tst_zero_shift_zero_lsr_teq_
mask=0x0ff00ff0	sig=0x01300060	prob=0.08	name=DEC_fetch_oper_dpi_ishift_tst_zero_shift_zero_ror_teq_
mask=0x0ff000f0	sig=0x01300050	prob=0.08	name=DEC_fetch_oper_dpi_rshift_tst_r_asr_teq_
mask=0x0ff000f0	sig=0x01300010	prob=0.08	name=DEC_fetch_oper_dpi_rshift_tst_r_lsl_teq_
mask=0x0ff000f0	sig=0x01300030	prob=0.08	name=DEC_fetch_oper_dpi_rshift_tst_r_lsr_teq_
mask=0x0ff000f0	sig=0x01300070	prob=0.08	name=DEC_fetch_oper_dpi_rshift_tst_r_ror_teq_
mask=0x0ff00090	sig=0x01300090	prob=0.04	name=DEC_fetch_oper_load_ext_reg_mem_mode_5_
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_18, 16)
  _STUB_NAME(stub_18_0),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_tst_r_lsl_teq_),
  _STUB_NAME(stub_18_2),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_tst_r_lsr_teq_),
  _STUB_NAME(stub_18_4),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_tst_r_asr_teq_),
  _STUB_NAME(stub_18_6),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_tst_r_ror_teq_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsl_teq_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_reg_mem_mode_5_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsr_teq_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_reg_mem_mode_5_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_asr_teq_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_reg_mem_mode_5_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_ror_teq_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_reg_mem_mode_5_),
_TABLE_DEF_END

/*
Table begin: index=21 size=29
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x01500090
Prob=0.0227684629	Entropy=3.68385619	HTreeHeight=3.76
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x015000c0	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_asr_cmp_
mask=0x0ff00170	sig=0x01500140	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_asr_cmp_
mask=0x0ff00270	sig=0x01500240	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_asr_cmp_
mask=0x0ff00470	sig=0x01500440	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_asr_cmp_
mask=0x0ff00870	sig=0x01500840	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_asr_cmp_
mask=0x0ff000f0	sig=0x01500080	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsl_cmp_
mask=0x0ff00170	sig=0x01500100	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsl_cmp_
mask=0x0ff00270	sig=0x01500200	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsl_cmp_
mask=0x0ff00470	sig=0x01500400	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsl_cmp_
mask=0x0ff00870	sig=0x01500800	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsl_cmp_
mask=0x0ff00870	sig=0x01500820	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsr_cmp_
mask=0x0ff00470	sig=0x01500420	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsr_cmp_
mask=0x0ff00270	sig=0x01500220	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsr_cmp_
mask=0x0ff00170	sig=0x01500120	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsr_cmp_
mask=0x0ff000f0	sig=0x015000a0	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsr_cmp_
mask=0x0ff000f0	sig=0x015000e0	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_ror_cmp_
mask=0x0ff00170	sig=0x01500160	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_ror_cmp_
mask=0x0ff00270	sig=0x01500260	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_ror_cmp_
mask=0x0ff00470	sig=0x01500460	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_ror_cmp_
mask=0x0ff00870	sig=0x01500860	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_ror_cmp_
mask=0x0ff00ff0	sig=0x01500040	prob=0.08	name=DEC_fetch_oper_dpi_ishift_tst_zero_shift_zero_asr_cmp_
mask=0x0ff00ff0	sig=0x01500000	prob=0.08	name=DEC_fetch_oper_dpi_ishift_tst_zero_shift_zero_lsl_cmp_
mask=0x0ff00ff0	sig=0x01500020	prob=0.08	name=DEC_fetch_oper_dpi_ishift_tst_zero_shift_zero_lsr_cmp_
mask=0x0ff00ff0	sig=0x01500060	prob=0.08	name=DEC_fetch_oper_dpi_ishift_tst_zero_shift_zero_ror_cmp_
mask=0x0ff000f0	sig=0x01500050	prob=0.08	name=DEC_fetch_oper_dpi_rshift_tst_r_asr_cmp_
mask=0x0ff000f0	sig=0x01500010	prob=0.08	name=DEC_fetch_oper_dpi_rshift_tst_r_lsl_cmp_
mask=0x0ff000f0	sig=0x01500030	prob=0.08	name=DEC_fetch_oper_dpi_rshift_tst_r_lsr_cmp_
mask=0x0ff000f0	sig=0x01500070	prob=0.08	name=DEC_fetch_oper_dpi_rshift_tst_r_ror_cmp_
mask=0x0ff00090	sig=0x01500090	prob=0.04	name=DEC_fetch_oper_load_ext_imm_mem_mode_6_
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_19, 16)
  _STUB_NAME(stub_19_0),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_tst_r_lsl_cmp_),
  _STUB_NAME(stub_19_2),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_tst_r_lsr_cmp_),
  _STUB_NAME(stub_19_4),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_tst_r_asr_cmp_),
  _STUB_NAME(stub_19_6),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_tst_r_ror_cmp_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsl_cmp_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_imm_mem_mode_6_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsr_cmp_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_imm_mem_mode_6_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_asr_cmp_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_imm_mem_mode_6_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_ror_cmp_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_imm_mem_mode_6_),
_TABLE_DEF_END

/*
Table begin: index=23 size=29
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x01700090
Prob=0.0227684629	Entropy=3.68385619	HTreeHeight=3.76
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x017000c0	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_asr_cmn_
mask=0x0ff00170	sig=0x01700140	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_asr_cmn_
mask=0x0ff00270	sig=0x01700240	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_asr_cmn_
mask=0x0ff00470	sig=0x01700440	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_asr_cmn_
mask=0x0ff00870	sig=0x01700840	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_asr_cmn_
mask=0x0ff000f0	sig=0x01700080	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsl_cmn_
mask=0x0ff00170	sig=0x01700100	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsl_cmn_
mask=0x0ff00270	sig=0x01700200	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsl_cmn_
mask=0x0ff00470	sig=0x01700400	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsl_cmn_
mask=0x0ff00870	sig=0x01700800	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsl_cmn_
mask=0x0ff00870	sig=0x01700820	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsr_cmn_
mask=0x0ff00470	sig=0x01700420	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsr_cmn_
mask=0x0ff00270	sig=0x01700220	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsr_cmn_
mask=0x0ff00170	sig=0x01700120	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsr_cmn_
mask=0x0ff000f0	sig=0x017000a0	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsr_cmn_
mask=0x0ff000f0	sig=0x017000e0	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_ror_cmn_
mask=0x0ff00170	sig=0x01700160	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_ror_cmn_
mask=0x0ff00270	sig=0x01700260	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_ror_cmn_
mask=0x0ff00470	sig=0x01700460	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_ror_cmn_
mask=0x0ff00870	sig=0x01700860	prob=0.016	name=DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_ror_cmn_
mask=0x0ff00ff0	sig=0x01700040	prob=0.08	name=DEC_fetch_oper_dpi_ishift_tst_zero_shift_zero_asr_cmn_
mask=0x0ff00ff0	sig=0x01700000	prob=0.08	name=DEC_fetch_oper_dpi_ishift_tst_zero_shift_zero_lsl_cmn_
mask=0x0ff00ff0	sig=0x01700020	prob=0.08	name=DEC_fetch_oper_dpi_ishift_tst_zero_shift_zero_lsr_cmn_
mask=0x0ff00ff0	sig=0x01700060	prob=0.08	name=DEC_fetch_oper_dpi_ishift_tst_zero_shift_zero_ror_cmn_
mask=0x0ff000f0	sig=0x01700050	prob=0.08	name=DEC_fetch_oper_dpi_rshift_tst_r_asr_cmn_
mask=0x0ff000f0	sig=0x01700010	prob=0.08	name=DEC_fetch_oper_dpi_rshift_tst_r_lsl_cmn_
mask=0x0ff000f0	sig=0x01700030	prob=0.08	name=DEC_fetch_oper_dpi_rshift_tst_r_lsr_cmn_
mask=0x0ff000f0	sig=0x01700070	prob=0.08	name=DEC_fetch_oper_dpi_rshift_tst_r_ror_cmn_
mask=0x0ff00090	sig=0x01700090	prob=0.04	name=DEC_fetch_oper_load_ext_imm_mem_mode_7_
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_20, 16)
  _STUB_NAME(stub_20_0),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_tst_r_lsl_cmn_),
  _STUB_NAME(stub_20_2),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_tst_r_lsr_cmn_),
  _STUB_NAME(stub_20_4),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_tst_r_asr_cmn_),
  _STUB_NAME(stub_20_6),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_tst_r_ror_cmn_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsl_cmn_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_imm_mem_mode_7_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsr_cmn_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_imm_mem_mode_7_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_asr_cmn_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_imm_mem_mode_7_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_ror_cmn_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_imm_mem_mode_7_),
_TABLE_DEF_END

/*
Table begin: index=24 size=35
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x01800090
Prob=0.0228985684	Entropy=3.68877465	HTreeHeight=3.76136364
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x018000c0	prob=0.0159090909	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_orr_
mask=0x0ff00170	sig=0x01800140	prob=0.0159090909	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_orr_
mask=0x0ff00270	sig=0x01800240	prob=0.0159090909	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_orr_
mask=0x0ff00470	sig=0x01800440	prob=0.0159090909	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_orr_
mask=0x0ff00870	sig=0x01800840	prob=0.0159090909	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_orr_
mask=0x0ff00870	sig=0x01800800	prob=0.0159090909	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_orr_
mask=0x0ff00470	sig=0x01800400	prob=0.0159090909	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_orr_
mask=0x0ff00270	sig=0x01800200	prob=0.0159090909	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_orr_
mask=0x0ff00170	sig=0x01800100	prob=0.0159090909	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_orr_
mask=0x0ff000f0	sig=0x01800080	prob=0.0159090909	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_orr_
mask=0x0ff000f0	sig=0x018000a0	prob=0.0159090909	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_orr_
mask=0x0ff00170	sig=0x01800120	prob=0.0159090909	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_orr_
mask=0x0ff00270	sig=0x01800220	prob=0.0159090909	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_orr_
mask=0x0ff00470	sig=0x01800420	prob=0.0159090909	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_orr_
mask=0x0ff00870	sig=0x01800820	prob=0.0159090909	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_orr_
mask=0x0ff00870	sig=0x01800860	prob=0.0159090909	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_orr_
mask=0x0ff00470	sig=0x01800460	prob=0.0159090909	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_orr_
mask=0x0ff00270	sig=0x01800260	prob=0.0159090909	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_orr_
mask=0x0ff00170	sig=0x01800160	prob=0.0159090909	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_orr_
mask=0x0ff000f0	sig=0x018000e0	prob=0.0159090909	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_orr_
mask=0x0ff00ff0	sig=0x01800040	prob=0.0795454545	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_orr_
mask=0x0ff00ff0	sig=0x01800000	prob=0.0795454545	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_orr_
mask=0x0ff00ff0	sig=0x01800020	prob=0.0795454545	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_orr_
mask=0x0ff00ff0	sig=0x01800060	prob=0.0795454545	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_orr_
mask=0x0ff000f0	sig=0x01800050	prob=0.0795454545	name=DEC_fetch_oper_dpi_rshift_binop_r_asr_orr_
mask=0x0ff000f0	sig=0x01800010	prob=0.0795454545	name=DEC_fetch_oper_dpi_rshift_binop_r_lsl_orr_
mask=0x0ff000f0	sig=0x01800030	prob=0.0795454545	name=DEC_fetch_oper_dpi_rshift_binop_r_lsr_orr_
mask=0x0ff000f0	sig=0x01800070	prob=0.0795454545	name=DEC_fetch_oper_dpi_rshift_binop_r_ror_orr_
mask=0x0ff000b0	sig=0x018000b0	prob=0.00568181818	name=DEC_fetch_oper_store_ext_reg_mem_mode_4_
mask=0x0ff000d0	sig=0x018000d0	prob=0.00568181818	name=DEC_fetch_oper_store_ext_reg_mem_mode_4_
mask=0x0ff00190	sig=0x01800190	prob=0.00568181818	name=DEC_fetch_oper_store_ext_reg_mem_mode_4_
mask=0x0ff00290	sig=0x01800290	prob=0.00568181818	name=DEC_fetch_oper_store_ext_reg_mem_mode_4_
mask=0x0ff00490	sig=0x01800490	prob=0.00568181818	name=DEC_fetch_oper_store_ext_reg_mem_mode_4_
mask=0x0ff00890	sig=0x01800890	prob=0.00568181818	name=DEC_fetch_oper_store_ext_reg_mem_mode_4_
mask=0x0ff00090	sig=0x01800090	prob=0.0113636364	name=DEC_fetch_oper_store_ext_reg_mem_mode_4_
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_21, 16)
  _STUB_NAME(stub_21_0),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_lsl_orr_),
  _STUB_NAME(stub_21_2),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_lsr_orr_),
  _STUB_NAME(stub_21_4),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_asr_orr_),
  _STUB_NAME(stub_21_6),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_ror_orr_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_orr_),
  _FUNC_NAME(DEC_fetch_oper_store_ext_reg_mem_mode_4_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_orr_),
  _FUNC_NAME(DEC_fetch_oper_store_ext_reg_mem_mode_4_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_orr_),
  _FUNC_NAME(DEC_fetch_oper_store_ext_reg_mem_mode_4_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_orr_),
  _FUNC_NAME(DEC_fetch_oper_store_ext_reg_mem_mode_4_),
_TABLE_DEF_END

/*
Table begin: index=25 size=29
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x01900090
Prob=0.0227684629	Entropy=3.68385619	HTreeHeight=3.76
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x019000c0	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_orrs_
mask=0x0ff00170	sig=0x01900140	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_orrs_
mask=0x0ff00270	sig=0x01900240	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_orrs_
mask=0x0ff00470	sig=0x01900440	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_orrs_
mask=0x0ff00870	sig=0x01900840	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_orrs_
mask=0x0ff000f0	sig=0x01900080	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_orrs_
mask=0x0ff00170	sig=0x01900100	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_orrs_
mask=0x0ff00270	sig=0x01900200	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_orrs_
mask=0x0ff00470	sig=0x01900400	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_orrs_
mask=0x0ff00870	sig=0x01900800	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_orrs_
mask=0x0ff00870	sig=0x01900820	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_orrs_
mask=0x0ff00470	sig=0x01900420	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_orrs_
mask=0x0ff00270	sig=0x01900220	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_orrs_
mask=0x0ff00170	sig=0x01900120	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_orrs_
mask=0x0ff000f0	sig=0x019000a0	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_orrs_
mask=0x0ff000f0	sig=0x019000e0	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_orrs_
mask=0x0ff00170	sig=0x01900160	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_orrs_
mask=0x0ff00270	sig=0x01900260	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_orrs_
mask=0x0ff00470	sig=0x01900460	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_orrs_
mask=0x0ff00870	sig=0x01900860	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_orrs_
mask=0x0ff00ff0	sig=0x01900040	prob=0.08	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_orrs_
mask=0x0ff00ff0	sig=0x01900000	prob=0.08	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_orrs_
mask=0x0ff00ff0	sig=0x01900020	prob=0.08	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_orrs_
mask=0x0ff00ff0	sig=0x01900060	prob=0.08	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_orrs_
mask=0x0ff000f0	sig=0x01900050	prob=0.08	name=DEC_fetch_oper_dpi_rshift_binop_r_asr_orrs_
mask=0x0ff000f0	sig=0x01900010	prob=0.08	name=DEC_fetch_oper_dpi_rshift_binop_r_lsl_orrs_
mask=0x0ff000f0	sig=0x01900030	prob=0.08	name=DEC_fetch_oper_dpi_rshift_binop_r_lsr_orrs_
mask=0x0ff000f0	sig=0x01900070	prob=0.08	name=DEC_fetch_oper_dpi_rshift_binop_r_ror_orrs_
mask=0x0ff00090	sig=0x01900090	prob=0.04	name=DEC_fetch_oper_load_ext_reg_mem_mode_4_
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_22, 16)
  _STUB_NAME(stub_22_0),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_lsl_orrs_),
  _STUB_NAME(stub_22_2),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_lsr_orrs_),
  _STUB_NAME(stub_22_4),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_asr_orrs_),
  _STUB_NAME(stub_22_6),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_ror_orrs_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_orrs_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_reg_mem_mode_4_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_orrs_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_reg_mem_mode_4_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_orrs_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_reg_mem_mode_4_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_orrs_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_reg_mem_mode_4_),
_TABLE_DEF_END

/*
Table begin: index=26 size=29
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x01a00090
Prob=0.0227684629	Entropy=3.68385619	HTreeHeight=3.76
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x01a000c0	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_asr_mov_
mask=0x0ff00170	sig=0x01a00140	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_asr_mov_
mask=0x0ff00270	sig=0x01a00240	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_asr_mov_
mask=0x0ff00470	sig=0x01a00440	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_asr_mov_
mask=0x0ff00870	sig=0x01a00840	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_asr_mov_
mask=0x0ff000f0	sig=0x01a00080	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsl_mov_
mask=0x0ff00170	sig=0x01a00100	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsl_mov_
mask=0x0ff00270	sig=0x01a00200	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsl_mov_
mask=0x0ff00470	sig=0x01a00400	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsl_mov_
mask=0x0ff00870	sig=0x01a00800	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsl_mov_
mask=0x0ff00870	sig=0x01a00820	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsr_mov_
mask=0x0ff00470	sig=0x01a00420	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsr_mov_
mask=0x0ff00270	sig=0x01a00220	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsr_mov_
mask=0x0ff00170	sig=0x01a00120	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsr_mov_
mask=0x0ff000f0	sig=0x01a000a0	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsr_mov_
mask=0x0ff000f0	sig=0x01a000e0	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_ror_mov_
mask=0x0ff00170	sig=0x01a00160	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_ror_mov_
mask=0x0ff00270	sig=0x01a00260	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_ror_mov_
mask=0x0ff00470	sig=0x01a00460	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_ror_mov_
mask=0x0ff00870	sig=0x01a00860	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_ror_mov_
mask=0x0ff00ff0	sig=0x01a00040	prob=0.08	name=DEC_fetch_oper_dpi_ishift_unop_zero_shift_zero_asr_mov_
mask=0x0ff00ff0	sig=0x01a00000	prob=0.08	name=DEC_fetch_oper_dpi_ishift_unop_zero_shift_zero_lsl_mov_
mask=0x0ff00ff0	sig=0x01a00020	prob=0.08	name=DEC_fetch_oper_dpi_ishift_unop_zero_shift_zero_lsr_mov_
mask=0x0ff00ff0	sig=0x01a00060	prob=0.08	name=DEC_fetch_oper_dpi_ishift_unop_zero_shift_zero_ror_mov_
mask=0x0ff000f0	sig=0x01a00050	prob=0.08	name=DEC_fetch_oper_dpi_rshift_unop_r_asr_mov_
mask=0x0ff000f0	sig=0x01a00010	prob=0.08	name=DEC_fetch_oper_dpi_rshift_unop_r_lsl_mov_
mask=0x0ff000f0	sig=0x01a00030	prob=0.08	name=DEC_fetch_oper_dpi_rshift_unop_r_lsr_mov_
mask=0x0ff000f0	sig=0x01a00070	prob=0.08	name=DEC_fetch_oper_dpi_rshift_unop_r_ror_mov_
mask=0x0ff00090	sig=0x01a00090	prob=0.04	name=DEC_fetch_oper_store_ext_reg_mem_mode_5_
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_23, 16)
  _STUB_NAME(stub_23_0),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_unop_r_lsl_mov_),
  _STUB_NAME(stub_23_2),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_unop_r_lsr_mov_),
  _STUB_NAME(stub_23_4),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_unop_r_asr_mov_),
  _STUB_NAME(stub_23_6),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_unop_r_ror_mov_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsl_mov_),
  _FUNC_NAME(DEC_fetch_oper_store_ext_reg_mem_mode_5_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsr_mov_),
  _FUNC_NAME(DEC_fetch_oper_store_ext_reg_mem_mode_5_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_asr_mov_),
  _FUNC_NAME(DEC_fetch_oper_store_ext_reg_mem_mode_5_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_ror_mov_),
  _FUNC_NAME(DEC_fetch_oper_store_ext_reg_mem_mode_5_),
_TABLE_DEF_END

/*
Table begin: index=27 size=29
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x01b00090
Prob=0.0227684629	Entropy=3.68385619	HTreeHeight=3.76
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x01b000c0	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_asr_movs_
mask=0x0ff00170	sig=0x01b00140	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_asr_movs_
mask=0x0ff00270	sig=0x01b00240	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_asr_movs_
mask=0x0ff00470	sig=0x01b00440	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_asr_movs_
mask=0x0ff00870	sig=0x01b00840	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_asr_movs_
mask=0x0ff000f0	sig=0x01b00080	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsl_movs_
mask=0x0ff00170	sig=0x01b00100	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsl_movs_
mask=0x0ff00270	sig=0x01b00200	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsl_movs_
mask=0x0ff00470	sig=0x01b00400	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsl_movs_
mask=0x0ff00870	sig=0x01b00800	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsl_movs_
mask=0x0ff00870	sig=0x01b00820	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsr_movs_
mask=0x0ff00470	sig=0x01b00420	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsr_movs_
mask=0x0ff00270	sig=0x01b00220	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsr_movs_
mask=0x0ff00170	sig=0x01b00120	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsr_movs_
mask=0x0ff000f0	sig=0x01b000a0	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsr_movs_
mask=0x0ff000f0	sig=0x01b000e0	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_ror_movs_
mask=0x0ff00170	sig=0x01b00160	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_ror_movs_
mask=0x0ff00270	sig=0x01b00260	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_ror_movs_
mask=0x0ff00470	sig=0x01b00460	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_ror_movs_
mask=0x0ff00870	sig=0x01b00860	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_ror_movs_
mask=0x0ff00ff0	sig=0x01b00040	prob=0.08	name=DEC_fetch_oper_dpi_ishift_unop_zero_shift_zero_asr_movs_
mask=0x0ff00ff0	sig=0x01b00000	prob=0.08	name=DEC_fetch_oper_dpi_ishift_unop_zero_shift_zero_lsl_movs_
mask=0x0ff00ff0	sig=0x01b00020	prob=0.08	name=DEC_fetch_oper_dpi_ishift_unop_zero_shift_zero_lsr_movs_
mask=0x0ff00ff0	sig=0x01b00060	prob=0.08	name=DEC_fetch_oper_dpi_ishift_unop_zero_shift_zero_ror_movs_
mask=0x0ff000f0	sig=0x01b00050	prob=0.08	name=DEC_fetch_oper_dpi_rshift_unop_r_asr_movs_
mask=0x0ff000f0	sig=0x01b00010	prob=0.08	name=DEC_fetch_oper_dpi_rshift_unop_r_lsl_movs_
mask=0x0ff000f0	sig=0x01b00030	prob=0.08	name=DEC_fetch_oper_dpi_rshift_unop_r_lsr_movs_
mask=0x0ff000f0	sig=0x01b00070	prob=0.08	name=DEC_fetch_oper_dpi_rshift_unop_r_ror_movs_
mask=0x0ff00090	sig=0x01b00090	prob=0.04	name=DEC_fetch_oper_load_ext_reg_mem_mode_5_
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_24, 16)
  _STUB_NAME(stub_24_0),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_unop_r_lsl_movs_),
  _STUB_NAME(stub_24_2),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_unop_r_lsr_movs_),
  _STUB_NAME(stub_24_4),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_unop_r_asr_movs_),
  _STUB_NAME(stub_24_6),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_unop_r_ror_movs_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsl_movs_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_reg_mem_mode_5_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsr_movs_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_reg_mem_mode_5_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_asr_movs_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_reg_mem_mode_5_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_ror_movs_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_reg_mem_mode_5_),
_TABLE_DEF_END

/*
Table begin: index=28 size=29
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x01c00090
Prob=0.0227684629	Entropy=3.68385619	HTreeHeight=3.76
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x01c000c0	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_bic_
mask=0x0ff00170	sig=0x01c00140	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_bic_
mask=0x0ff00270	sig=0x01c00240	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_bic_
mask=0x0ff00470	sig=0x01c00440	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_bic_
mask=0x0ff00870	sig=0x01c00840	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_bic_
mask=0x0ff000f0	sig=0x01c00080	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_bic_
mask=0x0ff00170	sig=0x01c00100	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_bic_
mask=0x0ff00270	sig=0x01c00200	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_bic_
mask=0x0ff00470	sig=0x01c00400	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_bic_
mask=0x0ff00870	sig=0x01c00800	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_bic_
mask=0x0ff00870	sig=0x01c00820	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_bic_
mask=0x0ff00470	sig=0x01c00420	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_bic_
mask=0x0ff00270	sig=0x01c00220	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_bic_
mask=0x0ff00170	sig=0x01c00120	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_bic_
mask=0x0ff000f0	sig=0x01c000a0	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_bic_
mask=0x0ff000f0	sig=0x01c000e0	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_bic_
mask=0x0ff00170	sig=0x01c00160	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_bic_
mask=0x0ff00270	sig=0x01c00260	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_bic_
mask=0x0ff00470	sig=0x01c00460	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_bic_
mask=0x0ff00870	sig=0x01c00860	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_bic_
mask=0x0ff00ff0	sig=0x01c00040	prob=0.08	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_bic_
mask=0x0ff00ff0	sig=0x01c00000	prob=0.08	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_bic_
mask=0x0ff00ff0	sig=0x01c00020	prob=0.08	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_bic_
mask=0x0ff00ff0	sig=0x01c00060	prob=0.08	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_bic_
mask=0x0ff000f0	sig=0x01c00050	prob=0.08	name=DEC_fetch_oper_dpi_rshift_binop_r_asr_bic_
mask=0x0ff000f0	sig=0x01c00010	prob=0.08	name=DEC_fetch_oper_dpi_rshift_binop_r_lsl_bic_
mask=0x0ff000f0	sig=0x01c00030	prob=0.08	name=DEC_fetch_oper_dpi_rshift_binop_r_lsr_bic_
mask=0x0ff000f0	sig=0x01c00070	prob=0.08	name=DEC_fetch_oper_dpi_rshift_binop_r_ror_bic_
mask=0x0ff00090	sig=0x01c00090	prob=0.04	name=DEC_fetch_oper_store_ext_imm_mem_mode_6_
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_25, 16)
  _STUB_NAME(stub_25_0),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_lsl_bic_),
  _STUB_NAME(stub_25_2),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_lsr_bic_),
  _STUB_NAME(stub_25_4),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_asr_bic_),
  _STUB_NAME(stub_25_6),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_ror_bic_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_bic_),
  _FUNC_NAME(DEC_fetch_oper_store_ext_imm_mem_mode_6_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_bic_),
  _FUNC_NAME(DEC_fetch_oper_store_ext_imm_mem_mode_6_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_bic_),
  _FUNC_NAME(DEC_fetch_oper_store_ext_imm_mem_mode_6_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_bic_),
  _FUNC_NAME(DEC_fetch_oper_store_ext_imm_mem_mode_6_),
_TABLE_DEF_END

/*
Table begin: index=29 size=29
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x01d00090
Prob=0.0227684629	Entropy=3.68385619	HTreeHeight=3.76
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x01d000c0	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_bics_
mask=0x0ff00170	sig=0x01d00140	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_bics_
mask=0x0ff00270	sig=0x01d00240	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_bics_
mask=0x0ff00470	sig=0x01d00440	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_bics_
mask=0x0ff00870	sig=0x01d00840	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_bics_
mask=0x0ff000f0	sig=0x01d00080	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_bics_
mask=0x0ff00170	sig=0x01d00100	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_bics_
mask=0x0ff00270	sig=0x01d00200	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_bics_
mask=0x0ff00470	sig=0x01d00400	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_bics_
mask=0x0ff00870	sig=0x01d00800	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_bics_
mask=0x0ff00870	sig=0x01d00820	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_bics_
mask=0x0ff00470	sig=0x01d00420	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_bics_
mask=0x0ff00270	sig=0x01d00220	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_bics_
mask=0x0ff00170	sig=0x01d00120	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_bics_
mask=0x0ff000f0	sig=0x01d000a0	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_bics_
mask=0x0ff000f0	sig=0x01d000e0	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_bics_
mask=0x0ff00170	sig=0x01d00160	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_bics_
mask=0x0ff00270	sig=0x01d00260	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_bics_
mask=0x0ff00470	sig=0x01d00460	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_bics_
mask=0x0ff00870	sig=0x01d00860	prob=0.016	name=DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_bics_
mask=0x0ff00ff0	sig=0x01d00040	prob=0.08	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_bics_
mask=0x0ff00ff0	sig=0x01d00000	prob=0.08	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_bics_
mask=0x0ff00ff0	sig=0x01d00020	prob=0.08	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_bics_
mask=0x0ff00ff0	sig=0x01d00060	prob=0.08	name=DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_bics_
mask=0x0ff000f0	sig=0x01d00050	prob=0.08	name=DEC_fetch_oper_dpi_rshift_binop_r_asr_bics_
mask=0x0ff000f0	sig=0x01d00010	prob=0.08	name=DEC_fetch_oper_dpi_rshift_binop_r_lsl_bics_
mask=0x0ff000f0	sig=0x01d00030	prob=0.08	name=DEC_fetch_oper_dpi_rshift_binop_r_lsr_bics_
mask=0x0ff000f0	sig=0x01d00070	prob=0.08	name=DEC_fetch_oper_dpi_rshift_binop_r_ror_bics_
mask=0x0ff00090	sig=0x01d00090	prob=0.04	name=DEC_fetch_oper_load_ext_imm_mem_mode_6_
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_26, 16)
  _STUB_NAME(stub_26_0),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_lsl_bics_),
  _STUB_NAME(stub_26_2),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_lsr_bics_),
  _STUB_NAME(stub_26_4),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_asr_bics_),
  _STUB_NAME(stub_26_6),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_binop_r_ror_bics_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_bics_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_imm_mem_mode_6_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_bics_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_imm_mem_mode_6_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_bics_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_imm_mem_mode_6_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_bics_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_imm_mem_mode_6_),
_TABLE_DEF_END

/*
Table begin: index=30 size=29
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x01e00090
Prob=0.0227684629	Entropy=3.68385619	HTreeHeight=3.76
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x01e000c0	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_asr_mvn_
mask=0x0ff00170	sig=0x01e00140	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_asr_mvn_
mask=0x0ff00270	sig=0x01e00240	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_asr_mvn_
mask=0x0ff00470	sig=0x01e00440	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_asr_mvn_
mask=0x0ff00870	sig=0x01e00840	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_asr_mvn_
mask=0x0ff000f0	sig=0x01e00080	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsl_mvn_
mask=0x0ff00170	sig=0x01e00100	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsl_mvn_
mask=0x0ff00270	sig=0x01e00200	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsl_mvn_
mask=0x0ff00470	sig=0x01e00400	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsl_mvn_
mask=0x0ff00870	sig=0x01e00800	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsl_mvn_
mask=0x0ff00870	sig=0x01e00820	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsr_mvn_
mask=0x0ff00470	sig=0x01e00420	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsr_mvn_
mask=0x0ff00270	sig=0x01e00220	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsr_mvn_
mask=0x0ff00170	sig=0x01e00120	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsr_mvn_
mask=0x0ff000f0	sig=0x01e000a0	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsr_mvn_
mask=0x0ff000f0	sig=0x01e000e0	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_ror_mvn_
mask=0x0ff00170	sig=0x01e00160	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_ror_mvn_
mask=0x0ff00270	sig=0x01e00260	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_ror_mvn_
mask=0x0ff00470	sig=0x01e00460	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_ror_mvn_
mask=0x0ff00870	sig=0x01e00860	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_ror_mvn_
mask=0x0ff00ff0	sig=0x01e00040	prob=0.08	name=DEC_fetch_oper_dpi_ishift_unop_zero_shift_zero_asr_mvn_
mask=0x0ff00ff0	sig=0x01e00000	prob=0.08	name=DEC_fetch_oper_dpi_ishift_unop_zero_shift_zero_lsl_mvn_
mask=0x0ff00ff0	sig=0x01e00020	prob=0.08	name=DEC_fetch_oper_dpi_ishift_unop_zero_shift_zero_lsr_mvn_
mask=0x0ff00ff0	sig=0x01e00060	prob=0.08	name=DEC_fetch_oper_dpi_ishift_unop_zero_shift_zero_ror_mvn_
mask=0x0ff000f0	sig=0x01e00050	prob=0.08	name=DEC_fetch_oper_dpi_rshift_unop_r_asr_mvn_
mask=0x0ff000f0	sig=0x01e00010	prob=0.08	name=DEC_fetch_oper_dpi_rshift_unop_r_lsl_mvn_
mask=0x0ff000f0	sig=0x01e00030	prob=0.08	name=DEC_fetch_oper_dpi_rshift_unop_r_lsr_mvn_
mask=0x0ff000f0	sig=0x01e00070	prob=0.08	name=DEC_fetch_oper_dpi_rshift_unop_r_ror_mvn_
mask=0x0ff00090	sig=0x01e00090	prob=0.04	name=DEC_fetch_oper_store_ext_imm_mem_mode_7_
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_27, 16)
  _STUB_NAME(stub_27_0),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_unop_r_lsl_mvn_),
  _STUB_NAME(stub_27_2),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_unop_r_lsr_mvn_),
  _STUB_NAME(stub_27_4),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_unop_r_asr_mvn_),
  _STUB_NAME(stub_27_6),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_unop_r_ror_mvn_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsl_mvn_),
  _FUNC_NAME(DEC_fetch_oper_store_ext_imm_mem_mode_7_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsr_mvn_),
  _FUNC_NAME(DEC_fetch_oper_store_ext_imm_mem_mode_7_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_asr_mvn_),
  _FUNC_NAME(DEC_fetch_oper_store_ext_imm_mem_mode_7_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_ror_mvn_),
  _FUNC_NAME(DEC_fetch_oper_store_ext_imm_mem_mode_7_),
_TABLE_DEF_END

/*
Table begin: index=31 size=29
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x01f00090
Prob=0.0227684629	Entropy=3.68385619	HTreeHeight=3.76
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x01f000c0	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_asr_mvns_
mask=0x0ff00170	sig=0x01f00140	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_asr_mvns_
mask=0x0ff00270	sig=0x01f00240	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_asr_mvns_
mask=0x0ff00470	sig=0x01f00440	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_asr_mvns_
mask=0x0ff00870	sig=0x01f00840	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_asr_mvns_
mask=0x0ff000f0	sig=0x01f00080	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsl_mvns_
mask=0x0ff00170	sig=0x01f00100	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsl_mvns_
mask=0x0ff00270	sig=0x01f00200	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsl_mvns_
mask=0x0ff00470	sig=0x01f00400	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsl_mvns_
mask=0x0ff00870	sig=0x01f00800	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsl_mvns_
mask=0x0ff00870	sig=0x01f00820	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsr_mvns_
mask=0x0ff00470	sig=0x01f00420	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsr_mvns_
mask=0x0ff00270	sig=0x01f00220	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsr_mvns_
mask=0x0ff00170	sig=0x01f00120	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsr_mvns_
mask=0x0ff000f0	sig=0x01f000a0	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsr_mvns_
mask=0x0ff000f0	sig=0x01f000e0	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_ror_mvns_
mask=0x0ff00170	sig=0x01f00160	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_ror_mvns_
mask=0x0ff00270	sig=0x01f00260	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_ror_mvns_
mask=0x0ff00470	sig=0x01f00460	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_ror_mvns_
mask=0x0ff00870	sig=0x01f00860	prob=0.016	name=DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_ror_mvns_
mask=0x0ff00ff0	sig=0x01f00040	prob=0.08	name=DEC_fetch_oper_dpi_ishift_unop_zero_shift_zero_asr_mvns_
mask=0x0ff00ff0	sig=0x01f00000	prob=0.08	name=DEC_fetch_oper_dpi_ishift_unop_zero_shift_zero_lsl_mvns_
mask=0x0ff00ff0	sig=0x01f00020	prob=0.08	name=DEC_fetch_oper_dpi_ishift_unop_zero_shift_zero_lsr_mvns_
mask=0x0ff00ff0	sig=0x01f00060	prob=0.08	name=DEC_fetch_oper_dpi_ishift_unop_zero_shift_zero_ror_mvns_
mask=0x0ff000f0	sig=0x01f00050	prob=0.08	name=DEC_fetch_oper_dpi_rshift_unop_r_asr_mvns_
mask=0x0ff000f0	sig=0x01f00010	prob=0.08	name=DEC_fetch_oper_dpi_rshift_unop_r_lsl_mvns_
mask=0x0ff000f0	sig=0x01f00030	prob=0.08	name=DEC_fetch_oper_dpi_rshift_unop_r_lsr_mvns_
mask=0x0ff000f0	sig=0x01f00070	prob=0.08	name=DEC_fetch_oper_dpi_rshift_unop_r_ror_mvns_
mask=0x0ff00090	sig=0x01f00090	prob=0.04	name=DEC_fetch_oper_load_ext_imm_mem_mode_7_
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_28, 16)
  _STUB_NAME(stub_28_0),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_unop_r_lsl_mvns_),
  _STUB_NAME(stub_28_2),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_unop_r_lsr_mvns_),
  _STUB_NAME(stub_28_4),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_unop_r_asr_mvns_),
  _STUB_NAME(stub_28_6),
  _FUNC_NAME(DEC_fetch_oper_dpi_rshift_unop_r_ror_mvns_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsl_mvns_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_imm_mem_mode_7_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsr_mvns_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_imm_mem_mode_7_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_asr_mvns_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_imm_mem_mode_7_),
  _FUNC_NAME(DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_ror_mvns_),
  _FUNC_NAME(DEC_fetch_oper_load_ext_imm_mem_mode_7_),
_TABLE_DEF_END

/*
Table begin: index=96 size=25
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x06000010
Prob=0.00728596506	Entropy=3.00012038	HTreeHeight=3.12500684
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x060000c0	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_0_
mask=0x0ff00170	sig=0x06000140	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_0_
mask=0x0ff00270	sig=0x06000240	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_0_
mask=0x0ff00470	sig=0x06000440	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_0_
mask=0x0ff00870	sig=0x06000840	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_0_
mask=0x0ff000f0	sig=0x06000080	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_0_
mask=0x0ff00170	sig=0x06000100	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_0_
mask=0x0ff00270	sig=0x06000200	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_0_
mask=0x0ff00470	sig=0x06000400	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_0_
mask=0x0ff00870	sig=0x06000800	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_0_
mask=0x0ff00870	sig=0x06000820	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_0_
mask=0x0ff00470	sig=0x06000420	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_0_
mask=0x0ff00270	sig=0x06000220	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_0_
mask=0x0ff00170	sig=0x06000120	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_0_
mask=0x0ff000f0	sig=0x060000a0	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_0_
mask=0x0ff000f0	sig=0x060000e0	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_0_
mask=0x0ff00170	sig=0x06000160	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_0_
mask=0x0ff00270	sig=0x06000260	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_0_
mask=0x0ff00470	sig=0x06000460	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_0_
mask=0x0ff00870	sig=0x06000860	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_0_
mask=0x0ff00ff0	sig=0x06000040	prob=0.124999023	name=DEC_fetch_oper_store_reg_zero_shift_zero_asr_mem_mode_0_
mask=0x0ff00ff0	sig=0x06000000	prob=0.124999023	name=DEC_fetch_oper_store_reg_zero_shift_zero_lsl_mem_mode_0_
mask=0x0ff00ff0	sig=0x06000020	prob=0.124999023	name=DEC_fetch_oper_store_reg_zero_shift_zero_lsr_mem_mode_0_
mask=0x0ff00ff0	sig=0x06000060	prob=0.124999023	name=DEC_fetch_oper_store_reg_zero_shift_zero_ror_mem_mode_0_
mask=0x0ff00010	sig=0x06000010	prob=7.81243897e-06	name=DEC_fetch_oper_unknown
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_29, 16)
  _STUB_NAME(stub_29_0),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_29_2),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_29_4),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_29_6),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_0_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_0_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_0_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_0_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
_TABLE_DEF_END

/*
Table begin: index=97 size=25
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x06100010
Prob=0.00728596506	Entropy=3.00012038	HTreeHeight=3.12500684
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x061000c0	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_0_
mask=0x0ff00170	sig=0x06100140	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_0_
mask=0x0ff00270	sig=0x06100240	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_0_
mask=0x0ff00470	sig=0x06100440	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_0_
mask=0x0ff00870	sig=0x06100840	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_0_
mask=0x0ff000f0	sig=0x06100080	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_0_
mask=0x0ff00170	sig=0x06100100	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_0_
mask=0x0ff00270	sig=0x06100200	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_0_
mask=0x0ff00470	sig=0x06100400	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_0_
mask=0x0ff00870	sig=0x06100800	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_0_
mask=0x0ff00870	sig=0x06100820	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_0_
mask=0x0ff00470	sig=0x06100420	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_0_
mask=0x0ff00270	sig=0x06100220	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_0_
mask=0x0ff00170	sig=0x06100120	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_0_
mask=0x0ff000f0	sig=0x061000a0	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_0_
mask=0x0ff000f0	sig=0x061000e0	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_0_
mask=0x0ff00170	sig=0x06100160	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_0_
mask=0x0ff00270	sig=0x06100260	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_0_
mask=0x0ff00470	sig=0x06100460	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_0_
mask=0x0ff00870	sig=0x06100860	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_0_
mask=0x0ff00ff0	sig=0x06100040	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_asr_mem_mode_0_
mask=0x0ff00ff0	sig=0x06100000	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_lsl_mem_mode_0_
mask=0x0ff00ff0	sig=0x06100020	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_lsr_mem_mode_0_
mask=0x0ff00ff0	sig=0x06100060	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_ror_mem_mode_0_
mask=0x0ff00010	sig=0x06100010	prob=7.81243897e-06	name=DEC_fetch_oper_unknown
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_30, 16)
  _STUB_NAME(stub_30_0),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_30_2),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_30_4),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_30_6),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_0_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_0_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_0_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_0_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
_TABLE_DEF_END

/*
Table begin: index=98 size=25
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x06200010
Prob=0.00728596506	Entropy=3.00012038	HTreeHeight=3.12500684
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x062000c0	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_1_
mask=0x0ff00170	sig=0x06200140	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_1_
mask=0x0ff00270	sig=0x06200240	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_1_
mask=0x0ff00470	sig=0x06200440	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_1_
mask=0x0ff00870	sig=0x06200840	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_1_
mask=0x0ff000f0	sig=0x06200080	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_1_
mask=0x0ff00170	sig=0x06200100	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_1_
mask=0x0ff00270	sig=0x06200200	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_1_
mask=0x0ff00470	sig=0x06200400	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_1_
mask=0x0ff00870	sig=0x06200800	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_1_
mask=0x0ff00870	sig=0x06200820	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_1_
mask=0x0ff00470	sig=0x06200420	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_1_
mask=0x0ff00270	sig=0x06200220	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_1_
mask=0x0ff00170	sig=0x06200120	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_1_
mask=0x0ff000f0	sig=0x062000a0	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_1_
mask=0x0ff000f0	sig=0x062000e0	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_1_
mask=0x0ff00170	sig=0x06200160	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_1_
mask=0x0ff00270	sig=0x06200260	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_1_
mask=0x0ff00470	sig=0x06200460	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_1_
mask=0x0ff00870	sig=0x06200860	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_1_
mask=0x0ff00ff0	sig=0x06200040	prob=0.124999023	name=DEC_fetch_oper_store_reg_zero_shift_zero_asr_mem_mode_1_
mask=0x0ff00ff0	sig=0x06200000	prob=0.124999023	name=DEC_fetch_oper_store_reg_zero_shift_zero_lsl_mem_mode_1_
mask=0x0ff00ff0	sig=0x06200020	prob=0.124999023	name=DEC_fetch_oper_store_reg_zero_shift_zero_lsr_mem_mode_1_
mask=0x0ff00ff0	sig=0x06200060	prob=0.124999023	name=DEC_fetch_oper_store_reg_zero_shift_zero_ror_mem_mode_1_
mask=0x0ff00010	sig=0x06200010	prob=7.81243897e-06	name=DEC_fetch_oper_unknown
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_31, 16)
  _STUB_NAME(stub_31_0),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_31_2),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_31_4),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_31_6),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_1_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_1_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_1_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_1_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
_TABLE_DEF_END

/*
Table begin: index=99 size=25
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x06300010
Prob=0.00728596506	Entropy=3.00012038	HTreeHeight=3.12500684
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x063000c0	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_1_
mask=0x0ff00170	sig=0x06300140	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_1_
mask=0x0ff00270	sig=0x06300240	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_1_
mask=0x0ff00470	sig=0x06300440	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_1_
mask=0x0ff00870	sig=0x06300840	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_1_
mask=0x0ff000f0	sig=0x06300080	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_1_
mask=0x0ff00170	sig=0x06300100	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_1_
mask=0x0ff00270	sig=0x06300200	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_1_
mask=0x0ff00470	sig=0x06300400	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_1_
mask=0x0ff00870	sig=0x06300800	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_1_
mask=0x0ff00870	sig=0x06300820	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_1_
mask=0x0ff00470	sig=0x06300420	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_1_
mask=0x0ff00270	sig=0x06300220	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_1_
mask=0x0ff00170	sig=0x06300120	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_1_
mask=0x0ff000f0	sig=0x063000a0	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_1_
mask=0x0ff000f0	sig=0x063000e0	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_1_
mask=0x0ff00170	sig=0x06300160	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_1_
mask=0x0ff00270	sig=0x06300260	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_1_
mask=0x0ff00470	sig=0x06300460	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_1_
mask=0x0ff00870	sig=0x06300860	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_1_
mask=0x0ff00ff0	sig=0x06300040	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_asr_mem_mode_1_
mask=0x0ff00ff0	sig=0x06300000	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_lsl_mem_mode_1_
mask=0x0ff00ff0	sig=0x06300020	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_lsr_mem_mode_1_
mask=0x0ff00ff0	sig=0x06300060	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_ror_mem_mode_1_
mask=0x0ff00010	sig=0x06300010	prob=7.81243897e-06	name=DEC_fetch_oper_unknown
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_32, 16)
  _STUB_NAME(stub_32_0),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_32_2),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_32_4),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_32_6),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_1_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_1_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_1_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_1_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
_TABLE_DEF_END

/*
Table begin: index=100 size=25
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x06400010
Prob=0.00728596506	Entropy=3.00012038	HTreeHeight=3.12500684
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x064000c0	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_2_
mask=0x0ff00170	sig=0x06400140	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_2_
mask=0x0ff00270	sig=0x06400240	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_2_
mask=0x0ff00470	sig=0x06400440	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_2_
mask=0x0ff00870	sig=0x06400840	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_2_
mask=0x0ff000f0	sig=0x06400080	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_2_
mask=0x0ff00170	sig=0x06400100	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_2_
mask=0x0ff00270	sig=0x06400200	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_2_
mask=0x0ff00470	sig=0x06400400	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_2_
mask=0x0ff00870	sig=0x06400800	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_2_
mask=0x0ff00870	sig=0x06400820	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_2_
mask=0x0ff00470	sig=0x06400420	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_2_
mask=0x0ff00270	sig=0x06400220	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_2_
mask=0x0ff00170	sig=0x06400120	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_2_
mask=0x0ff000f0	sig=0x064000a0	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_2_
mask=0x0ff000f0	sig=0x064000e0	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_2_
mask=0x0ff00170	sig=0x06400160	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_2_
mask=0x0ff00270	sig=0x06400260	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_2_
mask=0x0ff00470	sig=0x06400460	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_2_
mask=0x0ff00870	sig=0x06400860	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_2_
mask=0x0ff00ff0	sig=0x06400040	prob=0.124999023	name=DEC_fetch_oper_store_reg_zero_shift_zero_asr_mem_mode_2_
mask=0x0ff00ff0	sig=0x06400000	prob=0.124999023	name=DEC_fetch_oper_store_reg_zero_shift_zero_lsl_mem_mode_2_
mask=0x0ff00ff0	sig=0x06400020	prob=0.124999023	name=DEC_fetch_oper_store_reg_zero_shift_zero_lsr_mem_mode_2_
mask=0x0ff00ff0	sig=0x06400060	prob=0.124999023	name=DEC_fetch_oper_store_reg_zero_shift_zero_ror_mem_mode_2_
mask=0x0ff00010	sig=0x06400010	prob=7.81243897e-06	name=DEC_fetch_oper_unknown
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_33, 16)
  _STUB_NAME(stub_33_0),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_33_2),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_33_4),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_33_6),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_2_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_2_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_2_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_2_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
_TABLE_DEF_END

/*
Table begin: index=101 size=25
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x06500010
Prob=0.00728596506	Entropy=3.00012038	HTreeHeight=3.12500684
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x065000c0	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_2_
mask=0x0ff00170	sig=0x06500140	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_2_
mask=0x0ff00270	sig=0x06500240	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_2_
mask=0x0ff00470	sig=0x06500440	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_2_
mask=0x0ff00870	sig=0x06500840	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_2_
mask=0x0ff000f0	sig=0x06500080	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_2_
mask=0x0ff00170	sig=0x06500100	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_2_
mask=0x0ff00270	sig=0x06500200	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_2_
mask=0x0ff00470	sig=0x06500400	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_2_
mask=0x0ff00870	sig=0x06500800	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_2_
mask=0x0ff00870	sig=0x06500820	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_2_
mask=0x0ff00470	sig=0x06500420	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_2_
mask=0x0ff00270	sig=0x06500220	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_2_
mask=0x0ff00170	sig=0x06500120	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_2_
mask=0x0ff000f0	sig=0x065000a0	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_2_
mask=0x0ff000f0	sig=0x065000e0	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_2_
mask=0x0ff00170	sig=0x06500160	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_2_
mask=0x0ff00270	sig=0x06500260	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_2_
mask=0x0ff00470	sig=0x06500460	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_2_
mask=0x0ff00870	sig=0x06500860	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_2_
mask=0x0ff00ff0	sig=0x06500040	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_asr_mem_mode_2_
mask=0x0ff00ff0	sig=0x06500000	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_lsl_mem_mode_2_
mask=0x0ff00ff0	sig=0x06500020	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_lsr_mem_mode_2_
mask=0x0ff00ff0	sig=0x06500060	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_ror_mem_mode_2_
mask=0x0ff00010	sig=0x06500010	prob=7.81243897e-06	name=DEC_fetch_oper_unknown
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_34, 16)
  _STUB_NAME(stub_34_0),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_34_2),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_34_4),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_34_6),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_2_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_2_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_2_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_2_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
_TABLE_DEF_END

/*
Table begin: index=102 size=25
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x06600010
Prob=0.00728596506	Entropy=3.00012038	HTreeHeight=3.12500684
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x066000c0	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_3_
mask=0x0ff00170	sig=0x06600140	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_3_
mask=0x0ff00270	sig=0x06600240	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_3_
mask=0x0ff00470	sig=0x06600440	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_3_
mask=0x0ff00870	sig=0x06600840	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_3_
mask=0x0ff000f0	sig=0x06600080	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_3_
mask=0x0ff00170	sig=0x06600100	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_3_
mask=0x0ff00270	sig=0x06600200	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_3_
mask=0x0ff00470	sig=0x06600400	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_3_
mask=0x0ff00870	sig=0x06600800	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_3_
mask=0x0ff00870	sig=0x06600820	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_3_
mask=0x0ff00470	sig=0x06600420	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_3_
mask=0x0ff00270	sig=0x06600220	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_3_
mask=0x0ff00170	sig=0x06600120	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_3_
mask=0x0ff000f0	sig=0x066000a0	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_3_
mask=0x0ff000f0	sig=0x066000e0	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_3_
mask=0x0ff00170	sig=0x06600160	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_3_
mask=0x0ff00270	sig=0x06600260	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_3_
mask=0x0ff00470	sig=0x06600460	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_3_
mask=0x0ff00870	sig=0x06600860	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_3_
mask=0x0ff00ff0	sig=0x06600040	prob=0.124999023	name=DEC_fetch_oper_store_reg_zero_shift_zero_asr_mem_mode_3_
mask=0x0ff00ff0	sig=0x06600000	prob=0.124999023	name=DEC_fetch_oper_store_reg_zero_shift_zero_lsl_mem_mode_3_
mask=0x0ff00ff0	sig=0x06600020	prob=0.124999023	name=DEC_fetch_oper_store_reg_zero_shift_zero_lsr_mem_mode_3_
mask=0x0ff00ff0	sig=0x06600060	prob=0.124999023	name=DEC_fetch_oper_store_reg_zero_shift_zero_ror_mem_mode_3_
mask=0x0ff00010	sig=0x06600010	prob=7.81243897e-06	name=DEC_fetch_oper_unknown
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_35, 16)
  _STUB_NAME(stub_35_0),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_35_2),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_35_4),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_35_6),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_3_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_3_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_3_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_3_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
_TABLE_DEF_END

/*
Table begin: index=103 size=25
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x06700010
Prob=0.00728596506	Entropy=3.00012038	HTreeHeight=3.12500684
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x067000c0	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_3_
mask=0x0ff00170	sig=0x06700140	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_3_
mask=0x0ff00270	sig=0x06700240	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_3_
mask=0x0ff00470	sig=0x06700440	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_3_
mask=0x0ff00870	sig=0x06700840	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_3_
mask=0x0ff000f0	sig=0x06700080	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_3_
mask=0x0ff00170	sig=0x06700100	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_3_
mask=0x0ff00270	sig=0x06700200	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_3_
mask=0x0ff00470	sig=0x06700400	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_3_
mask=0x0ff00870	sig=0x06700800	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_3_
mask=0x0ff00870	sig=0x06700820	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_3_
mask=0x0ff00470	sig=0x06700420	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_3_
mask=0x0ff00270	sig=0x06700220	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_3_
mask=0x0ff00170	sig=0x06700120	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_3_
mask=0x0ff000f0	sig=0x067000a0	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_3_
mask=0x0ff000f0	sig=0x067000e0	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_3_
mask=0x0ff00170	sig=0x06700160	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_3_
mask=0x0ff00270	sig=0x06700260	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_3_
mask=0x0ff00470	sig=0x06700460	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_3_
mask=0x0ff00870	sig=0x06700860	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_3_
mask=0x0ff00ff0	sig=0x06700040	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_asr_mem_mode_3_
mask=0x0ff00ff0	sig=0x06700000	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_lsl_mem_mode_3_
mask=0x0ff00ff0	sig=0x06700020	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_lsr_mem_mode_3_
mask=0x0ff00ff0	sig=0x06700060	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_ror_mem_mode_3_
mask=0x0ff00010	sig=0x06700010	prob=7.81243897e-06	name=DEC_fetch_oper_unknown
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_36, 16)
  _STUB_NAME(stub_36_0),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_36_2),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_36_4),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_36_6),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_3_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_3_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_3_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_3_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
_TABLE_DEF_END

/*
Table begin: index=104 size=25
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x06800010
Prob=0.00728596506	Entropy=3.00012038	HTreeHeight=3.12500684
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x068000c0	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_0_
mask=0x0ff00170	sig=0x06800140	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_0_
mask=0x0ff00270	sig=0x06800240	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_0_
mask=0x0ff00470	sig=0x06800440	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_0_
mask=0x0ff00870	sig=0x06800840	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_0_
mask=0x0ff000f0	sig=0x06800080	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_0_
mask=0x0ff00170	sig=0x06800100	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_0_
mask=0x0ff00270	sig=0x06800200	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_0_
mask=0x0ff00470	sig=0x06800400	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_0_
mask=0x0ff00870	sig=0x06800800	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_0_
mask=0x0ff00870	sig=0x06800820	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_0_
mask=0x0ff00470	sig=0x06800420	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_0_
mask=0x0ff00270	sig=0x06800220	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_0_
mask=0x0ff00170	sig=0x06800120	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_0_
mask=0x0ff000f0	sig=0x068000a0	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_0_
mask=0x0ff000f0	sig=0x068000e0	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_0_
mask=0x0ff00170	sig=0x06800160	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_0_
mask=0x0ff00270	sig=0x06800260	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_0_
mask=0x0ff00470	sig=0x06800460	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_0_
mask=0x0ff00870	sig=0x06800860	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_0_
mask=0x0ff00ff0	sig=0x06800040	prob=0.124999023	name=DEC_fetch_oper_store_reg_zero_shift_zero_asr_mem_mode_0_
mask=0x0ff00ff0	sig=0x06800000	prob=0.124999023	name=DEC_fetch_oper_store_reg_zero_shift_zero_lsl_mem_mode_0_
mask=0x0ff00ff0	sig=0x06800020	prob=0.124999023	name=DEC_fetch_oper_store_reg_zero_shift_zero_lsr_mem_mode_0_
mask=0x0ff00ff0	sig=0x06800060	prob=0.124999023	name=DEC_fetch_oper_store_reg_zero_shift_zero_ror_mem_mode_0_
mask=0x0ff00010	sig=0x06800010	prob=7.81243897e-06	name=DEC_fetch_oper_unknown
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_37, 16)
  _STUB_NAME(stub_37_0),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_37_2),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_37_4),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_37_6),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_0_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_0_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_0_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_0_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
_TABLE_DEF_END

/*
Table begin: index=105 size=25
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x06900010
Prob=0.00728596506	Entropy=3.00012038	HTreeHeight=3.12500684
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x069000c0	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_0_
mask=0x0ff00170	sig=0x06900140	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_0_
mask=0x0ff00270	sig=0x06900240	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_0_
mask=0x0ff00470	sig=0x06900440	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_0_
mask=0x0ff00870	sig=0x06900840	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_0_
mask=0x0ff000f0	sig=0x06900080	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_0_
mask=0x0ff00170	sig=0x06900100	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_0_
mask=0x0ff00270	sig=0x06900200	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_0_
mask=0x0ff00470	sig=0x06900400	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_0_
mask=0x0ff00870	sig=0x06900800	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_0_
mask=0x0ff00870	sig=0x06900820	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_0_
mask=0x0ff00470	sig=0x06900420	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_0_
mask=0x0ff00270	sig=0x06900220	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_0_
mask=0x0ff00170	sig=0x06900120	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_0_
mask=0x0ff000f0	sig=0x069000a0	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_0_
mask=0x0ff000f0	sig=0x069000e0	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_0_
mask=0x0ff00170	sig=0x06900160	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_0_
mask=0x0ff00270	sig=0x06900260	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_0_
mask=0x0ff00470	sig=0x06900460	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_0_
mask=0x0ff00870	sig=0x06900860	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_0_
mask=0x0ff00ff0	sig=0x06900040	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_asr_mem_mode_0_
mask=0x0ff00ff0	sig=0x06900000	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_lsl_mem_mode_0_
mask=0x0ff00ff0	sig=0x06900020	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_lsr_mem_mode_0_
mask=0x0ff00ff0	sig=0x06900060	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_ror_mem_mode_0_
mask=0x0ff00010	sig=0x06900010	prob=7.81243897e-06	name=DEC_fetch_oper_unknown
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_38, 16)
  _STUB_NAME(stub_38_0),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_38_2),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_38_4),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_38_6),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_0_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_0_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_0_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_0_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
_TABLE_DEF_END

/*
Table begin: index=106 size=25
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x06a00010
Prob=0.00728596506	Entropy=3.00012038	HTreeHeight=3.12500684
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x06a000c0	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_1_
mask=0x0ff00170	sig=0x06a00140	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_1_
mask=0x0ff00270	sig=0x06a00240	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_1_
mask=0x0ff00470	sig=0x06a00440	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_1_
mask=0x0ff00870	sig=0x06a00840	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_1_
mask=0x0ff000f0	sig=0x06a00080	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_1_
mask=0x0ff00170	sig=0x06a00100	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_1_
mask=0x0ff00270	sig=0x06a00200	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_1_
mask=0x0ff00470	sig=0x06a00400	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_1_
mask=0x0ff00870	sig=0x06a00800	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_1_
mask=0x0ff00870	sig=0x06a00820	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_1_
mask=0x0ff00470	sig=0x06a00420	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_1_
mask=0x0ff00270	sig=0x06a00220	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_1_
mask=0x0ff00170	sig=0x06a00120	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_1_
mask=0x0ff000f0	sig=0x06a000a0	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_1_
mask=0x0ff000f0	sig=0x06a000e0	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_1_
mask=0x0ff00170	sig=0x06a00160	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_1_
mask=0x0ff00270	sig=0x06a00260	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_1_
mask=0x0ff00470	sig=0x06a00460	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_1_
mask=0x0ff00870	sig=0x06a00860	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_1_
mask=0x0ff00ff0	sig=0x06a00040	prob=0.124999023	name=DEC_fetch_oper_store_reg_zero_shift_zero_asr_mem_mode_1_
mask=0x0ff00ff0	sig=0x06a00000	prob=0.124999023	name=DEC_fetch_oper_store_reg_zero_shift_zero_lsl_mem_mode_1_
mask=0x0ff00ff0	sig=0x06a00020	prob=0.124999023	name=DEC_fetch_oper_store_reg_zero_shift_zero_lsr_mem_mode_1_
mask=0x0ff00ff0	sig=0x06a00060	prob=0.124999023	name=DEC_fetch_oper_store_reg_zero_shift_zero_ror_mem_mode_1_
mask=0x0ff00010	sig=0x06a00010	prob=7.81243897e-06	name=DEC_fetch_oper_unknown
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_39, 16)
  _STUB_NAME(stub_39_0),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_39_2),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_39_4),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_39_6),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_1_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_1_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_1_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_1_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
_TABLE_DEF_END

/*
Table begin: index=107 size=25
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x06b00010
Prob=0.00728596506	Entropy=3.00012038	HTreeHeight=3.12500684
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x06b000c0	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_1_
mask=0x0ff00170	sig=0x06b00140	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_1_
mask=0x0ff00270	sig=0x06b00240	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_1_
mask=0x0ff00470	sig=0x06b00440	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_1_
mask=0x0ff00870	sig=0x06b00840	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_1_
mask=0x0ff000f0	sig=0x06b00080	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_1_
mask=0x0ff00170	sig=0x06b00100	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_1_
mask=0x0ff00270	sig=0x06b00200	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_1_
mask=0x0ff00470	sig=0x06b00400	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_1_
mask=0x0ff00870	sig=0x06b00800	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_1_
mask=0x0ff00870	sig=0x06b00820	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_1_
mask=0x0ff00470	sig=0x06b00420	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_1_
mask=0x0ff00270	sig=0x06b00220	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_1_
mask=0x0ff00170	sig=0x06b00120	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_1_
mask=0x0ff000f0	sig=0x06b000a0	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_1_
mask=0x0ff000f0	sig=0x06b000e0	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_1_
mask=0x0ff00170	sig=0x06b00160	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_1_
mask=0x0ff00270	sig=0x06b00260	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_1_
mask=0x0ff00470	sig=0x06b00460	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_1_
mask=0x0ff00870	sig=0x06b00860	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_1_
mask=0x0ff00ff0	sig=0x06b00040	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_asr_mem_mode_1_
mask=0x0ff00ff0	sig=0x06b00000	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_lsl_mem_mode_1_
mask=0x0ff00ff0	sig=0x06b00020	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_lsr_mem_mode_1_
mask=0x0ff00ff0	sig=0x06b00060	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_ror_mem_mode_1_
mask=0x0ff00010	sig=0x06b00010	prob=7.81243897e-06	name=DEC_fetch_oper_unknown
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_40, 16)
  _STUB_NAME(stub_40_0),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_40_2),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_40_4),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_40_6),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_1_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_1_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_1_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_1_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
_TABLE_DEF_END

/*
Table begin: index=108 size=25
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x06c00010
Prob=0.00728596506	Entropy=3.00012038	HTreeHeight=3.12500684
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x06c000c0	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_2_
mask=0x0ff00170	sig=0x06c00140	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_2_
mask=0x0ff00270	sig=0x06c00240	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_2_
mask=0x0ff00470	sig=0x06c00440	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_2_
mask=0x0ff00870	sig=0x06c00840	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_2_
mask=0x0ff000f0	sig=0x06c00080	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_2_
mask=0x0ff00170	sig=0x06c00100	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_2_
mask=0x0ff00270	sig=0x06c00200	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_2_
mask=0x0ff00470	sig=0x06c00400	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_2_
mask=0x0ff00870	sig=0x06c00800	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_2_
mask=0x0ff00870	sig=0x06c00820	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_2_
mask=0x0ff00470	sig=0x06c00420	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_2_
mask=0x0ff00270	sig=0x06c00220	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_2_
mask=0x0ff00170	sig=0x06c00120	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_2_
mask=0x0ff000f0	sig=0x06c000a0	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_2_
mask=0x0ff000f0	sig=0x06c000e0	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_2_
mask=0x0ff00170	sig=0x06c00160	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_2_
mask=0x0ff00270	sig=0x06c00260	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_2_
mask=0x0ff00470	sig=0x06c00460	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_2_
mask=0x0ff00870	sig=0x06c00860	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_2_
mask=0x0ff00ff0	sig=0x06c00040	prob=0.124999023	name=DEC_fetch_oper_store_reg_zero_shift_zero_asr_mem_mode_2_
mask=0x0ff00ff0	sig=0x06c00000	prob=0.124999023	name=DEC_fetch_oper_store_reg_zero_shift_zero_lsl_mem_mode_2_
mask=0x0ff00ff0	sig=0x06c00020	prob=0.124999023	name=DEC_fetch_oper_store_reg_zero_shift_zero_lsr_mem_mode_2_
mask=0x0ff00ff0	sig=0x06c00060	prob=0.124999023	name=DEC_fetch_oper_store_reg_zero_shift_zero_ror_mem_mode_2_
mask=0x0ff00010	sig=0x06c00010	prob=7.81243897e-06	name=DEC_fetch_oper_unknown
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_41, 16)
  _STUB_NAME(stub_41_0),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_41_2),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_41_4),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_41_6),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_2_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_2_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_2_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_2_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
_TABLE_DEF_END

/*
Table begin: index=109 size=25
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x06d00010
Prob=0.00728596506	Entropy=3.00012038	HTreeHeight=3.12500684
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x06d000c0	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_2_
mask=0x0ff00170	sig=0x06d00140	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_2_
mask=0x0ff00270	sig=0x06d00240	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_2_
mask=0x0ff00470	sig=0x06d00440	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_2_
mask=0x0ff00870	sig=0x06d00840	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_2_
mask=0x0ff000f0	sig=0x06d00080	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_2_
mask=0x0ff00170	sig=0x06d00100	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_2_
mask=0x0ff00270	sig=0x06d00200	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_2_
mask=0x0ff00470	sig=0x06d00400	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_2_
mask=0x0ff00870	sig=0x06d00800	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_2_
mask=0x0ff00870	sig=0x06d00820	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_2_
mask=0x0ff00470	sig=0x06d00420	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_2_
mask=0x0ff00270	sig=0x06d00220	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_2_
mask=0x0ff00170	sig=0x06d00120	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_2_
mask=0x0ff000f0	sig=0x06d000a0	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_2_
mask=0x0ff000f0	sig=0x06d000e0	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_2_
mask=0x0ff00170	sig=0x06d00160	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_2_
mask=0x0ff00270	sig=0x06d00260	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_2_
mask=0x0ff00470	sig=0x06d00460	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_2_
mask=0x0ff00870	sig=0x06d00860	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_2_
mask=0x0ff00ff0	sig=0x06d00040	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_asr_mem_mode_2_
mask=0x0ff00ff0	sig=0x06d00000	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_lsl_mem_mode_2_
mask=0x0ff00ff0	sig=0x06d00020	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_lsr_mem_mode_2_
mask=0x0ff00ff0	sig=0x06d00060	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_ror_mem_mode_2_
mask=0x0ff00010	sig=0x06d00010	prob=7.81243897e-06	name=DEC_fetch_oper_unknown
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_42, 16)
  _STUB_NAME(stub_42_0),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_42_2),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_42_4),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_42_6),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_2_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_2_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_2_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_2_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
_TABLE_DEF_END

/*
Table begin: index=110 size=25
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x06e00010
Prob=0.00728596506	Entropy=3.00012038	HTreeHeight=3.12500684
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x06e000c0	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_3_
mask=0x0ff00170	sig=0x06e00140	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_3_
mask=0x0ff00270	sig=0x06e00240	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_3_
mask=0x0ff00470	sig=0x06e00440	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_3_
mask=0x0ff00870	sig=0x06e00840	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_3_
mask=0x0ff000f0	sig=0x06e00080	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_3_
mask=0x0ff00170	sig=0x06e00100	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_3_
mask=0x0ff00270	sig=0x06e00200	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_3_
mask=0x0ff00470	sig=0x06e00400	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_3_
mask=0x0ff00870	sig=0x06e00800	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_3_
mask=0x0ff00870	sig=0x06e00820	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_3_
mask=0x0ff00470	sig=0x06e00420	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_3_
mask=0x0ff00270	sig=0x06e00220	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_3_
mask=0x0ff00170	sig=0x06e00120	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_3_
mask=0x0ff000f0	sig=0x06e000a0	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_3_
mask=0x0ff000f0	sig=0x06e000e0	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_3_
mask=0x0ff00170	sig=0x06e00160	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_3_
mask=0x0ff00270	sig=0x06e00260	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_3_
mask=0x0ff00470	sig=0x06e00460	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_3_
mask=0x0ff00870	sig=0x06e00860	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_3_
mask=0x0ff00ff0	sig=0x06e00040	prob=0.124999023	name=DEC_fetch_oper_store_reg_zero_shift_zero_asr_mem_mode_3_
mask=0x0ff00ff0	sig=0x06e00000	prob=0.124999023	name=DEC_fetch_oper_store_reg_zero_shift_zero_lsl_mem_mode_3_
mask=0x0ff00ff0	sig=0x06e00020	prob=0.124999023	name=DEC_fetch_oper_store_reg_zero_shift_zero_lsr_mem_mode_3_
mask=0x0ff00ff0	sig=0x06e00060	prob=0.124999023	name=DEC_fetch_oper_store_reg_zero_shift_zero_ror_mem_mode_3_
mask=0x0ff00010	sig=0x06e00010	prob=7.81243897e-06	name=DEC_fetch_oper_unknown
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_43, 16)
  _STUB_NAME(stub_43_0),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_43_2),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_43_4),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_43_6),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_3_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_3_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_3_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_3_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
_TABLE_DEF_END

/*
Table begin: index=111 size=25
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x06f00010
Prob=0.00728596506	Entropy=3.00012038	HTreeHeight=3.12500684
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x06f000c0	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_3_
mask=0x0ff00170	sig=0x06f00140	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_3_
mask=0x0ff00270	sig=0x06f00240	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_3_
mask=0x0ff00470	sig=0x06f00440	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_3_
mask=0x0ff00870	sig=0x06f00840	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_3_
mask=0x0ff000f0	sig=0x06f00080	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_3_
mask=0x0ff00170	sig=0x06f00100	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_3_
mask=0x0ff00270	sig=0x06f00200	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_3_
mask=0x0ff00470	sig=0x06f00400	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_3_
mask=0x0ff00870	sig=0x06f00800	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_3_
mask=0x0ff00870	sig=0x06f00820	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_3_
mask=0x0ff00470	sig=0x06f00420	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_3_
mask=0x0ff00270	sig=0x06f00220	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_3_
mask=0x0ff00170	sig=0x06f00120	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_3_
mask=0x0ff000f0	sig=0x06f000a0	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_3_
mask=0x0ff000f0	sig=0x06f000e0	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_3_
mask=0x0ff00170	sig=0x06f00160	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_3_
mask=0x0ff00270	sig=0x06f00260	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_3_
mask=0x0ff00470	sig=0x06f00460	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_3_
mask=0x0ff00870	sig=0x06f00860	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_3_
mask=0x0ff00ff0	sig=0x06f00040	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_asr_mem_mode_3_
mask=0x0ff00ff0	sig=0x06f00000	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_lsl_mem_mode_3_
mask=0x0ff00ff0	sig=0x06f00020	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_lsr_mem_mode_3_
mask=0x0ff00ff0	sig=0x06f00060	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_ror_mem_mode_3_
mask=0x0ff00010	sig=0x06f00010	prob=7.81243897e-06	name=DEC_fetch_oper_unknown
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_44, 16)
  _STUB_NAME(stub_44_0),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_44_2),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_44_4),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_44_6),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_3_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_3_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_3_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_3_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
_TABLE_DEF_END

/*
Table begin: index=112 size=26
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x07000010
Prob=0.00728619275	Entropy=3.00051117	HTreeHeight=3.12503418
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x070000c0	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_4_
mask=0x0ff00170	sig=0x07000140	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_4_
mask=0x0ff00270	sig=0x07000240	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_4_
mask=0x0ff00470	sig=0x07000440	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_4_
mask=0x0ff00870	sig=0x07000840	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_4_
mask=0x0ff000f0	sig=0x07000080	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_4_
mask=0x0ff00170	sig=0x07000100	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_4_
mask=0x0ff00270	sig=0x07000200	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_4_
mask=0x0ff00470	sig=0x07000400	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_4_
mask=0x0ff00870	sig=0x07000800	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_4_
mask=0x0ff00870	sig=0x07000820	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_4_
mask=0x0ff00470	sig=0x07000420	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_4_
mask=0x0ff00270	sig=0x07000220	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_4_
mask=0x0ff00170	sig=0x07000120	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_4_
mask=0x0ff000f0	sig=0x070000a0	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_4_
mask=0x0ff000f0	sig=0x070000e0	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_4_
mask=0x0ff00170	sig=0x07000160	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_4_
mask=0x0ff00270	sig=0x07000260	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_4_
mask=0x0ff00470	sig=0x07000460	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_4_
mask=0x0ff00870	sig=0x07000860	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_4_
mask=0x0ff00ff0	sig=0x07000040	prob=0.124995117	name=DEC_fetch_oper_store_reg_zero_shift_zero_asr_mem_mode_4_
mask=0x0ff00ff0	sig=0x07000000	prob=0.124995117	name=DEC_fetch_oper_store_reg_zero_shift_zero_lsl_mem_mode_4_
mask=0x0ff00ff0	sig=0x07000020	prob=0.124995117	name=DEC_fetch_oper_store_reg_zero_shift_zero_lsr_mem_mode_4_
mask=0x0ff00ff0	sig=0x07000060	prob=0.124995117	name=DEC_fetch_oper_store_reg_zero_shift_zero_ror_mem_mode_4_
mask=0x0ff00010	sig=0x07000010	prob=7.81219484e-06	name=DEC_fetch_oper_unknown
mask=0x0ff00010	sig=0x07000010	prob=3.12487793e-05	name=DEC_fetch_oper_unknown
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_45, 16)
  _STUB_NAME(stub_45_0),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_45_2),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_45_4),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_45_6),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_4_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_4_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_4_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_4_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
_TABLE_DEF_END

/*
Table begin: index=113 size=25
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x07100010
Prob=0.00728596506	Entropy=3.00012038	HTreeHeight=3.12500684
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x071000c0	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_4_
mask=0x0ff00170	sig=0x07100140	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_4_
mask=0x0ff00270	sig=0x07100240	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_4_
mask=0x0ff00470	sig=0x07100440	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_4_
mask=0x0ff00870	sig=0x07100840	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_4_
mask=0x0ff000f0	sig=0x07100080	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_4_
mask=0x0ff00170	sig=0x07100100	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_4_
mask=0x0ff00270	sig=0x07100200	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_4_
mask=0x0ff00470	sig=0x07100400	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_4_
mask=0x0ff00870	sig=0x07100800	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_4_
mask=0x0ff00870	sig=0x07100820	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_4_
mask=0x0ff00470	sig=0x07100420	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_4_
mask=0x0ff00270	sig=0x07100220	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_4_
mask=0x0ff00170	sig=0x07100120	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_4_
mask=0x0ff000f0	sig=0x071000a0	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_4_
mask=0x0ff000f0	sig=0x071000e0	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_4_
mask=0x0ff00170	sig=0x07100160	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_4_
mask=0x0ff00270	sig=0x07100260	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_4_
mask=0x0ff00470	sig=0x07100460	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_4_
mask=0x0ff00870	sig=0x07100860	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_4_
mask=0x0ff00ff0	sig=0x07100040	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_asr_mem_mode_4_
mask=0x0ff00ff0	sig=0x07100000	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_lsl_mem_mode_4_
mask=0x0ff00ff0	sig=0x07100020	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_lsr_mem_mode_4_
mask=0x0ff00ff0	sig=0x07100060	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_ror_mem_mode_4_
mask=0x0ff00010	sig=0x07100010	prob=7.81243897e-06	name=DEC_fetch_oper_unknown
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_46, 16)
  _STUB_NAME(stub_46_0),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_46_2),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_46_4),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_46_6),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_4_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_4_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_4_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_4_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
_TABLE_DEF_END

/*
Table begin: index=114 size=26
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x07200010
Prob=0.00728619275	Entropy=3.00051117	HTreeHeight=3.12503418
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x072000c0	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_5_
mask=0x0ff00170	sig=0x07200140	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_5_
mask=0x0ff00270	sig=0x07200240	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_5_
mask=0x0ff00470	sig=0x07200440	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_5_
mask=0x0ff00870	sig=0x07200840	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_5_
mask=0x0ff000f0	sig=0x07200080	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_5_
mask=0x0ff00170	sig=0x07200100	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_5_
mask=0x0ff00270	sig=0x07200200	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_5_
mask=0x0ff00470	sig=0x07200400	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_5_
mask=0x0ff00870	sig=0x07200800	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_5_
mask=0x0ff00870	sig=0x07200820	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_5_
mask=0x0ff00470	sig=0x07200420	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_5_
mask=0x0ff00270	sig=0x07200220	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_5_
mask=0x0ff00170	sig=0x07200120	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_5_
mask=0x0ff000f0	sig=0x072000a0	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_5_
mask=0x0ff000f0	sig=0x072000e0	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_5_
mask=0x0ff00170	sig=0x07200160	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_5_
mask=0x0ff00270	sig=0x07200260	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_5_
mask=0x0ff00470	sig=0x07200460	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_5_
mask=0x0ff00870	sig=0x07200860	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_5_
mask=0x0ff00ff0	sig=0x07200040	prob=0.124995117	name=DEC_fetch_oper_store_reg_zero_shift_zero_asr_mem_mode_5_
mask=0x0ff00ff0	sig=0x07200000	prob=0.124995117	name=DEC_fetch_oper_store_reg_zero_shift_zero_lsl_mem_mode_5_
mask=0x0ff00ff0	sig=0x07200020	prob=0.124995117	name=DEC_fetch_oper_store_reg_zero_shift_zero_lsr_mem_mode_5_
mask=0x0ff00ff0	sig=0x07200060	prob=0.124995117	name=DEC_fetch_oper_store_reg_zero_shift_zero_ror_mem_mode_5_
mask=0x0ff00010	sig=0x07200010	prob=7.81219484e-06	name=DEC_fetch_oper_unknown
mask=0x0ff00010	sig=0x07200010	prob=3.12487793e-05	name=DEC_fetch_oper_unknown
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_47, 16)
  _STUB_NAME(stub_47_0),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_47_2),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_47_4),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_47_6),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_5_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_5_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_5_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_5_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
_TABLE_DEF_END

/*
Table begin: index=115 size=25
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x07300010
Prob=0.00728596506	Entropy=3.00012038	HTreeHeight=3.12500684
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x073000c0	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_5_
mask=0x0ff00170	sig=0x07300140	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_5_
mask=0x0ff00270	sig=0x07300240	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_5_
mask=0x0ff00470	sig=0x07300440	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_5_
mask=0x0ff00870	sig=0x07300840	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_5_
mask=0x0ff000f0	sig=0x07300080	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_5_
mask=0x0ff00170	sig=0x07300100	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_5_
mask=0x0ff00270	sig=0x07300200	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_5_
mask=0x0ff00470	sig=0x07300400	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_5_
mask=0x0ff00870	sig=0x07300800	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_5_
mask=0x0ff00870	sig=0x07300820	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_5_
mask=0x0ff00470	sig=0x07300420	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_5_
mask=0x0ff00270	sig=0x07300220	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_5_
mask=0x0ff00170	sig=0x07300120	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_5_
mask=0x0ff000f0	sig=0x073000a0	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_5_
mask=0x0ff000f0	sig=0x073000e0	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_5_
mask=0x0ff00170	sig=0x07300160	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_5_
mask=0x0ff00270	sig=0x07300260	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_5_
mask=0x0ff00470	sig=0x07300460	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_5_
mask=0x0ff00870	sig=0x07300860	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_5_
mask=0x0ff00ff0	sig=0x07300040	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_asr_mem_mode_5_
mask=0x0ff00ff0	sig=0x07300000	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_lsl_mem_mode_5_
mask=0x0ff00ff0	sig=0x07300020	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_lsr_mem_mode_5_
mask=0x0ff00ff0	sig=0x07300060	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_ror_mem_mode_5_
mask=0x0ff00010	sig=0x07300010	prob=7.81243897e-06	name=DEC_fetch_oper_unknown
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_48, 16)
  _STUB_NAME(stub_48_0),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_48_2),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_48_4),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_48_6),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_5_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_5_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_5_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_5_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
_TABLE_DEF_END

/*
Table begin: index=116 size=26
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x07400010
Prob=0.00728619275	Entropy=3.00051117	HTreeHeight=3.12503418
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x074000c0	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_6_
mask=0x0ff00170	sig=0x07400140	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_6_
mask=0x0ff00270	sig=0x07400240	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_6_
mask=0x0ff00470	sig=0x07400440	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_6_
mask=0x0ff00870	sig=0x07400840	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_6_
mask=0x0ff000f0	sig=0x07400080	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_6_
mask=0x0ff00170	sig=0x07400100	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_6_
mask=0x0ff00270	sig=0x07400200	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_6_
mask=0x0ff00470	sig=0x07400400	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_6_
mask=0x0ff00870	sig=0x07400800	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_6_
mask=0x0ff00870	sig=0x07400820	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_6_
mask=0x0ff00470	sig=0x07400420	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_6_
mask=0x0ff00270	sig=0x07400220	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_6_
mask=0x0ff00170	sig=0x07400120	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_6_
mask=0x0ff000f0	sig=0x074000a0	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_6_
mask=0x0ff000f0	sig=0x074000e0	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_6_
mask=0x0ff00170	sig=0x07400160	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_6_
mask=0x0ff00270	sig=0x07400260	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_6_
mask=0x0ff00470	sig=0x07400460	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_6_
mask=0x0ff00870	sig=0x07400860	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_6_
mask=0x0ff00ff0	sig=0x07400040	prob=0.124995117	name=DEC_fetch_oper_store_reg_zero_shift_zero_asr_mem_mode_6_
mask=0x0ff00ff0	sig=0x07400000	prob=0.124995117	name=DEC_fetch_oper_store_reg_zero_shift_zero_lsl_mem_mode_6_
mask=0x0ff00ff0	sig=0x07400020	prob=0.124995117	name=DEC_fetch_oper_store_reg_zero_shift_zero_lsr_mem_mode_6_
mask=0x0ff00ff0	sig=0x07400060	prob=0.124995117	name=DEC_fetch_oper_store_reg_zero_shift_zero_ror_mem_mode_6_
mask=0x0ff00010	sig=0x07400010	prob=7.81219484e-06	name=DEC_fetch_oper_unknown
mask=0x0ff00010	sig=0x07400010	prob=3.12487793e-05	name=DEC_fetch_oper_unknown
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_49, 16)
  _STUB_NAME(stub_49_0),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_49_2),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_49_4),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_49_6),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_6_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_6_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_6_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_6_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
_TABLE_DEF_END

/*
Table begin: index=117 size=25
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x07500010
Prob=0.00728596506	Entropy=3.00012038	HTreeHeight=3.12500684
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x075000c0	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_6_
mask=0x0ff00170	sig=0x07500140	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_6_
mask=0x0ff00270	sig=0x07500240	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_6_
mask=0x0ff00470	sig=0x07500440	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_6_
mask=0x0ff00870	sig=0x07500840	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_6_
mask=0x0ff000f0	sig=0x07500080	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_6_
mask=0x0ff00170	sig=0x07500100	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_6_
mask=0x0ff00270	sig=0x07500200	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_6_
mask=0x0ff00470	sig=0x07500400	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_6_
mask=0x0ff00870	sig=0x07500800	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_6_
mask=0x0ff00870	sig=0x07500820	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_6_
mask=0x0ff00470	sig=0x07500420	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_6_
mask=0x0ff00270	sig=0x07500220	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_6_
mask=0x0ff00170	sig=0x07500120	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_6_
mask=0x0ff000f0	sig=0x075000a0	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_6_
mask=0x0ff000f0	sig=0x075000e0	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_6_
mask=0x0ff00170	sig=0x07500160	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_6_
mask=0x0ff00270	sig=0x07500260	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_6_
mask=0x0ff00470	sig=0x07500460	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_6_
mask=0x0ff00870	sig=0x07500860	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_6_
mask=0x0ff00ff0	sig=0x07500040	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_asr_mem_mode_6_
mask=0x0ff00ff0	sig=0x07500000	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_lsl_mem_mode_6_
mask=0x0ff00ff0	sig=0x07500020	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_lsr_mem_mode_6_
mask=0x0ff00ff0	sig=0x07500060	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_ror_mem_mode_6_
mask=0x0ff00010	sig=0x07500010	prob=7.81243897e-06	name=DEC_fetch_oper_unknown
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_50, 16)
  _STUB_NAME(stub_50_0),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_50_2),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_50_4),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_50_6),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_6_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_6_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_6_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_6_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
_TABLE_DEF_END

/*
Table begin: index=118 size=26
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x07600010
Prob=0.00728619275	Entropy=3.00051117	HTreeHeight=3.12503418
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x076000c0	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_7_
mask=0x0ff00170	sig=0x07600140	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_7_
mask=0x0ff00270	sig=0x07600240	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_7_
mask=0x0ff00470	sig=0x07600440	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_7_
mask=0x0ff00870	sig=0x07600840	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_7_
mask=0x0ff000f0	sig=0x07600080	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_7_
mask=0x0ff00170	sig=0x07600100	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_7_
mask=0x0ff00270	sig=0x07600200	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_7_
mask=0x0ff00470	sig=0x07600400	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_7_
mask=0x0ff00870	sig=0x07600800	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_7_
mask=0x0ff00870	sig=0x07600820	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_7_
mask=0x0ff00470	sig=0x07600420	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_7_
mask=0x0ff00270	sig=0x07600220	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_7_
mask=0x0ff00170	sig=0x07600120	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_7_
mask=0x0ff000f0	sig=0x076000a0	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_7_
mask=0x0ff000f0	sig=0x076000e0	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_7_
mask=0x0ff00170	sig=0x07600160	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_7_
mask=0x0ff00270	sig=0x07600260	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_7_
mask=0x0ff00470	sig=0x07600460	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_7_
mask=0x0ff00870	sig=0x07600860	prob=0.0249990235	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_7_
mask=0x0ff00ff0	sig=0x07600040	prob=0.124995117	name=DEC_fetch_oper_store_reg_zero_shift_zero_asr_mem_mode_7_
mask=0x0ff00ff0	sig=0x07600000	prob=0.124995117	name=DEC_fetch_oper_store_reg_zero_shift_zero_lsl_mem_mode_7_
mask=0x0ff00ff0	sig=0x07600020	prob=0.124995117	name=DEC_fetch_oper_store_reg_zero_shift_zero_lsr_mem_mode_7_
mask=0x0ff00ff0	sig=0x07600060	prob=0.124995117	name=DEC_fetch_oper_store_reg_zero_shift_zero_ror_mem_mode_7_
mask=0x0ff00010	sig=0x07600010	prob=7.81219484e-06	name=DEC_fetch_oper_unknown
mask=0x0ff00010	sig=0x07600010	prob=3.12487793e-05	name=DEC_fetch_oper_unknown
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_51, 16)
  _STUB_NAME(stub_51_0),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_51_2),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_51_4),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_51_6),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_7_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_7_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_7_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_7_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
_TABLE_DEF_END

/*
Table begin: index=119 size=25
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x07700010
Prob=0.00728596506	Entropy=3.00012038	HTreeHeight=3.12500684
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x077000c0	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_7_
mask=0x0ff00170	sig=0x07700140	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_7_
mask=0x0ff00270	sig=0x07700240	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_7_
mask=0x0ff00470	sig=0x07700440	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_7_
mask=0x0ff00870	sig=0x07700840	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_7_
mask=0x0ff000f0	sig=0x07700080	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_7_
mask=0x0ff00170	sig=0x07700100	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_7_
mask=0x0ff00270	sig=0x07700200	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_7_
mask=0x0ff00470	sig=0x07700400	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_7_
mask=0x0ff00870	sig=0x07700800	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_7_
mask=0x0ff00870	sig=0x07700820	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_7_
mask=0x0ff00470	sig=0x07700420	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_7_
mask=0x0ff00270	sig=0x07700220	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_7_
mask=0x0ff00170	sig=0x07700120	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_7_
mask=0x0ff000f0	sig=0x077000a0	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_7_
mask=0x0ff000f0	sig=0x077000e0	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_7_
mask=0x0ff00170	sig=0x07700160	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_7_
mask=0x0ff00270	sig=0x07700260	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_7_
mask=0x0ff00470	sig=0x07700460	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_7_
mask=0x0ff00870	sig=0x07700860	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_7_
mask=0x0ff00ff0	sig=0x07700040	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_asr_mem_mode_7_
mask=0x0ff00ff0	sig=0x07700000	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_lsl_mem_mode_7_
mask=0x0ff00ff0	sig=0x07700020	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_lsr_mem_mode_7_
mask=0x0ff00ff0	sig=0x07700060	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_ror_mem_mode_7_
mask=0x0ff00010	sig=0x07700010	prob=7.81243897e-06	name=DEC_fetch_oper_unknown
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_52, 16)
  _STUB_NAME(stub_52_0),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_52_2),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_52_4),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_52_6),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_7_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_7_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_7_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_7_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
_TABLE_DEF_END

/*
Table begin: index=120 size=25
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x07800010
Prob=0.00728596506	Entropy=3.00012038	HTreeHeight=3.12500684
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x078000c0	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_4_
mask=0x0ff00170	sig=0x07800140	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_4_
mask=0x0ff00270	sig=0x07800240	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_4_
mask=0x0ff00470	sig=0x07800440	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_4_
mask=0x0ff00870	sig=0x07800840	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_4_
mask=0x0ff000f0	sig=0x07800080	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_4_
mask=0x0ff00170	sig=0x07800100	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_4_
mask=0x0ff00270	sig=0x07800200	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_4_
mask=0x0ff00470	sig=0x07800400	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_4_
mask=0x0ff00870	sig=0x07800800	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_4_
mask=0x0ff00870	sig=0x07800820	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_4_
mask=0x0ff00470	sig=0x07800420	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_4_
mask=0x0ff00270	sig=0x07800220	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_4_
mask=0x0ff00170	sig=0x07800120	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_4_
mask=0x0ff000f0	sig=0x078000a0	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_4_
mask=0x0ff000f0	sig=0x078000e0	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_4_
mask=0x0ff00170	sig=0x07800160	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_4_
mask=0x0ff00270	sig=0x07800260	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_4_
mask=0x0ff00470	sig=0x07800460	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_4_
mask=0x0ff00870	sig=0x07800860	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_4_
mask=0x0ff00ff0	sig=0x07800040	prob=0.124999023	name=DEC_fetch_oper_store_reg_zero_shift_zero_asr_mem_mode_4_
mask=0x0ff00ff0	sig=0x07800000	prob=0.124999023	name=DEC_fetch_oper_store_reg_zero_shift_zero_lsl_mem_mode_4_
mask=0x0ff00ff0	sig=0x07800020	prob=0.124999023	name=DEC_fetch_oper_store_reg_zero_shift_zero_lsr_mem_mode_4_
mask=0x0ff00ff0	sig=0x07800060	prob=0.124999023	name=DEC_fetch_oper_store_reg_zero_shift_zero_ror_mem_mode_4_
mask=0x0ff00010	sig=0x07800010	prob=7.81243897e-06	name=DEC_fetch_oper_unknown
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_53, 16)
  _STUB_NAME(stub_53_0),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_53_2),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_53_4),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_53_6),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_4_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_4_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_4_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_4_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
_TABLE_DEF_END

/*
Table begin: index=121 size=25
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x07900010
Prob=0.00728596506	Entropy=3.00012038	HTreeHeight=3.12500684
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x079000c0	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_4_
mask=0x0ff00170	sig=0x07900140	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_4_
mask=0x0ff00270	sig=0x07900240	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_4_
mask=0x0ff00470	sig=0x07900440	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_4_
mask=0x0ff00870	sig=0x07900840	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_4_
mask=0x0ff000f0	sig=0x07900080	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_4_
mask=0x0ff00170	sig=0x07900100	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_4_
mask=0x0ff00270	sig=0x07900200	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_4_
mask=0x0ff00470	sig=0x07900400	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_4_
mask=0x0ff00870	sig=0x07900800	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_4_
mask=0x0ff00870	sig=0x07900820	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_4_
mask=0x0ff00470	sig=0x07900420	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_4_
mask=0x0ff00270	sig=0x07900220	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_4_
mask=0x0ff00170	sig=0x07900120	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_4_
mask=0x0ff000f0	sig=0x079000a0	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_4_
mask=0x0ff000f0	sig=0x079000e0	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_4_
mask=0x0ff00170	sig=0x07900160	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_4_
mask=0x0ff00270	sig=0x07900260	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_4_
mask=0x0ff00470	sig=0x07900460	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_4_
mask=0x0ff00870	sig=0x07900860	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_4_
mask=0x0ff00ff0	sig=0x07900040	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_asr_mem_mode_4_
mask=0x0ff00ff0	sig=0x07900000	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_lsl_mem_mode_4_
mask=0x0ff00ff0	sig=0x07900020	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_lsr_mem_mode_4_
mask=0x0ff00ff0	sig=0x07900060	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_ror_mem_mode_4_
mask=0x0ff00010	sig=0x07900010	prob=7.81243897e-06	name=DEC_fetch_oper_unknown
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_54, 16)
  _STUB_NAME(stub_54_0),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_54_2),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_54_4),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_54_6),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_4_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_4_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_4_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_4_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
_TABLE_DEF_END

/*
Table begin: index=122 size=25
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x07a00010
Prob=0.00728596506	Entropy=3.00012038	HTreeHeight=3.12500684
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x07a000c0	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_5_
mask=0x0ff00170	sig=0x07a00140	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_5_
mask=0x0ff00270	sig=0x07a00240	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_5_
mask=0x0ff00470	sig=0x07a00440	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_5_
mask=0x0ff00870	sig=0x07a00840	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_5_
mask=0x0ff000f0	sig=0x07a00080	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_5_
mask=0x0ff00170	sig=0x07a00100	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_5_
mask=0x0ff00270	sig=0x07a00200	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_5_
mask=0x0ff00470	sig=0x07a00400	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_5_
mask=0x0ff00870	sig=0x07a00800	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_5_
mask=0x0ff00870	sig=0x07a00820	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_5_
mask=0x0ff00470	sig=0x07a00420	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_5_
mask=0x0ff00270	sig=0x07a00220	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_5_
mask=0x0ff00170	sig=0x07a00120	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_5_
mask=0x0ff000f0	sig=0x07a000a0	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_5_
mask=0x0ff000f0	sig=0x07a000e0	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_5_
mask=0x0ff00170	sig=0x07a00160	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_5_
mask=0x0ff00270	sig=0x07a00260	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_5_
mask=0x0ff00470	sig=0x07a00460	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_5_
mask=0x0ff00870	sig=0x07a00860	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_5_
mask=0x0ff00ff0	sig=0x07a00040	prob=0.124999023	name=DEC_fetch_oper_store_reg_zero_shift_zero_asr_mem_mode_5_
mask=0x0ff00ff0	sig=0x07a00000	prob=0.124999023	name=DEC_fetch_oper_store_reg_zero_shift_zero_lsl_mem_mode_5_
mask=0x0ff00ff0	sig=0x07a00020	prob=0.124999023	name=DEC_fetch_oper_store_reg_zero_shift_zero_lsr_mem_mode_5_
mask=0x0ff00ff0	sig=0x07a00060	prob=0.124999023	name=DEC_fetch_oper_store_reg_zero_shift_zero_ror_mem_mode_5_
mask=0x0ff00010	sig=0x07a00010	prob=7.81243897e-06	name=DEC_fetch_oper_unknown
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_55, 16)
  _STUB_NAME(stub_55_0),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_55_2),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_55_4),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_55_6),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_5_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_5_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_5_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_5_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
_TABLE_DEF_END

/*
Table begin: index=123 size=25
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x07b00010
Prob=0.00728596506	Entropy=3.00012038	HTreeHeight=3.12500684
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x07b000c0	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_5_
mask=0x0ff00170	sig=0x07b00140	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_5_
mask=0x0ff00270	sig=0x07b00240	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_5_
mask=0x0ff00470	sig=0x07b00440	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_5_
mask=0x0ff00870	sig=0x07b00840	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_5_
mask=0x0ff000f0	sig=0x07b00080	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_5_
mask=0x0ff00170	sig=0x07b00100	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_5_
mask=0x0ff00270	sig=0x07b00200	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_5_
mask=0x0ff00470	sig=0x07b00400	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_5_
mask=0x0ff00870	sig=0x07b00800	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_5_
mask=0x0ff00870	sig=0x07b00820	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_5_
mask=0x0ff00470	sig=0x07b00420	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_5_
mask=0x0ff00270	sig=0x07b00220	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_5_
mask=0x0ff00170	sig=0x07b00120	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_5_
mask=0x0ff000f0	sig=0x07b000a0	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_5_
mask=0x0ff000f0	sig=0x07b000e0	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_5_
mask=0x0ff00170	sig=0x07b00160	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_5_
mask=0x0ff00270	sig=0x07b00260	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_5_
mask=0x0ff00470	sig=0x07b00460	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_5_
mask=0x0ff00870	sig=0x07b00860	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_5_
mask=0x0ff00ff0	sig=0x07b00040	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_asr_mem_mode_5_
mask=0x0ff00ff0	sig=0x07b00000	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_lsl_mem_mode_5_
mask=0x0ff00ff0	sig=0x07b00020	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_lsr_mem_mode_5_
mask=0x0ff00ff0	sig=0x07b00060	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_ror_mem_mode_5_
mask=0x0ff00010	sig=0x07b00010	prob=7.81243897e-06	name=DEC_fetch_oper_unknown
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_56, 16)
  _STUB_NAME(stub_56_0),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_56_2),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_56_4),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_56_6),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_5_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_5_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_5_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_5_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
_TABLE_DEF_END

/*
Table begin: index=124 size=25
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x07c00010
Prob=0.00728596506	Entropy=3.00012038	HTreeHeight=3.12500684
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x07c000c0	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_6_
mask=0x0ff00170	sig=0x07c00140	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_6_
mask=0x0ff00270	sig=0x07c00240	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_6_
mask=0x0ff00470	sig=0x07c00440	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_6_
mask=0x0ff00870	sig=0x07c00840	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_6_
mask=0x0ff000f0	sig=0x07c00080	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_6_
mask=0x0ff00170	sig=0x07c00100	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_6_
mask=0x0ff00270	sig=0x07c00200	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_6_
mask=0x0ff00470	sig=0x07c00400	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_6_
mask=0x0ff00870	sig=0x07c00800	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_6_
mask=0x0ff00870	sig=0x07c00820	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_6_
mask=0x0ff00470	sig=0x07c00420	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_6_
mask=0x0ff00270	sig=0x07c00220	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_6_
mask=0x0ff00170	sig=0x07c00120	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_6_
mask=0x0ff000f0	sig=0x07c000a0	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_6_
mask=0x0ff000f0	sig=0x07c000e0	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_6_
mask=0x0ff00170	sig=0x07c00160	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_6_
mask=0x0ff00270	sig=0x07c00260	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_6_
mask=0x0ff00470	sig=0x07c00460	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_6_
mask=0x0ff00870	sig=0x07c00860	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_6_
mask=0x0ff00ff0	sig=0x07c00040	prob=0.124999023	name=DEC_fetch_oper_store_reg_zero_shift_zero_asr_mem_mode_6_
mask=0x0ff00ff0	sig=0x07c00000	prob=0.124999023	name=DEC_fetch_oper_store_reg_zero_shift_zero_lsl_mem_mode_6_
mask=0x0ff00ff0	sig=0x07c00020	prob=0.124999023	name=DEC_fetch_oper_store_reg_zero_shift_zero_lsr_mem_mode_6_
mask=0x0ff00ff0	sig=0x07c00060	prob=0.124999023	name=DEC_fetch_oper_store_reg_zero_shift_zero_ror_mem_mode_6_
mask=0x0ff00010	sig=0x07c00010	prob=7.81243897e-06	name=DEC_fetch_oper_unknown
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_57, 16)
  _STUB_NAME(stub_57_0),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_57_2),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_57_4),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_57_6),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_6_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_6_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_6_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_6_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
_TABLE_DEF_END

/*
Table begin: index=125 size=25
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x07d00010
Prob=0.00728596506	Entropy=3.00012038	HTreeHeight=3.12500684
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x07d000c0	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_6_
mask=0x0ff00170	sig=0x07d00140	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_6_
mask=0x0ff00270	sig=0x07d00240	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_6_
mask=0x0ff00470	sig=0x07d00440	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_6_
mask=0x0ff00870	sig=0x07d00840	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_6_
mask=0x0ff000f0	sig=0x07d00080	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_6_
mask=0x0ff00170	sig=0x07d00100	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_6_
mask=0x0ff00270	sig=0x07d00200	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_6_
mask=0x0ff00470	sig=0x07d00400	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_6_
mask=0x0ff00870	sig=0x07d00800	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_6_
mask=0x0ff00870	sig=0x07d00820	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_6_
mask=0x0ff00470	sig=0x07d00420	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_6_
mask=0x0ff00270	sig=0x07d00220	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_6_
mask=0x0ff00170	sig=0x07d00120	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_6_
mask=0x0ff000f0	sig=0x07d000a0	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_6_
mask=0x0ff000f0	sig=0x07d000e0	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_6_
mask=0x0ff00170	sig=0x07d00160	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_6_
mask=0x0ff00270	sig=0x07d00260	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_6_
mask=0x0ff00470	sig=0x07d00460	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_6_
mask=0x0ff00870	sig=0x07d00860	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_6_
mask=0x0ff00ff0	sig=0x07d00040	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_asr_mem_mode_6_
mask=0x0ff00ff0	sig=0x07d00000	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_lsl_mem_mode_6_
mask=0x0ff00ff0	sig=0x07d00020	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_lsr_mem_mode_6_
mask=0x0ff00ff0	sig=0x07d00060	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_ror_mem_mode_6_
mask=0x0ff00010	sig=0x07d00010	prob=7.81243897e-06	name=DEC_fetch_oper_unknown
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_58, 16)
  _STUB_NAME(stub_58_0),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_58_2),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_58_4),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_58_6),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_6_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_6_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_6_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_6_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
_TABLE_DEF_END

/*
Table begin: index=126 size=25
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x07e00010
Prob=0.00728596506	Entropy=3.00012038	HTreeHeight=3.12500684
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x07e000c0	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_7_
mask=0x0ff00170	sig=0x07e00140	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_7_
mask=0x0ff00270	sig=0x07e00240	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_7_
mask=0x0ff00470	sig=0x07e00440	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_7_
mask=0x0ff00870	sig=0x07e00840	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_7_
mask=0x0ff000f0	sig=0x07e00080	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_7_
mask=0x0ff00170	sig=0x07e00100	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_7_
mask=0x0ff00270	sig=0x07e00200	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_7_
mask=0x0ff00470	sig=0x07e00400	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_7_
mask=0x0ff00870	sig=0x07e00800	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_7_
mask=0x0ff00870	sig=0x07e00820	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_7_
mask=0x0ff00470	sig=0x07e00420	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_7_
mask=0x0ff00270	sig=0x07e00220	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_7_
mask=0x0ff00170	sig=0x07e00120	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_7_
mask=0x0ff000f0	sig=0x07e000a0	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_7_
mask=0x0ff000f0	sig=0x07e000e0	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_7_
mask=0x0ff00170	sig=0x07e00160	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_7_
mask=0x0ff00270	sig=0x07e00260	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_7_
mask=0x0ff00470	sig=0x07e00460	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_7_
mask=0x0ff00870	sig=0x07e00860	prob=0.0249998047	name=DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_7_
mask=0x0ff00ff0	sig=0x07e00040	prob=0.124999023	name=DEC_fetch_oper_store_reg_zero_shift_zero_asr_mem_mode_7_
mask=0x0ff00ff0	sig=0x07e00000	prob=0.124999023	name=DEC_fetch_oper_store_reg_zero_shift_zero_lsl_mem_mode_7_
mask=0x0ff00ff0	sig=0x07e00020	prob=0.124999023	name=DEC_fetch_oper_store_reg_zero_shift_zero_lsr_mem_mode_7_
mask=0x0ff00ff0	sig=0x07e00060	prob=0.124999023	name=DEC_fetch_oper_store_reg_zero_shift_zero_ror_mem_mode_7_
mask=0x0ff00010	sig=0x07e00010	prob=7.81243897e-06	name=DEC_fetch_oper_unknown
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_59, 16)
  _STUB_NAME(stub_59_0),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_59_2),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_59_4),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_59_6),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_7_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_7_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_7_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_7_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
_TABLE_DEF_END

/*
Table begin: index=127 size=25
Mask=0x00000ff0	DMask=0x0ff00000	CMask=0x00000010	Sig=0x07f00010
Prob=0.00728596506	Entropy=3.00012038	HTreeHeight=3.12500684
-------------------------------------------------------------------
mask=0x0ff000f0	sig=0x07f000c0	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_7_
mask=0x0ff00170	sig=0x07f00140	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_7_
mask=0x0ff00270	sig=0x07f00240	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_7_
mask=0x0ff00470	sig=0x07f00440	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_7_
mask=0x0ff00870	sig=0x07f00840	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_7_
mask=0x0ff000f0	sig=0x07f00080	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_7_
mask=0x0ff00170	sig=0x07f00100	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_7_
mask=0x0ff00270	sig=0x07f00200	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_7_
mask=0x0ff00470	sig=0x07f00400	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_7_
mask=0x0ff00870	sig=0x07f00800	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_7_
mask=0x0ff00870	sig=0x07f00820	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_7_
mask=0x0ff00470	sig=0x07f00420	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_7_
mask=0x0ff00270	sig=0x07f00220	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_7_
mask=0x0ff00170	sig=0x07f00120	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_7_
mask=0x0ff000f0	sig=0x07f000a0	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_7_
mask=0x0ff000f0	sig=0x07f000e0	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_7_
mask=0x0ff00170	sig=0x07f00160	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_7_
mask=0x0ff00270	sig=0x07f00260	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_7_
mask=0x0ff00470	sig=0x07f00460	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_7_
mask=0x0ff00870	sig=0x07f00860	prob=0.0249998047	name=DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_7_
mask=0x0ff00ff0	sig=0x07f00040	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_asr_mem_mode_7_
mask=0x0ff00ff0	sig=0x07f00000	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_lsl_mem_mode_7_
mask=0x0ff00ff0	sig=0x07f00020	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_lsr_mem_mode_7_
mask=0x0ff00ff0	sig=0x07f00060	prob=0.124999023	name=DEC_fetch_oper_load_reg_zero_shift_zero_ror_mem_mode_7_
mask=0x0ff00010	sig=0x07f00010	prob=7.81243897e-06	name=DEC_fetch_oper_unknown
-------------------------------------------------------------------
Table Mask=0x000000f0*/

_TABLE_DEF_BEGIN(table_60, 16)
  _STUB_NAME(stub_60_0),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_60_2),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_60_4),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _STUB_NAME(stub_60_6),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_7_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_7_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_7_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
  _FUNC_NAME(DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_7_),
  _FUNC_NAME(DEC_fetch_oper_unknown),
_TABLE_DEF_END


_STUB_ENTRY(stub_0_0)
{
 _TABLE_JUMP(table_1, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_1)
{
 _TABLE_JUMP(table_2, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_2)
{
 _TABLE_JUMP(table_3, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_3)
{
 _TABLE_JUMP(table_4, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_4)
{
 _TABLE_JUMP(table_5, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_5)
{
 _TABLE_JUMP(table_6, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_6)
{
 _TABLE_JUMP(table_7, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_7)
{
 _TABLE_JUMP(table_8, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_8)
{
 _TABLE_JUMP(table_9, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_9)
{
 _TABLE_JUMP(table_10, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_10)
{
 _TABLE_JUMP(table_11, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_11)
{
 _TABLE_JUMP(table_12, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_12)
{
 _TABLE_JUMP(table_13, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_13)
{
 _TABLE_JUMP(table_14, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_14)
{
 _TABLE_JUMP(table_15, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_15)
{
 _TABLE_JUMP(table_16, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_16)
{
 if _PATTERN_TRUE(0x00000f60, 0x00000000) {
   if _PATTERN_TRUE(0x00000090, 0x00000090) {
     _FUNC_CALL(DEC_fetch_oper_swap_);
   } else {
     _FUNC_CALL(DEC_fetch_oper_unknown);
   }
 } else {
   if _PATTERN_TRUE(0x00000090, 0x00000090) {
     _FUNC_CALL(DEC_fetch_oper_store_ext_reg_mem_mode_4_);
   } else {
     _FUNC_CALL(DEC_fetch_oper_unknown);
   }
 }
}

_STUB_ENTRY(stub_0_17)
{
 _TABLE_JUMP(table_17, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_18)
{
 if _PATTERN_TRUE(0x00000090, 0x00000090) {
   _FUNC_CALL(DEC_fetch_oper_store_ext_reg_mem_mode_5_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_unknown);
 }
}

_STUB_ENTRY(stub_0_19)
{
 _TABLE_JUMP(table_18, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_20)
{
 if _PATTERN_TRUE(0x00000090, 0x00000090) {
   _FUNC_CALL(DEC_fetch_oper_store_ext_imm_mem_mode_6_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_unknown);
 }
}

_STUB_ENTRY(stub_0_21)
{
 _TABLE_JUMP(table_19, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_22)
{
 if _PATTERN_TRUE(0x00000090, 0x00000090) {
   _FUNC_CALL(DEC_fetch_oper_store_ext_imm_mem_mode_7_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_unknown);
 }
}

_STUB_ENTRY(stub_0_23)
{
 _TABLE_JUMP(table_20, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_24)
{
 _TABLE_JUMP(table_21, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_25)
{
 _TABLE_JUMP(table_22, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_26)
{
 _TABLE_JUMP(table_23, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_27)
{
 _TABLE_JUMP(table_24, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_28)
{
 _TABLE_JUMP(table_25, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_29)
{
 _TABLE_JUMP(table_26, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_30)
{
 _TABLE_JUMP(table_27, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_31)
{
 _TABLE_JUMP(table_28, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_96)
{
 _TABLE_JUMP(table_29, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_97)
{
 _TABLE_JUMP(table_30, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_98)
{
 _TABLE_JUMP(table_31, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_99)
{
 _TABLE_JUMP(table_32, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_100)
{
 _TABLE_JUMP(table_33, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_101)
{
 _TABLE_JUMP(table_34, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_102)
{
 _TABLE_JUMP(table_35, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_103)
{
 _TABLE_JUMP(table_36, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_104)
{
 _TABLE_JUMP(table_37, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_105)
{
 _TABLE_JUMP(table_38, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_106)
{
 _TABLE_JUMP(table_39, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_107)
{
 _TABLE_JUMP(table_40, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_108)
{
 _TABLE_JUMP(table_41, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_109)
{
 _TABLE_JUMP(table_42, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_110)
{
 _TABLE_JUMP(table_43, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_111)
{
 _TABLE_JUMP(table_44, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_112)
{
 _TABLE_JUMP(table_45, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_113)
{
 _TABLE_JUMP(table_46, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_114)
{
 _TABLE_JUMP(table_47, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_115)
{
 _TABLE_JUMP(table_48, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_116)
{
 _TABLE_JUMP(table_49, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_117)
{
 _TABLE_JUMP(table_50, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_118)
{
 _TABLE_JUMP(table_51, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_119)
{
 _TABLE_JUMP(table_52, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_120)
{
 _TABLE_JUMP(table_53, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_121)
{
 _TABLE_JUMP(table_54, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_122)
{
 _TABLE_JUMP(table_55, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_123)
{
 _TABLE_JUMP(table_56, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_124)
{
 _TABLE_JUMP(table_57, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_125)
{
 _TABLE_JUMP(table_58, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_126)
{
 _TABLE_JUMP(table_59, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_127)
{
 _TABLE_JUMP(table_60, 0x0000000f, 4);
}

_STUB_ENTRY(stub_0_208)
{
 if _PATTERN_TRUE(0x00000e00, 0x00000e00) {
   _FUNC_CALL(DEC_fetch_oper_coproc_st_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_fpe_);
 }
}

_STUB_ENTRY(stub_0_209)
{
 if _PATTERN_TRUE(0x00000e00, 0x00000e00) {
   _FUNC_CALL(DEC_fetch_oper_coproc_ld_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_fpe_);
 }
}

_STUB_ENTRY(stub_0_210)
{
 if _PATTERN_TRUE(0x00000e00, 0x00000e00) {
   _FUNC_CALL(DEC_fetch_oper_coproc_st_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_fpe_);
 }
}

_STUB_ENTRY(stub_0_211)
{
 if _PATTERN_TRUE(0x00000e00, 0x00000e00) {
   _FUNC_CALL(DEC_fetch_oper_coproc_ld_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_fpe_);
 }
}

_STUB_ENTRY(stub_0_212)
{
 if _PATTERN_TRUE(0x00000e00, 0x00000e00) {
   _FUNC_CALL(DEC_fetch_oper_coproc_st_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_fpe_);
 }
}

_STUB_ENTRY(stub_0_213)
{
 if _PATTERN_TRUE(0x00000e00, 0x00000e00) {
   _FUNC_CALL(DEC_fetch_oper_coproc_ld_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_fpe_);
 }
}

_STUB_ENTRY(stub_0_214)
{
 if _PATTERN_TRUE(0x00000e00, 0x00000e00) {
   _FUNC_CALL(DEC_fetch_oper_coproc_st_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_fpe_);
 }
}

_STUB_ENTRY(stub_0_215)
{
 if _PATTERN_TRUE(0x00000e00, 0x00000e00) {
   _FUNC_CALL(DEC_fetch_oper_coproc_ld_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_fpe_);
 }
}

_STUB_ENTRY(stub_0_216)
{
 if _PATTERN_TRUE(0x00000e00, 0x00000e00) {
   _FUNC_CALL(DEC_fetch_oper_coproc_st_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_fpe_);
 }
}

_STUB_ENTRY(stub_0_217)
{
 if _PATTERN_TRUE(0x00000e00, 0x00000e00) {
   _FUNC_CALL(DEC_fetch_oper_coproc_ld_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_fpe_);
 }
}

_STUB_ENTRY(stub_0_218)
{
 if _PATTERN_TRUE(0x00000e00, 0x00000e00) {
   _FUNC_CALL(DEC_fetch_oper_coproc_st_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_fpe_);
 }
}

_STUB_ENTRY(stub_0_219)
{
 if _PATTERN_TRUE(0x00000e00, 0x00000e00) {
   _FUNC_CALL(DEC_fetch_oper_coproc_ld_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_fpe_);
 }
}

_STUB_ENTRY(stub_0_220)
{
 if _PATTERN_TRUE(0x00000e00, 0x00000e00) {
   _FUNC_CALL(DEC_fetch_oper_coproc_st_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_fpe_);
 }
}

_STUB_ENTRY(stub_0_221)
{
 if _PATTERN_TRUE(0x00000e00, 0x00000e00) {
   _FUNC_CALL(DEC_fetch_oper_coproc_ld_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_fpe_);
 }
}

_STUB_ENTRY(stub_0_222)
{
 if _PATTERN_TRUE(0x00000e00, 0x00000e00) {
   _FUNC_CALL(DEC_fetch_oper_coproc_st_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_fpe_);
 }
}

_STUB_ENTRY(stub_0_223)
{
 if _PATTERN_TRUE(0x00000e00, 0x00000e00) {
   _FUNC_CALL(DEC_fetch_oper_coproc_ld_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_fpe_);
 }
}

_STUB_ENTRY(stub_1_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_and_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_and_);
 }
}

_STUB_ENTRY(stub_1_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_and_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_and_);
 }
}

_STUB_ENTRY(stub_1_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_and_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_and_);
 }
}

_STUB_ENTRY(stub_1_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_and_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_and_);
 }
}

_STUB_ENTRY(stub_2_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_ands_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_ands_);
 }
}

_STUB_ENTRY(stub_2_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_ands_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_ands_);
 }
}

_STUB_ENTRY(stub_2_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_ands_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_ands_);
 }
}

_STUB_ENTRY(stub_2_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_ands_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_ands_);
 }
}

_STUB_ENTRY(stub_3_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_eor_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_eor_);
 }
}

_STUB_ENTRY(stub_3_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_eor_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_eor_);
 }
}

_STUB_ENTRY(stub_3_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_eor_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_eor_);
 }
}

_STUB_ENTRY(stub_3_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_eor_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_eor_);
 }
}

_STUB_ENTRY(stub_4_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_eors_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_eors_);
 }
}

_STUB_ENTRY(stub_4_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_eors_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_eors_);
 }
}

_STUB_ENTRY(stub_4_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_eors_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_eors_);
 }
}

_STUB_ENTRY(stub_4_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_eors_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_eors_);
 }
}

_STUB_ENTRY(stub_5_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_sub_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_sub_);
 }
}

_STUB_ENTRY(stub_5_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_sub_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_sub_);
 }
}

_STUB_ENTRY(stub_5_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_sub_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_sub_);
 }
}

_STUB_ENTRY(stub_5_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_sub_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_sub_);
 }
}

_STUB_ENTRY(stub_6_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_subs_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_subs_);
 }
}

_STUB_ENTRY(stub_6_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_subs_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_subs_);
 }
}

_STUB_ENTRY(stub_6_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_subs_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_subs_);
 }
}

_STUB_ENTRY(stub_6_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_subs_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_subs_);
 }
}

_STUB_ENTRY(stub_7_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_rsb_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_rsb_);
 }
}

_STUB_ENTRY(stub_7_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_rsb_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_rsb_);
 }
}

_STUB_ENTRY(stub_7_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_rsb_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_rsb_);
 }
}

_STUB_ENTRY(stub_7_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_rsb_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_rsb_);
 }
}

_STUB_ENTRY(stub_8_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_rsbs_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_rsbs_);
 }
}

_STUB_ENTRY(stub_8_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_rsbs_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_rsbs_);
 }
}

_STUB_ENTRY(stub_8_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_rsbs_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_rsbs_);
 }
}

_STUB_ENTRY(stub_8_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_rsbs_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_rsbs_);
 }
}

_STUB_ENTRY(stub_9_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_add_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_add_);
 }
}

_STUB_ENTRY(stub_9_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_add_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_add_);
 }
}

_STUB_ENTRY(stub_9_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_add_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_add_);
 }
}

_STUB_ENTRY(stub_9_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_add_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_add_);
 }
}

_STUB_ENTRY(stub_10_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_adds_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_adds_);
 }
}

_STUB_ENTRY(stub_10_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_adds_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_adds_);
 }
}

_STUB_ENTRY(stub_10_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_adds_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_adds_);
 }
}

_STUB_ENTRY(stub_10_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_adds_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_adds_);
 }
}

_STUB_ENTRY(stub_11_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_adc_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_adc_);
 }
}

_STUB_ENTRY(stub_11_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_adc_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_adc_);
 }
}

_STUB_ENTRY(stub_11_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_adc_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_adc_);
 }
}

_STUB_ENTRY(stub_11_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_adc_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_adc_);
 }
}

_STUB_ENTRY(stub_12_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_adcs_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_adcs_);
 }
}

_STUB_ENTRY(stub_12_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_adcs_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_adcs_);
 }
}

_STUB_ENTRY(stub_12_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_adcs_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_adcs_);
 }
}

_STUB_ENTRY(stub_12_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_adcs_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_adcs_);
 }
}

_STUB_ENTRY(stub_13_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_sbc_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_sbc_);
 }
}

_STUB_ENTRY(stub_13_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_sbc_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_sbc_);
 }
}

_STUB_ENTRY(stub_13_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_sbc_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_sbc_);
 }
}

_STUB_ENTRY(stub_13_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_sbc_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_sbc_);
 }
}

_STUB_ENTRY(stub_14_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_sbcs_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_sbcs_);
 }
}

_STUB_ENTRY(stub_14_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_sbcs_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_sbcs_);
 }
}

_STUB_ENTRY(stub_14_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_sbcs_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_sbcs_);
 }
}

_STUB_ENTRY(stub_14_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_sbcs_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_sbcs_);
 }
}

_STUB_ENTRY(stub_15_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_rsc_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_rsc_);
 }
}

_STUB_ENTRY(stub_15_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_rsc_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_rsc_);
 }
}

_STUB_ENTRY(stub_15_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_rsc_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_rsc_);
 }
}

_STUB_ENTRY(stub_15_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_rsc_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_rsc_);
 }
}

_STUB_ENTRY(stub_16_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_rscs_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_rscs_);
 }
}

_STUB_ENTRY(stub_16_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_rscs_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_rscs_);
 }
}

_STUB_ENTRY(stub_16_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_rscs_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_rscs_);
 }
}

_STUB_ENTRY(stub_16_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_rscs_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_rscs_);
 }
}

_STUB_ENTRY(stub_17_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_tst_zero_shift_zero_lsl_tst_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsl_tst_);
 }
}

_STUB_ENTRY(stub_17_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_tst_zero_shift_zero_lsr_tst_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsr_tst_);
 }
}

_STUB_ENTRY(stub_17_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_tst_zero_shift_zero_asr_tst_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_asr_tst_);
 }
}

_STUB_ENTRY(stub_17_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_tst_zero_shift_zero_ror_tst_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_ror_tst_);
 }
}

_STUB_ENTRY(stub_18_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_tst_zero_shift_zero_lsl_teq_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsl_teq_);
 }
}

_STUB_ENTRY(stub_18_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_tst_zero_shift_zero_lsr_teq_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsr_teq_);
 }
}

_STUB_ENTRY(stub_18_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_tst_zero_shift_zero_asr_teq_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_asr_teq_);
 }
}

_STUB_ENTRY(stub_18_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_tst_zero_shift_zero_ror_teq_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_ror_teq_);
 }
}

_STUB_ENTRY(stub_19_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_tst_zero_shift_zero_lsl_cmp_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsl_cmp_);
 }
}

_STUB_ENTRY(stub_19_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_tst_zero_shift_zero_lsr_cmp_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsr_cmp_);
 }
}

_STUB_ENTRY(stub_19_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_tst_zero_shift_zero_asr_cmp_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_asr_cmp_);
 }
}

_STUB_ENTRY(stub_19_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_tst_zero_shift_zero_ror_cmp_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_ror_cmp_);
 }
}

_STUB_ENTRY(stub_20_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_tst_zero_shift_zero_lsl_cmn_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsl_cmn_);
 }
}

_STUB_ENTRY(stub_20_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_tst_zero_shift_zero_lsr_cmn_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_lsr_cmn_);
 }
}

_STUB_ENTRY(stub_20_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_tst_zero_shift_zero_asr_cmn_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_asr_cmn_);
 }
}

_STUB_ENTRY(stub_20_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_tst_zero_shift_zero_ror_cmn_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_tst_imm_shift_i_ror_cmn_);
 }
}

_STUB_ENTRY(stub_21_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_orr_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_orr_);
 }
}

_STUB_ENTRY(stub_21_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_orr_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_orr_);
 }
}

_STUB_ENTRY(stub_21_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_orr_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_orr_);
 }
}

_STUB_ENTRY(stub_21_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_orr_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_orr_);
 }
}

_STUB_ENTRY(stub_22_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_orrs_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_orrs_);
 }
}

_STUB_ENTRY(stub_22_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_orrs_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_orrs_);
 }
}

_STUB_ENTRY(stub_22_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_orrs_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_orrs_);
 }
}

_STUB_ENTRY(stub_22_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_orrs_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_orrs_);
 }
}

_STUB_ENTRY(stub_23_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_unop_zero_shift_zero_lsl_mov_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsl_mov_);
 }
}

_STUB_ENTRY(stub_23_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_unop_zero_shift_zero_lsr_mov_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsr_mov_);
 }
}

_STUB_ENTRY(stub_23_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_unop_zero_shift_zero_asr_mov_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_asr_mov_);
 }
}

_STUB_ENTRY(stub_23_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_unop_zero_shift_zero_ror_mov_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_ror_mov_);
 }
}

_STUB_ENTRY(stub_24_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_unop_zero_shift_zero_lsl_movs_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsl_movs_);
 }
}

_STUB_ENTRY(stub_24_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_unop_zero_shift_zero_lsr_movs_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsr_movs_);
 }
}

_STUB_ENTRY(stub_24_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_unop_zero_shift_zero_asr_movs_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_asr_movs_);
 }
}

_STUB_ENTRY(stub_24_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_unop_zero_shift_zero_ror_movs_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_ror_movs_);
 }
}

_STUB_ENTRY(stub_25_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_bic_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_bic_);
 }
}

_STUB_ENTRY(stub_25_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_bic_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_bic_);
 }
}

_STUB_ENTRY(stub_25_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_bic_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_bic_);
 }
}

_STUB_ENTRY(stub_25_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_bic_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_bic_);
 }
}

_STUB_ENTRY(stub_26_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsl_bics_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsl_bics_);
 }
}

_STUB_ENTRY(stub_26_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_lsr_bics_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_lsr_bics_);
 }
}

_STUB_ENTRY(stub_26_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_asr_bics_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_asr_bics_);
 }
}

_STUB_ENTRY(stub_26_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_zero_shift_zero_ror_bics_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_binop_imm_shift_i_ror_bics_);
 }
}

_STUB_ENTRY(stub_27_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_unop_zero_shift_zero_lsl_mvn_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsl_mvn_);
 }
}

_STUB_ENTRY(stub_27_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_unop_zero_shift_zero_lsr_mvn_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsr_mvn_);
 }
}

_STUB_ENTRY(stub_27_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_unop_zero_shift_zero_asr_mvn_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_asr_mvn_);
 }
}

_STUB_ENTRY(stub_27_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_unop_zero_shift_zero_ror_mvn_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_ror_mvn_);
 }
}

_STUB_ENTRY(stub_28_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_unop_zero_shift_zero_lsl_mvns_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsl_mvns_);
 }
}

_STUB_ENTRY(stub_28_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_unop_zero_shift_zero_lsr_mvns_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_lsr_mvns_);
 }
}

_STUB_ENTRY(stub_28_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_unop_zero_shift_zero_asr_mvns_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_asr_mvns_);
 }
}

_STUB_ENTRY(stub_28_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_unop_zero_shift_zero_ror_mvns_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_dpi_ishift_unop_imm_shift_i_ror_mvns_);
 }
}

_STUB_ENTRY(stub_29_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_lsl_mem_mode_0_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_0_);
 }
}

_STUB_ENTRY(stub_29_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_lsr_mem_mode_0_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_0_);
 }
}

_STUB_ENTRY(stub_29_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_asr_mem_mode_0_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_0_);
 }
}

_STUB_ENTRY(stub_29_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_ror_mem_mode_0_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_0_);
 }
}

_STUB_ENTRY(stub_30_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_lsl_mem_mode_0_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_0_);
 }
}

_STUB_ENTRY(stub_30_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_lsr_mem_mode_0_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_0_);
 }
}

_STUB_ENTRY(stub_30_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_asr_mem_mode_0_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_0_);
 }
}

_STUB_ENTRY(stub_30_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_ror_mem_mode_0_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_0_);
 }
}

_STUB_ENTRY(stub_31_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_lsl_mem_mode_1_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_1_);
 }
}

_STUB_ENTRY(stub_31_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_lsr_mem_mode_1_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_1_);
 }
}

_STUB_ENTRY(stub_31_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_asr_mem_mode_1_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_1_);
 }
}

_STUB_ENTRY(stub_31_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_ror_mem_mode_1_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_1_);
 }
}

_STUB_ENTRY(stub_32_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_lsl_mem_mode_1_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_1_);
 }
}

_STUB_ENTRY(stub_32_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_lsr_mem_mode_1_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_1_);
 }
}

_STUB_ENTRY(stub_32_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_asr_mem_mode_1_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_1_);
 }
}

_STUB_ENTRY(stub_32_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_ror_mem_mode_1_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_1_);
 }
}

_STUB_ENTRY(stub_33_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_lsl_mem_mode_2_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_2_);
 }
}

_STUB_ENTRY(stub_33_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_lsr_mem_mode_2_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_2_);
 }
}

_STUB_ENTRY(stub_33_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_asr_mem_mode_2_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_2_);
 }
}

_STUB_ENTRY(stub_33_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_ror_mem_mode_2_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_2_);
 }
}

_STUB_ENTRY(stub_34_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_lsl_mem_mode_2_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_2_);
 }
}

_STUB_ENTRY(stub_34_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_lsr_mem_mode_2_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_2_);
 }
}

_STUB_ENTRY(stub_34_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_asr_mem_mode_2_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_2_);
 }
}

_STUB_ENTRY(stub_34_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_ror_mem_mode_2_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_2_);
 }
}

_STUB_ENTRY(stub_35_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_lsl_mem_mode_3_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_3_);
 }
}

_STUB_ENTRY(stub_35_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_lsr_mem_mode_3_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_3_);
 }
}

_STUB_ENTRY(stub_35_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_asr_mem_mode_3_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_3_);
 }
}

_STUB_ENTRY(stub_35_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_ror_mem_mode_3_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_3_);
 }
}

_STUB_ENTRY(stub_36_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_lsl_mem_mode_3_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_3_);
 }
}

_STUB_ENTRY(stub_36_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_lsr_mem_mode_3_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_3_);
 }
}

_STUB_ENTRY(stub_36_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_asr_mem_mode_3_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_3_);
 }
}

_STUB_ENTRY(stub_36_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_ror_mem_mode_3_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_3_);
 }
}

_STUB_ENTRY(stub_37_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_lsl_mem_mode_0_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_0_);
 }
}

_STUB_ENTRY(stub_37_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_lsr_mem_mode_0_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_0_);
 }
}

_STUB_ENTRY(stub_37_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_asr_mem_mode_0_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_0_);
 }
}

_STUB_ENTRY(stub_37_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_ror_mem_mode_0_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_0_);
 }
}

_STUB_ENTRY(stub_38_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_lsl_mem_mode_0_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_0_);
 }
}

_STUB_ENTRY(stub_38_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_lsr_mem_mode_0_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_0_);
 }
}

_STUB_ENTRY(stub_38_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_asr_mem_mode_0_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_0_);
 }
}

_STUB_ENTRY(stub_38_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_ror_mem_mode_0_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_0_);
 }
}

_STUB_ENTRY(stub_39_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_lsl_mem_mode_1_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_1_);
 }
}

_STUB_ENTRY(stub_39_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_lsr_mem_mode_1_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_1_);
 }
}

_STUB_ENTRY(stub_39_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_asr_mem_mode_1_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_1_);
 }
}

_STUB_ENTRY(stub_39_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_ror_mem_mode_1_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_1_);
 }
}

_STUB_ENTRY(stub_40_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_lsl_mem_mode_1_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_1_);
 }
}

_STUB_ENTRY(stub_40_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_lsr_mem_mode_1_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_1_);
 }
}

_STUB_ENTRY(stub_40_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_asr_mem_mode_1_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_1_);
 }
}

_STUB_ENTRY(stub_40_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_ror_mem_mode_1_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_1_);
 }
}

_STUB_ENTRY(stub_41_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_lsl_mem_mode_2_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_2_);
 }
}

_STUB_ENTRY(stub_41_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_lsr_mem_mode_2_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_2_);
 }
}

_STUB_ENTRY(stub_41_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_asr_mem_mode_2_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_2_);
 }
}

_STUB_ENTRY(stub_41_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_ror_mem_mode_2_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_2_);
 }
}

_STUB_ENTRY(stub_42_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_lsl_mem_mode_2_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_2_);
 }
}

_STUB_ENTRY(stub_42_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_lsr_mem_mode_2_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_2_);
 }
}

_STUB_ENTRY(stub_42_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_asr_mem_mode_2_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_2_);
 }
}

_STUB_ENTRY(stub_42_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_ror_mem_mode_2_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_2_);
 }
}

_STUB_ENTRY(stub_43_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_lsl_mem_mode_3_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_3_);
 }
}

_STUB_ENTRY(stub_43_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_lsr_mem_mode_3_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_3_);
 }
}

_STUB_ENTRY(stub_43_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_asr_mem_mode_3_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_3_);
 }
}

_STUB_ENTRY(stub_43_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_ror_mem_mode_3_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_3_);
 }
}

_STUB_ENTRY(stub_44_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_lsl_mem_mode_3_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_3_);
 }
}

_STUB_ENTRY(stub_44_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_lsr_mem_mode_3_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_3_);
 }
}

_STUB_ENTRY(stub_44_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_asr_mem_mode_3_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_3_);
 }
}

_STUB_ENTRY(stub_44_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_ror_mem_mode_3_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_3_);
 }
}

_STUB_ENTRY(stub_45_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_lsl_mem_mode_4_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_4_);
 }
}

_STUB_ENTRY(stub_45_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_lsr_mem_mode_4_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_4_);
 }
}

_STUB_ENTRY(stub_45_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_asr_mem_mode_4_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_4_);
 }
}

_STUB_ENTRY(stub_45_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_ror_mem_mode_4_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_4_);
 }
}

_STUB_ENTRY(stub_46_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_lsl_mem_mode_4_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_4_);
 }
}

_STUB_ENTRY(stub_46_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_lsr_mem_mode_4_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_4_);
 }
}

_STUB_ENTRY(stub_46_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_asr_mem_mode_4_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_4_);
 }
}

_STUB_ENTRY(stub_46_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_ror_mem_mode_4_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_4_);
 }
}

_STUB_ENTRY(stub_47_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_lsl_mem_mode_5_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_5_);
 }
}

_STUB_ENTRY(stub_47_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_lsr_mem_mode_5_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_5_);
 }
}

_STUB_ENTRY(stub_47_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_asr_mem_mode_5_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_5_);
 }
}

_STUB_ENTRY(stub_47_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_ror_mem_mode_5_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_5_);
 }
}

_STUB_ENTRY(stub_48_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_lsl_mem_mode_5_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_5_);
 }
}

_STUB_ENTRY(stub_48_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_lsr_mem_mode_5_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_5_);
 }
}

_STUB_ENTRY(stub_48_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_asr_mem_mode_5_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_5_);
 }
}

_STUB_ENTRY(stub_48_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_ror_mem_mode_5_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_5_);
 }
}

_STUB_ENTRY(stub_49_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_lsl_mem_mode_6_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_6_);
 }
}

_STUB_ENTRY(stub_49_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_lsr_mem_mode_6_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_6_);
 }
}

_STUB_ENTRY(stub_49_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_asr_mem_mode_6_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_6_);
 }
}

_STUB_ENTRY(stub_49_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_ror_mem_mode_6_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_6_);
 }
}

_STUB_ENTRY(stub_50_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_lsl_mem_mode_6_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_6_);
 }
}

_STUB_ENTRY(stub_50_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_lsr_mem_mode_6_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_6_);
 }
}

_STUB_ENTRY(stub_50_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_asr_mem_mode_6_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_6_);
 }
}

_STUB_ENTRY(stub_50_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_ror_mem_mode_6_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_6_);
 }
}

_STUB_ENTRY(stub_51_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_lsl_mem_mode_7_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_7_);
 }
}

_STUB_ENTRY(stub_51_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_lsr_mem_mode_7_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_7_);
 }
}

_STUB_ENTRY(stub_51_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_asr_mem_mode_7_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_7_);
 }
}

_STUB_ENTRY(stub_51_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_ror_mem_mode_7_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_7_);
 }
}

_STUB_ENTRY(stub_52_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_lsl_mem_mode_7_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_7_);
 }
}

_STUB_ENTRY(stub_52_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_lsr_mem_mode_7_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_7_);
 }
}

_STUB_ENTRY(stub_52_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_asr_mem_mode_7_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_7_);
 }
}

_STUB_ENTRY(stub_52_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_ror_mem_mode_7_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_7_);
 }
}

_STUB_ENTRY(stub_53_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_lsl_mem_mode_4_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_4_);
 }
}

_STUB_ENTRY(stub_53_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_lsr_mem_mode_4_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_4_);
 }
}

_STUB_ENTRY(stub_53_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_asr_mem_mode_4_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_4_);
 }
}

_STUB_ENTRY(stub_53_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_ror_mem_mode_4_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_4_);
 }
}

_STUB_ENTRY(stub_54_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_lsl_mem_mode_4_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_4_);
 }
}

_STUB_ENTRY(stub_54_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_lsr_mem_mode_4_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_4_);
 }
}

_STUB_ENTRY(stub_54_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_asr_mem_mode_4_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_4_);
 }
}

_STUB_ENTRY(stub_54_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_ror_mem_mode_4_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_4_);
 }
}

_STUB_ENTRY(stub_55_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_lsl_mem_mode_5_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_5_);
 }
}

_STUB_ENTRY(stub_55_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_lsr_mem_mode_5_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_5_);
 }
}

_STUB_ENTRY(stub_55_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_asr_mem_mode_5_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_5_);
 }
}

_STUB_ENTRY(stub_55_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_ror_mem_mode_5_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_5_);
 }
}

_STUB_ENTRY(stub_56_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_lsl_mem_mode_5_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_5_);
 }
}

_STUB_ENTRY(stub_56_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_lsr_mem_mode_5_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_5_);
 }
}

_STUB_ENTRY(stub_56_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_asr_mem_mode_5_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_5_);
 }
}

_STUB_ENTRY(stub_56_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_ror_mem_mode_5_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_5_);
 }
}

_STUB_ENTRY(stub_57_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_lsl_mem_mode_6_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_6_);
 }
}

_STUB_ENTRY(stub_57_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_lsr_mem_mode_6_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_6_);
 }
}

_STUB_ENTRY(stub_57_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_asr_mem_mode_6_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_6_);
 }
}

_STUB_ENTRY(stub_57_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_ror_mem_mode_6_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_6_);
 }
}

_STUB_ENTRY(stub_58_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_lsl_mem_mode_6_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_6_);
 }
}

_STUB_ENTRY(stub_58_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_lsr_mem_mode_6_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_6_);
 }
}

_STUB_ENTRY(stub_58_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_asr_mem_mode_6_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_6_);
 }
}

_STUB_ENTRY(stub_58_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_ror_mem_mode_6_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_6_);
 }
}

_STUB_ENTRY(stub_59_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_lsl_mem_mode_7_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_lsl_mem_mode_7_);
 }
}

_STUB_ENTRY(stub_59_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_lsr_mem_mode_7_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_lsr_mem_mode_7_);
 }
}

_STUB_ENTRY(stub_59_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_asr_mem_mode_7_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_asr_mem_mode_7_);
 }
}

_STUB_ENTRY(stub_59_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_store_reg_zero_shift_zero_ror_mem_mode_7_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_store_reg_imm_shift_i_ror_mem_mode_7_);
 }
}

_STUB_ENTRY(stub_60_0)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_lsl_mem_mode_7_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_lsl_mem_mode_7_);
 }
}

_STUB_ENTRY(stub_60_2)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_lsr_mem_mode_7_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_lsr_mem_mode_7_);
 }
}

_STUB_ENTRY(stub_60_4)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_asr_mem_mode_7_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_asr_mem_mode_7_);
 }
}

_STUB_ENTRY(stub_60_6)
{
 if _PATTERN_TRUE(0x00000f00, 0x00000000) {
   _FUNC_CALL(DEC_fetch_oper_load_reg_zero_shift_zero_ror_mem_mode_7_);
 } else {
   _FUNC_CALL(DEC_fetch_oper_load_reg_imm_shift_i_ror_mem_mode_7_);
 }
}


_MAIN_DECODE_ENTRY
{
 _TABLE_JUMP(table_0, 0x000000ff, 20);
}
/* End of decoder */
