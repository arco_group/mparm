#!/usr/bin/env python
# **********************************************************************
#
# Copyright (c) 2003-2006 ZeroC, Inc. All rights reserved.
#
# This copy of Ice-E is licensed to you under the terms described in the
# ICEE_LICENSE file included in this distribution.
#
# **********************************************************************

import os, sys
import getopt

for toplevel in [".", "..", "../..", "../../..", "../../../.."]:
    toplevel = os.path.normpath(toplevel)
    if os.path.exists(os.path.join(toplevel, "config", "TestUtil.py")):
        break
else:
    raise "can't find toplevel directory!"

sys.path.append(os.path.join(toplevel, "config"))
import TestUtil

def runTests(tests, num = 0):

    #
    # Run each of the tests.
    #
    for i in tests:

	i = os.path.normpath(i)
	dir = os.path.join(toplevel, "test", i)

	print
	if(num > 0):
	    print "[" + str(num) + "]",
	print "*** running tests in " + dir,
	print

        if TestUtil.isWin9x():
	    status = os.system("python " + os.path.join(dir, "run.py"))
        else:
            status = os.system(os.path.join(dir, "run.py"))

	if status:
	    if(num > 0):
		print "[" + str(num) + "]",
	    print "test in " + dir + " failed with exit status", status,
	    sys.exit(status)

#
# List of all basic tests.
#
tests = [
    "IceE/thread", 
    "IceE/uuid", 
    "IceE/operations", 
    "IceE/exceptions", 
    "IceE/inheritance", 
    "IceE/facets", 
    "IceE/faultTolerance", 
    "IceE/location", 
    "IceE/adapterDeactivation", 
    "IceE/slicing", 
    "IceE/custom", 
    "IceE/retry", 
    ]

#
# These tests are currently disabled on cygwin
#
if TestUtil.isCygwin() == 0:
    tests += [ \
       
      ]

def usage():
    print "usage: " + sys.argv[0] + " [-l]"
    sys.exit(2)

try:
    opts, args = getopt.getopt(sys.argv[1:], "l")
except getopt.GetoptError:
    usage()

if(args):
    usage()

loop = 0
for o, a in opts:
    if o == "-l":
        loop = 1
    
if loop:
    num = 1
    while 1:
	runTests(tests, num)
	num += 1
else:
    runTests(tests)
