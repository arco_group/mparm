// **********************************************************************
//
// Copyright (c) 2003-2006 ZeroC, Inc. All rights reserved.
//
// This copy of Ice-E is licensed to you under the terms described in the
// ICEE_LICENSE file included in this distribution.
//
// **********************************************************************

#ifndef STATIC_MUTEX_TEST_H
#define STATIC_MUTEX_TEST_H

#include <TestBase.h>

class StaticMutexTest : public TestBase
{
public:

    StaticMutexTest();

private:
    
    virtual void run();
};

#endif
