// **********************************************************************
//
// Copyright (c) 2003-2006 ZeroC, Inc. All rights reserved.
//
// This copy of Ice-E is licensed to you under the terms described in the
// ICEE_LICENSE file included in this distribution.
//
// **********************************************************************

#ifndef ICE_FACET_MAP_ICE
#define ICE_FACET_MAP_ICE

module Ice
{

/**
 *
 * A mapping from facet name to servant.
 *
 **/
local dictionary<string, Object> FacetMap;

};

#endif
