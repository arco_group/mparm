// **********************************************************************
//
// Copyright (c) 2003-2006 ZeroC, Inc. All rights reserved.
//
// This copy of Ice-E is licensed to you under the terms described in the
// ICEE_LICENSE file included in this distribution.
//
// **********************************************************************

#ifndef ICE_LOGGER_F_ICE
#define ICE_LOGGER_F_ICE

module Ice
{

local interface Logger;

};

#endif
