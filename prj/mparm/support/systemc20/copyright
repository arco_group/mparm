This file has been generated with ps2ascii from the License.pdf file
also included in this package.  Please, refer to the original file for
an authoritative reference.

SYSTEMC OPEN SOURCE LICENSE(SOFTWARE DOWNLOAD AND USE LICENSE
AGREEMENT VERSION 2.3) PLEASE READ THIS LICENSE AGREEMENT CAREFULLY
BEFORE CLICKING ONTHE "ACCEPT" BUTTON, AS BY CLICKING ON THE "ACCEPT"
BUTTON YOU ACKNOWLEDGE THAT YOU HAVE READ, UNDERSTOOD AND AGREE TO
BEBOUND BY THIS LICENSE AGREEMENT AND ALL OF ITS TERMS AND
CONDITIONS. OPEN SYSTEMC INITIATIVE The purpose of the following
license agreement (the "Agreement") is to encourageinteroperability
and distributed development of a C++ modeling language known as
"SystemC" for system simulation and design (the "Purpose").  The
SystemC softwarelicensed hereunder is licensed, without fee of any
kind, for use pursuant to the terms and conditions set forth in this
Agreement. LICENSE AGREEMENT THE CONTRIBUTORS ARE WILLING TO LICENSE
THE PROGRAM TO YOU ONLYON THE CONDITION THAT YOU ACCEPT ALL OF THE
TERMS OF THIS LICENSE AGREEMENT.  IF YOU DO NOT AGREE TO ALL OF THE
TERMS OF THIS LICENSEAGREEMENT, NO RIGHTS ARE GRANTED TO YOU HEREUNDER
TO USE THE PROGRAM.  ANY USE, REPRODUCTION OR DISTRIBUTION OF THE
PROGRAMCONSTITUTES YOUR ACCEPTANCE OF THIS AGREEMENT.

1.  DEFINITIONS.

1.1  "Agreement" means this document. 

1.2 "Contribution" means: (a) the Original Program; and (b) all
Modificationsthat Recipient deposits or contributes in accordance with
Section 3 in furtherance of the Purpose of this Agreement but does not
include any software that hasbeen previously so deposited or
contributed.

1.3 "Contribution Questionnaire" means the questionnaire attached
hereto asExhibit C.

1.4 "Contributor" means any Recipient, including Synopsys,
Inc. ("Synopsys"),that makes a Contribution pursuant to Section 3.
Any Recipient depositing, as part or all of a Contribution, code which
has previously been so deposited byanother Recipient is not the
Contributor of such re-deposited code for the purposes of this
Agreement.  A list of the Contributors is attached hereto asExhibit A.

1.5 "Contributor's Necessary Patent Claim" means a claim in any patent
nowor hereafter owned or licensable by Contributor that is directly
infringed solely by the portion of an executing computer program
translated, compiled or interpreted from and corresponding directly
and solely to the Contribution disclosed byContributor hereunder and
the SystemC Kernel Code, except that Contributor's Necessary Patent
Claim shall not include any claim directed towards a datastructure,
method, algorithm, process, technique, circuit representation, or
circuit implementation that is not completely and entirely described
in the combinationof such Contribution and the SystemC Kernel Code.

1.6 "Copyright/Trade Secret Rights" means worldwide statutory and
commonlaw rights associated solely with (i) works of authorship
including copyrights, copyright applications, copyright registrations
and "moral rights", and (ii) theprotection of trade and industrial
secrets and confidential information. Patents are not included in
Copyright/Trade Secret Rights. 

1.7 "Distribute" means making a Distribution. 

1.8 "Distribution" means any distribution, sublicensing or other
transfer of theProgram (with or without Modifications) to any third
party.

1.9 "Executable" means Original Program (with or without
Modifications)compiled into object code form along with only those
header files from such Original Program that are strictly necessary to
make use of the object code.

1.10 "Marks" means, collectively, the registered and unregistered
marks andlogos that OSCI has licensed or otherwise authorized
Recipient to use.  All marks and logos are listed on Exhibit D, which
list may be amended from time to timeby OSCI to add or delete any
marks or logos.

1.11 "Modification" means: (a) any software code which comprises
change(s)to the Program including additions and/or deletions; (b) any
specifications for the Program; and (c) any reference implementation
of the Program. 

1.12 "Original Program" means the SystemC 1.1 version of the
softwareaccompanying this Agreement as released by Synopsys.

1.13 "OSCI" means Open SystemC Initiative, a California nonprofit
mutualbenefit corporation. 

1.14 "Program" means the Original Program and each other Contribution
andany combination thereof. 

1.15 "Recipient" means anyone who receives the Program under
thisAgreement, including all Contributors. 

1.16 For legal entities, "Recipient" includes any entity that
controls, is controlledby, or is under common control with Recipient.
For purposes of this definition, "control" means (a) the power, direct
or indirect, to cause the direction ormanagement of such entity,
whether by contract or otherwise, or (b) ownership of fifty percent
(50%) or more of the outstanding shares or beneficial ownership ofsuch
entity. 

1.17 "Source Code" means human readable text in an electronic form
suitablefor modification that describe the functions and data
structures, including C, C++, and other language modules, plus any
associated interface definition files, scriptsused to control
compilation and installation of an Executable, or a list of source
code differential comparisons against the Original Program. 1.18
"SystemC Kernel Code" means the set of compilable source and
headerfiles included in the Original Program that are necessary to
build the target SystemC library object module, but does not include
operating system headerfiles, operating system library elements,
documentation, example code, sample code fragments, or other ancillary
information.

2.  GRANT OF RIGHTS

2.1 Subject to the terms of this Agreement, each Contributor hereby
grantsRecipient a non-exclusive, worldwide, royalty-free license under
Contributor's Copyright/Trade Secrets to do the following:

(a) use, reproduce, prepare derivative works of, publicly
display,publicly perform and Distribute in source code and object code
form the Contribution of such Contributor and any such derivative
works, subject tothe terms and conditions of this Agreement including
Section 4; and

(b) use the know-how, information and knowledge embedded in
theContribution, without any obligation to keep the foregoing
confidential so long as Recipient does not otherwise violate this
Agreement. 

2.2 Contributor grants to each Recipient, a world-wide, royalty-free,
non-exclusive license under Contributor's Necessary Patent Claims to
make, use, sell, offer for sale, or import the such Contributor's
Contribution and the Programonly to the minimum extent necessary to
exercise the rights granted in Section 2.1(a). 

2.3 Each Contributor represents that to its knowledge it has
sufficient rights inits Contribution, if any, to grant the licenses
set forth in Sections 2.1 and 2.2.

2.4 Except as expressly stated in Sections 2.1 and 2.2, Recipient
receives norights or licenses to the intellectual property of any
Contributor under this Agreement, whether expressly, by implication,
estoppel or otherwise.  All rights inthe Program not expressly granted
under this Agreement are reserved.

2.5 Recipient shall retain its entire right, title, and interest in
and toContributions disclosed by Recipient hereunder, including all
Copyrights/Trade Secret Rights and patent rights embodied therein,
subject to the underlying rightsembodied in the Original Program and
further subject to those rights expressly granted in this Agreement.
Recipient agrees that Recipient shall not remove oralter any
proprietary notices contained in the Contributions licensed to
Recipient hereunder and shall reproduce and include such notices on
any copies of theContributions made by Recipient in any media.

2.6  License to Marks.

(a) OSCI shall retain all right, title and interest in and to the
Marksworldwide, subject to the limited license granted to Recipient in
this Section 2.6.  OSCI hereby grants Recipient a non-exclusive,
royalty-free,limited license to use the Marks solely in connection
with its exercise of the rights granted pursuant to this Agreement and
to indicate that theproducts being marketed by Recipient are
compatible with, and meet the standards of, the SystemC modeling
language.  All uses of the Marks shallbe in accordance with OSCI's
trademark usage policy set forth in Exhibit D. 

(b) Recipient shall assist OSCI to the extent reasonably necessary
toprotect and maintain the Marks worldwide, including, but not limited
to, giving prompt notice to OSCI of any known or potential
infringement of theMarks, and cooperating with OSCI in preparing and
executing any documents necessary to register the Marks, or as may be
required by thelaws or rules of any country or jurisdiction.  In its
sole discretion, OSCI may commence, prosecute or defend any action or
claim concerning theMarks.  OSCI shall have the right to control any
such litigation, and Recipient shall fully cooperate with OSCI in any
such litigation.  OSCI shallreimburse Recipient for the reasonable
costs associated with providing such assistance, except to the extent
that such costs result fromRecipient's breach of this Section 2.6.
Recipient shall not commence any action regarding the Marks without
OSCI's prior written consent. 

(c) All goodwill with respect to the Marks shall accrue for the
solebenefit of OSCI.  Recipient shall maintain the quality of any
products, associated packaging, collateral and marketing materials on
which it usesany of the Marks in a manner consistent with all terms,
conditions and requirements set forth in this Section 2.6 and at a
level that meets orexceeds Recipient's overall reputation for quality
and that is at least commensurate with industry standards. 2.7
RECIPIENT UNDERSTANDS THAT ALTHOUGH EACH CONTRIBUTORGRANTS THE
LICENSES TO ITS CONTRIBUTIONS SET FORTH HEREIN, NO ASSURANCES ARE
PROVIDED BY ANY CONTRIBUTOR THAT THECONTRIBUTION ALONE OR IN
COMBINATION WITH THE PROGRAM DOES NOT INFRINGE THE PATENT OR OTHER
INTELLECTUAL PROPERTYRIGHTS OF ANY OTHER ENTITY.  EACH CONTRIBUTOR
DISCLAIMS ANY LIABILITY TO RECIPIENT FOR CLAIMS BROUGHT BY ANY OTHER
ENTITYBASED ON INFRINGEMENT OF INTELLECTUAL PROPERTY RIGHTS OR
OTHERWISE.  In addition, as a condition to exercising the rights and
licensesgranted hereunder, each Recipient hereby assumes sole
responsibility to secure any other intellectual property rights
needed, if any.  For example, if a third partypatent license is
required to allow Recipient to Distribute the Program, it is
Recipient's responsibility to acquire that license before Distributing
the Program. 

3.  DESCRIPTION AND DEPOSIT OF CONTRIBUTIONS

3.1 To the extent Recipient wishes to become a Contributor by making
aContribution, such Contributor shall:

(a) (i) deposit such Contribution in Source Code form
athttp://www.systemc.org/ according to the Contribution instructions
found at such site, or (ii) disclose such Contribution at a meeting of
any workinggroup of OSCI;

(b) (i) describe such Contribution in reasonable detail on Exhibit
B(including the additions or changes such Contributor made to create
the Contribution and the date of any such changes or additions),
(ii)completing a Contribution Questionnaire with respect to such
Contribution, and (iii) delivering both documents to OSCI.  All
Contributions made afterthe date hereof shall be effectuated by
Contributor (x) amending Exhibit B and delivering such amended Exhibit
B to OSCI, which amended exhibitshall automatically replace the
existing Exhibit B, (y) competing a Contribution Questionnaire with
respect to such Contribution, and (z)delivering both documents to
OSCI;

(c) cause such Contribution to contain a file documenting
suchContributor's name and contact information, additions or changes
such Contributor made to create the Contribution, and the date of any
suchchanges or additions;

(d) cause such Contribution to include in each file a
prominentstatement substantially similar to the following: "The
following code is derived, directly or indirectly, from the SystemC
source code Copyright (c)1996-{date here} by all Contributors.  All
Rights reserved.  The contents of this file are subject to the
restrictions and limitations set forth in theSystemC Open Source
License Version 2.3 (the "License").  You may not use this file except
in compliance with such restrictions and limitations.You may obtain
instructions on how to receive a copy of the License at
http://www.systemc.org/.  Software distributed by Contributors under
theLicense is distributed exclusively on an "AS IS" basis, WITHOUT
WARRANTY OF ANY KIND, either express or implied.  See the Licensefor
the specific language governing rights and limitations under the
License."; and

(e) cause such Contribution to include a text file titled "LEGAL"
whichdescribes any intellectual property rights of which Contributor
is aware that it or any third party may hold in the functionality or
code of suchContribution.

3.2 OSCI or the administrators of the http://www.systemc.org/ web site
shallhave the right to authorize removal from such site any
Contribution at any time. 

4.  REQUIREMENTS OF DISTRIBUTION

4.1 A Recipient may choose to Distribute the Program in object code
form under its own license agreement, provided that:

(a) Recipient complies with the terms and conditions of this Agreement;
and 

(b) the terms and conditions of Recipient's license agreement with
itslicensees:

i.  effectively disclaim on behalf of all Contributors all warranties
and conditions, express and implied, including warranties or
conditions of title and non-infringement, and implied warranties or
conditions of merchantability and fitness for a particular purpose;

ii.  effectively exclude on behalf of all Contributors all liability
fordamages, including, but not limited to, direct, indirect, special,
incidental and consequential damages, such as lost profits; 

iii.  state that any provisions which differ from this Agreement are
offered by that Recipient alone and not by any other party; and

iv.  state that source code for the Program is available from such
Recipient, and inform licensees how to obtain it in a reasonable
manner on or through a medium customarily used for software exchange.

4.2 If a Recipient chooses to Distribute the Program in source code
form then:

(a) the Program must be Distributed under this Agreement; and

(b) a copy of this Agreement must be included with each copy of
the Program.

4.3 Each Recipient must include the following in a conspicuous
location in the Program so Distributed or transferred:

Copyright (c) 1996-{date here}, by all Contributors.  All
rightsreserved. 4.4 In addition, each Recipient that creates and
Distributes or otherwisetransfers a Modification whether or not such
Modification has been deposited pursuant to Section 3 must identify
the originator of such Modification in a manner that reasonably allows
third parties to identify the originator of the Modification. 

5.  INDEMNIFICATION 

A Recipient who Distributes the Program (a "Distributor") may accept
certain responsibilities with respect to end users, business partners
and the like.  While this license is intended to facilitate the
commercial use of the Program, a Distributor shall Distribute the
Program in a manner which does not create potential liability for
Contributors.  Therefore each Distributor hereby agrees to defend and
indemnify everyContributor ("Indemnified Contributor") against any
losses, damages and costs (collectively "Losses") arising from claims,
lawsuits and other legal actions brought by athird party against the
Indemnified Contributor to the extent caused by the acts or omissions
of such Distributor, including but not limited to the terms and
conditions underwhich Distributor offered the Program, in connection
with its Distribution of the Program. The obligations in this section
do not apply to any claims or Losses relating to anyactual or alleged
intellectual property infringement of the Program.  In order to
qualify, an Indemnified Contributor must: (a) promptly notify the
Distributor in writing of suchclaim, and (b) allow the Distributor to
control, and cooperate with the Distributor in, the defense and any
related settlement negotiations.  The Indemnified Contributor
may participate in the defense of any such claim at its own expense.

For example, a Recipient might include the Program in a commercial
product offering, Product X.  That Recipient is then a Distributor.  If
that Distributor then makes performance claims, or offers warranties,
support, or indemnity or any other licenseterms related to Product X,
those performance claims, offers and other terms are such
Distributor's responsibility alone.  Under this section, the
Distributor would have todefend claims against the Contributors
related to those performance claims, offers, and other terms, and if a
court requires any Contributor to pay any damages as a result,
the Distributor must pay those damages.

6.  NO WARRANTY 

EXCEPT AS EXPRESSLY SET FORTH IN THIS AGREEMENT, THE PROGRAM
ISPROVIDED EXCLUSIVELY ON AN "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED INCLUDING,
WITHOUTLIMITATION, ANY WARRANTIES OR CONDITIONS OF TITLE,
NON-INFRINGEMENT, MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
EACH RECIPIENT ISSOLELY RESPONSIBLE FOR DETERMINING THE
APPROPRIATENESS OF ITS USE AND DISTRIBUTION OF THE PROGRAM AND ASSUMES
ALL RISKS ASSOCIATED WITH ITSEXERCISE OF RIGHTS UNDER THIS AGREEMENT,
INCLUDING BUT NOT LIMITED TO THE RISKS AND COSTS OF PROGRAM ERRORS,
COMPLIANCE WITH APPLICABLE LAWS, DAMAGE TO OR LOSS OF DATA, PROGRAMS
OR EQUIPMENT, ANDUNAVAILABILITY OR INTERRUPTION OF OPERATIONS.  THIS
DISCLAIMER OR WARRANTY CONSTITUTES AN ESSENTIAL PART OF THIS
AGREEMENT.  NO USE OFTHE PROGRAM OR ANY MODIFICATIONS THERETO ARE
AUTHORIZED HEREUNDER EXCEPT UNDER THIS DISCLAIMER.

7.  DISCLAIMER OF LIABILITY 

EXCEPT AS EXPRESSLY SET FORTH IN THIS AGREEMENT, NEITHER RECIPIENT
NORANY CONTRIBUTORS SHALL HAVE ANY LIABILITY FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDINGWITHOUT LIMITATION LOST PROFITS), HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDINGNEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OR DISTRIBUTION OF THE PROGRAM OR THE EXERCISE OF ANY RIGHTS
GRANTEDHEREUNDER, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

8.  U.S. GOVERNMENT USE 

If Recipient is licensing the Program on behalf of any unit or agency
of the United StatesGovernment, then the Program and any related
documentation is commercial computer software, and, pursuant to FAR
12.212 or DFARS 227.7202 and their successors, asapplicable, shall be
licensed to the Government under the terms and conditions of this
Agreement.

9.  PATENT CLAIMS 

If Recipient institutes patent litigation against any entity
(including a cross-claim,counterclaim or declaratory judgment claim in
a lawsuit) alleging that the Program itself (excluding combinations of
the Program with other software or hardware) infringes suchRecipient's
patent(s), then the rights granted to Recipient by each Contributor
under Section 2.2 shall terminate as of the date such litigation is
filed.

10.  TERMINATION 

All Recipient's rights under this Agreement shall terminate if
Recipient fails to complywith any of the material terms or conditions
of this Agreement and does not cure such failure in a reasonable
period of time after becoming aware of such noncompliance.  Ifsuch
occurs, Recipient shall cease use and Distribution of the Program
based upon the rights granted to Recipient under this Agreement as
soon as reasonably practicable.However, Recipient's obligations under
this Agreement and any licenses granted by Recipient relating to the
Program shall survive such termination.

11.  LICENSE VERSIONS 

OSCI may publish new versions (including revisions) of this Agreement
from time totime.  Each new version of the Agreement will be given a
distinguishing version number. The Program may always be Distributed
subject to the version of the Agreement underwhich it was received.
In addition, after a new version of the Agreement is published,

Contributor may elect to Distribute the Program under the new version.
No one otherthan OSCI, acting by a vote of at least 75% of the members
of its Board of Directors, has the right to modify this Agreement;
provided that Exhibit B and Exhibit C may beamended as specifically
set forth in Section 3.1(b), and Exhibit D may be amended as
specifically set forth in Section 1.10. 

12.  ELECTRONIC ACCEPTANCE

This Agreement may be executed either electronically or on paper.  By
clicking on the"Accept" button, Recipient warrants that it agrees to
all of the terms of this Agreement, that Recipient is authorized to
enter into this Agreement, and that this Agreement islegally binding
upon Recipient.  If Recipient does not agree to be bound by this
Agreement, then Recipient shall click the "Decline" button and
Recipient shall notreceive any rights from the Contributors nor shall
Recipient download any materials, including the Program. 

13.  GENERAL

This Agreement represents the complete agreement concerning the
subject matterhereof and supersedes all prior agreements or
representations, oral or written, regarding the subject matter hereof.
If any provision of this Agreement is invalid orunenforceable under
applicable law, it shall not affect the validity or enforceability of
the remainder of the terms of this Agreement, and without further
action by the partieshereto, such provision shall be reformed to the
minimum extent necessary to make such provision valid and
enforceable. This Agreement is governed by the laws of California,
without reference to conflict oflaws principles.  Each party waives
its rights to a jury trial in any resulting litigation. Any litigation
relating to this Agreement shall be subject to the jurisdiction of the
FederalCourts of the Northern District of California, with venue lying
in Santa Clara County, California, or the Santa Clara County Superior
Court.  The application of the UnitedNations Convention on Contracts
for the International Sale of Goods is expressly excluded.  The
provisions of this Agreement shall be construed fairly in accordance
withits terms and no rules of construction for or against either party
shall be applied in the interpreting this Agreement.  Recipient shall
not use the Program in violation of localand other applicable laws
including, but not limited to, the export control laws of the United
States.

EXHIBIT A List of Contributors

1. Synopsys, Inc. 
2. Cadence Design Systems, Inc. 
3. CoWare, Inc.

EXHIBIT D Trademark Usage Policy

I.  LIST OF MARKS

1. Open SystemC 
2. Open SystemC Initiative 
3. OSCI, SystemC 
4. SystemC Graphic Logo 
5. All logos that incorporate the foregoing word marks

II.  PROPER USE OF MARKS 

Trademarks and service marks function as adjectives and generally
should not be used asnouns or verbs.  Accordingly, as often as
possible, the Marks should be used as adjectives immediately preceding
the generic noun that refers to the service in question.  For example:

The SystemC(TM) software
The SystemC(TM) LRM

No Possessives or Plurals.  Since they are not nouns, the Marks should
never be used in thepossessive or plural forms.  For example, it is
not appropriate to write "SystemC's software." No Use as Verbs or as
Puns.  The Marks should never be used as verbs or as puns. 

III. PROPER ATTRIBUTION 

Trademark ownership is attributed in two ways, with the use of a
symbol (TM, SM, (R)) after the markand with a legal legend, usually
found at the end of a document following the copyright
notice. Following are OSCI's rules for symbols and legends to
attribute the Marks:

Symbols: 

Which Symbol Do I Use?

The Marks generally function as trademarks rather than servicemarks.
Unless you are specifically directed otherwise, please use the (TM) or
(R) symbol after the Marks. 

Where Do I Place the (R) Symbol?

The (TM) or (R) symbol is placed immediately after the mark, either in
superscript or subscript.

When Do I Use the Symbol?

The (TM) or (R) symbol is to be used after the Marks in the following
instances:

Most Prominent Uses:  

A (TM) or (R) symbol is required after prominent uses of the Marks,
e.g., in the headlines and large print text of web pages,
advertisements, other promotional materials and press releases, except
where space limitations or specific style considerations
preventcompliance with this requirement.

First Use in Text:  

A (TM) or (R) symbol is required after the first use of each Mark in
text, e.g.advertising copy or the body of press releases, even though
the symbol may have already appeared in the headline or after another
prominent use of the mark in the same document.

All Logos: 

The (TM) or (R) symbol must appear after all logos incorporating the
Marks. 

IV. Legends

All Marks that appear on a web page or in a press release,
advertisement or other writtenmaterial (whether in print or electronic
form) must be attributed in an appropriate legend.  The legend may be
presented in "mouseprint" but must be large enough to be read easily.
Legendsgenerally appear at the end of a document or the bottom of a
web page but may be placed elsewhere, e.g. the inside cover of
documentation. The OSCI Legend: The following legend should be used in
all materials in which any of theMarks appear:

[Insert the Marks] are trademarks or registered trademarks of Open
SystemC Initiative, Inc. in the United States and other countries and
are used with permission.

V.  MARKS NEVER COMBINED 

The Marks should never be combined with the marks of any business
other than OSCI.  The Marks should always appear visually separate
from any other marks appearing in the same materials such that each
mark creates a distinct commercial impression.  It would, for
instance,not be appropriate to superimpose the logo of another
business over any OSCI logo.

VI.  LOGOS 

Logos incorporating the Marks can only be used in the format provided
to you by OSCI for incorporation into your materials or web pages.  The
logos provided to you by OSCI cannot be modified in any way without
OSCI's prior written approval.  Logos copied from OSCI web pages or
other materials may not to be used.  Please contact
webmaster@systemc.org to obtain electronic files containing the OSCI
logos and to ask any questions regarding the logos.
